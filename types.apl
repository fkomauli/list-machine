ty: type.
ty_nil: ty.
ty_list: ty -> ty.
ty_listcons: ty -> ty.

type env = [(var,ty)].


pred new_var (var,env).
new_var(_,[]).
new_var(V,[(V',T)|G]) :- not_same_var(V,V'), new_var(V,G).


pred env_ok (env).
env_ok([]).
env_ok([(V,T)|G]) :- new_var(V,G), env_ok(G).


func env_lookup (env,var) = (ty,env).
env_lookup([(V,T)|G],V) = (T,G).
env_lookup([(V,T)|G],V') = (T',[(V,T)|G']) :- not_same_var(V,V'), (T',G') = env_lookup(G,V').


func env_set(env,var,ty) = env.
env_set([(V,T)|G],V,T') = [(V,T')|G].
env_set([(V,T)|G],V',T') = [(V,T)|G'] :- not_same_var(V,V'), G' = env_set(G,V',T').
env_set([],V,T) = [(V,T)].


pred subtype (ty,ty).
subtype(T,T).
subtype(ty_nil,ty_list(_)).
subtype(ty_list(T),ty_list(T')) :- subtype(T,T').
subtype(ty_listcons(T),ty_list(T')) :- subtype(T,T').
subtype(ty_listcons(T),ty_listcons(T')) :- subtype(T,T').


pred env_sub (env,env).
env_sub(_,[]).
env_sub(G,[(V,T)|G']) :- (T1,G1) = env_lookup(G,V), subtype(T1,T), env_sub(G1,G').


func lub (ty,ty) = ty.
lub(T,T) = T.
lub(ty_list(T1),ty_nil) = ty_list(T1).
lub(ty_list(T1),ty_listcons(T2)) = ty_list(T3) :- T3 = lub(T1,T2).
lub(ty_listcons(T1),ty_list(T2)) = ty_list(T3) :- T3 = lub(T1,T2).
lub(ty_list(T1),ty_list(T2)) = ty_list(T3) :- T3 = lub(T1,T2).
lub(ty_nil,ty_list(T1)) = ty_list(T1).
lub(ty_nil,ty_listcons(T1)) = ty_list(T1).
lub(ty_listcons(T1),ty_nil) = ty_list(T1).
lub(ty_listcons(T1),ty_listcons(T2)) = ty_listcons(T3) :- T3 = lub(T1,T2).


pred value_has_ty (value,ty).
value_has_ty(value_nil,ty_nil).
value_has_ty(value_nil,ty_list(_)).
value_has_ty(value_cons(A',A''),ty_listcons(T)) :- value_has_ty(A',T), value_has_ty(A'',ty_list(T)).
value_has_ty(value_cons(A',A''),ty_list(T)) :- value_has_ty(A',T), value_has_ty(A'',ty_list(T)).

pred store_has_type (store,env).
store_has_type(R,[]).
store_has_type(R,[(V,T)|G]) :- A = var_lookup(R,V), value_has_ty(A,T), store_has_type(R,G).


pred exists_variable_with_type (store,var,ty).
exists_variable_with_type(R,V,T) :- A = var_lookup(R,V), value_has_ty(A,T).


type program_typing = [(label,env)].

func block_typing_lookup (program_typing,label) = env.
block_typing_lookup([(L,G)|Pi],L) = G :- env_ok(G).
block_typing_lookup([(L,G)|Pi],L') = G' :- not_same_label(L,L'), G' = block_typing_lookup(Pi,L').


pred check_instr (program_typing,env,instr,env).
check_instr(Pi,G,seq(I1,I2),G2) :- check_instr(Pi,G,I1,G1), check_instr(Pi,G1,I2,G2).
check_instr(Pi,G,instr_branch_if_nil(V,L),[(V,ty_listcons(T))|G']) :- (ty_list(T),G') = env_lookup(G,V), G1 = block_typing_lookup(Pi,L), env_sub([(V,ty_nil)|G'],G1).
check_instr(Pi,G,instr_branch_if_nil(V,L),G) :- (ty_listcons(T),_) = env_lookup(G,V).
check_instr(Pi,G,instr_branch_if_nil(V,L),G) :- (ty_nil,G') = env_lookup(G,V), G1 = block_typing_lookup(Pi,L), env_sub(G,G1).
check_instr(Pi,G,instr_fetch_field(V,zf,V'),G') :- 
(ty_listcons(T),_) = env_lookup(G,V), G' = env_set(G,V',T).
check_instr(Pi,G,instr_fetch_field(V,of,V'),G') :- (ty_listcons(T),_) = env_lookup(G,V), G' = env_set(G,V',ty_list(T)).
check_instr(Pi,G,instr_cons(V0,V1,V),G') :- (T0,_) = env_lookup(G,V0), (T1,_) = env_lookup(G,V1), ty_list(T) = lub(ty_list(T0),T1), G' = env_set(G,V,ty_listcons(T)).


pred check_block (program_typing,env,instr).
check_block(Pi,G,instr_halt).
check_block(Pi,G,seq(I1,I2)) :- check_instr(Pi,G,I1,G1), check_block(Pi,G1,I2).
check_block(Pi,G,instr_jump(L)) :- 
    G' = block_typing_lookup(Pi,L), env_sub(G,G').


pred check_blocks (program_typing,program).
check_blocks(Pi,[]).
check_blocks(Pi,[(L,I)|P]) :- G = block_typing_lookup(Pi,L), check_block(Pi,G,I), check_blocks(Pi,P).


pred typing_dom_match(program_typing,program).
typing_dom_match([(L,G)],[(L,I)]).
typing_dom_match([(L,G)|Pi],[(L,I)|P]) :- typing_dom_match(Pi,P).


pred check_program (program,program_typing).
check_program(P,Pi) :- block_typing_lookup(Pi,l0) = [(v0,ty_nil)], typing_dom_match(Pi,P), check_blocks(Pi,P).
