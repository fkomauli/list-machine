%% Counterexample to preservation found using F# FsCheck after the update of
%% the value-has-ty predicate. The iterative deepening approach shows that the
%% counterexample is a false positive, because the existential env that
%% satisfies the postconditions is found.
%% The found env is the empty one, meaning that the algorithmic approach doesn't
%% take in account this case. The version in F# has been fixed and now no
%% counterexamples are found.


pred sample_prog (program).
sample_prog([
    (l0, seq(
        instr_cons(v0,v0,v1),
        seq(
            instr_cons(v1,v0,v1),
            seq(
                instr_cons(v1,v0,v1),
                instr_jump(l1)
            )
        )
    )),
    (l1,seq(
        instr_fetch_field(v1,zf,v2),
        instr_halt
    ))
]).


pred sample_typing (program_typing).
sample_typing([
    (l0,[
        (v0,ty_nil)
    ]),
    (l1,[
        (v0,ty_nil),
        (v1,ty_listcons(ty_listcons(ty_listcons(ty_nil))))
    ])
]).


pred sample_store (store).
sample_store([
    (v0,value_nil),
    (v1,value_cons(value_cons(value_cons(value_nil,value_nil),value_nil),value_nil)),
    (v2,value_cons(value_cons(value_nil,value_cons(value_nil,value_nil)),value_nil))
]).


%% Defines an alternative definition of value_has_ty.
pred value_has_ty' (value,ty).
value_has_ty'(value_nil,ty_nil).
value_has_ty'(value_nil,ty_list(_)).
value_has_ty'(value_cons(A',A''),ty_listcons(T)) :- value_has_ty'(A',T), value_has_ty'(A'',ty_list(T)).
value_has_ty'(value_cons(A',A''),ty_list(T)) :- value_has_ty'(A',T), value_has_ty'(A'',ty_list(T)).

pred store_has_type' (store,env).
store_has_type'(R,[]).
store_has_type'(R,[(V,T)|G]) :- A = var_lookup(R,V), value_has_ty'(A,T), store_has_type'(R,G).


%% Preservation check:
%% #check "preservation" 10 : check_program(P,Pi), env_ok(G), store_has_type(R,G), check_block(Pi,G,I), step(P,R,I,R',I') => exists_env_for_store_and_block(Pi,R',I').


%% All preconditions are satisfied by the counterexample
? sample_prog(P), sample_typing(Pi), sample_store(R), G = block_typing_lookup(Pi,l1), I = program_lookup(P,l1),
  check_program(P,Pi), env_ok(G), store_has_type'(R,G), check_block(Pi,G,I), step(P,R,I,R',I').


%% Existential predicate
pred exists_env_for_store_and_block (program_typing,store,instr).
exists_env_for_store_and_block(Pi,R,I) :- env_ok(G), store_has_type(R,G), check_block(Pi,G,I).

pred exists_env_for_store_and_block' (program_typing,store,instr).
exists_env_for_store_and_block'(Pi,R,I) :- env_ok(G), store_has_type'(R,G), check_block(Pi,G,I).


%% Existential predicate (yields the found existential)
pred env_for_store_and_block (program_typing,store,instr,env).
env_for_store_and_block(Pi,R,I,G) :- env_ok(G), store_has_type(R,G), check_block(Pi,G,I).

pred env_for_store_and_block' (program_typing,store,instr,env).
env_for_store_and_block'(Pi,R,I,G) :- env_ok(G), store_has_type'(R,G), check_block(Pi,G,I).



?   sample_prog(P),
    sample_typing(Pi),
    sample_store(R),
    I = program_lookup(P,l1),
    step(P,R,I,R',I'),
    env_for_store_and_block'(Pi,R',I',G).


#check "find existence of env by iterative deepening - new value_has_ty" 100:
    sample_prog(P),
    sample_typing(Pi),
    sample_store(R),
    I = program_lookup(P,l1),
    step(P,R,I,R',I'),
    env_ok(G'),
    store_has_type'(R',G'),
    check_block(Pi,G',I')
=>  fail.
