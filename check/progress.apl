pred progress_var_bind_set (store).
progress_var_bind_set(R') :- R' = var_set([(V,A)|R],V',A').

pred progress_var_set (store).
progress_var_set(R') :- R' = var_set(R,V,A).

pred exists_seq_with_branch_that_steps_or_halts (program,store,var,label).
exists_seq_with_branch_that_steps_or_halts(P,R,V,L) :- step_or_halt(P,R,seq(instr_branch_if_nil(V,L),_)).

pred exists_seq_that_steps_or_halts (program,store,instr).
exists_seq_that_steps_or_halts(P,R,I) :- step_or_halt(P,R,seq(I,_)).


#check "progress-var-bind-set" 6 : progress_var_bind_set(R').

#check "progress-var-set" 6 : progress_var_set(R').

#check "progress-branch" 7 : program_lookup(P,L) = _, A = var_lookup(R,V) => exists_seq_with_branch_that_steps_or_halts(P,R,V,L).

#check "progress-instr" 10 : check_program(P,Pi), check_instr(Pi,G,I,G'), store_has_type(R,G) => exists_seq_that_steps_or_halts(P,R,I).
