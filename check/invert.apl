pred exists_env_contained_in_program_typing (program_typing,label,env).
exists_env_contained_in_program_typing(Pi,L,G) :- G' = block_typing_lookup(Pi,L), env_sub(G,G').

pred exists_variable_in_env (env,var).
exists_variable_in_env(G,V) :- env_lookup(G,V) = (_,_).

pred exists_env_checking_with_listcons (program_typing,env,label,var,ty,instr).
exists_env_checking_with_listcons(Pi,G,L,V,T,I) :- G' = block_typing_lookup(Pi,L), env_sub([(V,ty_nil)|G],G'), check_block(Pi,[(V,ty_listcons(T))|G],I).

pred exists_sub_env_checking (program_typing,env,env,label,var,instr).
exists_sub_env_checking(Pi,G1,G2,L,V,I) :- G' = block_typing_lookup(Pi,L), env_sub([(V,ty_nil)|G2],G'), check_block(Pi,G,I).

pred env_set_and_check_block (program_typing,env,var,var,instr).
env_set_and_check_block(Pi,G,V,V',I) :- (ty_listcons(T),_) = env_lookup(G,V), G' = env_set(G,V',T), check_block(Pi,G',I).


#check "check-instr-seq-invert" 9 : check_block(Pi,G,instr_jump(L)) => exists_env_contained_in_program_typing(Pi,L,G).

#check "check-instr2-invert" 11 : check_block(Pi,G,seq(instr_branch_if_nil(V,L),I)) => exists_variable_in_env(G,V).

#check "check-instr-branch-list-invert" 7 : check_block(Pi,G,seq(instr_branch_if_nil(V,L),I)), (ty_list(T),G') = env_lookup(G,V) => exists_env_checking_with_listcons(Pi,G',L,V,T,I).

#check "check-instr-branch-listcons-invert" 9 : check_block(Pi,G,seq(instr_branch_if_nil(V,L),I)), (ty_listcons(T),G') = env_lookup(G,V) => exists_sub_env_checking(Pi,G,G',L,V,I).

#check "check-instr-branch-nil-invert" 10 : check_block(Pi,G,seq(instr_branch_if_nil(V,L),I)), (ty_nil,G') = env_lookup(G,V) => exists_env_contained_in_program_typing(Pi,L,G).

#check "check-instr-fetch-field-0-invert" 9 : check_block(Pi,G,seq(instr_fetch_field(V,zf,V'),I)) => env_set_and_check_block(Pi,G,V,V',I).
