%% From ListMachine.v of Coq implementation
%% ("soundness" in specification.elf)

%%Theorem safety:
%%  forall p s i pt,
%%  check_program pt p ->
%%  run_prog_finite p s i ->
%%  step_or_halt p s i.

#check "safety" 15 : check_program(P,Pi), (R,I) = run_prog_finite(P) => step_or_halt(P,R,I).
