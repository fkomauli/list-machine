#check "var-set-type" 7 : env_ok(G), store_has_type(R,G), value_has_ty(A,T), R' = var_set(R,V,A), G' = env_set(G,V,T) => store_has_type(R',G').

#check "progress-typing-dom-match" 9 : typing_dom_match(Pi,P), G = block_typing_lookup(Pi,L) => exists_program_lookup(P,L).

#check "progress-check-program" 27 : check_program(P,Pi), G = block_typing_lookup(Pi,L) => exists_program_lookup(P,L).

#check "progress-env" 10 : env_lookup(G,V) = (T,_), store_has_type(R,G) => exists_variable_with_type(R,V,T).

#check "progress" 13 : check_program(P,Pi), check_block(Pi,G,I), store_has_type(R,G) => step_or_halt(P,R,I).

#check "lub-comm" 7 :  T3 = lub(T1,T2) => lub(T2,T1) = T3.

#check "lub-subtype-left" 8 :  T3 = lub(T1,T2) => subtype(T1,T3).

#check "lub-subtype-right" 8 :  T3 = lub(T1,T2) => subtype(T2,T3).

#check "subsumption" 9 : value_has_ty(A,T1), subtype(T1,T2) => value_has_ty(A,T2).

#check "subsumption-env" 9 : store_has_type(R,G1), env_sub(G1,G2) => store_has_type(R,G2).

#check "preservation-env-lookup" 11 : store_has_type(R,G), (T,_) = env_lookup(G,V), A = var_lookup(R,V) => value_has_ty(A,T).

#check "preservation-block-typing" 10 : check_blocks(Pi,P), I = program_lookup(P,L), G = block_typing_lookup(Pi,L) => check_block_and_env(Pi,G,I).

#check "preservation-program-typing" 21 : check_program(P,Pi), I = program_lookup(P,L), G = block_typing_lookup(Pi,L) => check_block_and_env(Pi,G,I).

#check "preservation" 13 : check_program(P,Pi), env_ok(G), store_has_type(R,G), check_block(Pi,G,I), step(P,R,I,R',I') => exists_env_for_store_and_block(4, Pi,R',I').

#check "run-well-typed" 15 : check_program(P,Pi), R = [(v0,value_nil)], I = program_lookup(P,l0), steps(P,R0,I0,R,I) => exists_env_for_store_and_block(5,Pi,R,I).

#check "soundness" 25 : check_program(P,Pi), R = [(v0,value_nil)], I = program_lookup(P,l0), steps(P,R,I,R',I') => step_or_halt(P,R',I').
