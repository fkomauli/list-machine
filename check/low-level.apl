#check "var-set-lookup" 10 : R' = var_set(R,V,A) => var_lookup(R',V) = A.

#check "var_set_type_refine" 7 : store_has_type(R,G), A = var_lookup(R,V), value_has_ty(A,T), G' = env_set(G,V,T) => store_has_type(R,G').

#check "store_has_ty_empty" 1 : store_has_type([],[]).

#check "var-lookup-uniq" 10 : A = var_lookup(R,V), A' = var_lookup(R,V) => A = A'.

#check "env-lookup-uniq T" 10 : (T1,G1) = env_lookup(G,V), (T2,G2) = env_lookup(G,V) => T1 = T2.

#check "env-lookup-uniq G" 12 : (T1,G1) = env_lookup(G,V), (T2,G2) = env_lookup(G,V) => G1 = G2.

#check "env-set-good" 10 : env_ok(G), G' = env_set(G,V,T) => env_ok(G').

#check "env-set-lookup" 10 : G' = env_set(G,V,T), (T',G'') = env_lookup(G',V) => T' = T.

#check "env-lookup removes variable" 10 : env_ok(G), (T, G') = env_lookup(G, V) => new_var(V, G').

#check "store-has-type-set-diff" 8 : store_has_type(R,G), R' = var_set(R,V,A), new_var(V,G) => store_has_type(R',G).

#check "var-set-lookup-diff" 10 : R' = var_set(R,V,A), A' = var_lookup(R,V'), not_same_var(V,V') => var_lookup(R',V') = A'.

#check "block-typing-lookup-ok" 10 : G = block_typing_lookup(Pi,L) => env_ok(G).

#check "block-typing-lookup-uniq" 10 : G = block_typing_lookup(Pi,L), G' = block_typing_lookup(Pi,L) => G = G'.

#check "step_deterministic R" 10 : step(P,R,I,R1,I1), step(P,R,I,R2,I2) => R1 = R2.

#check "step_deterministic I" 7 : step(P,R,I,R1,I1), step(P,R,I,R2,I2) => I1 = I2.

% inspired by unit test:
% ? not_same_var(X,Y), check_block([],[(X,ty_listcons(ty_nil))],seq(instr_fetch_field(X,zf,Y),instr_halt)).
#check "sequence well-typedness" 6 : check_instr(Pi,G,I,G'), check_block(Pi,G',I') => check_block(Pi,G,seq(I,I')).
