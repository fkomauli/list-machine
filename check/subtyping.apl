% ------------- LUB -----------------------

#check "lub-least" 6 :  T3 = lub(T1,T2), subtype(T1,T4), subtype(T2,T4) => subtype(T3,T4).

#check "subtype-list-invert" 6 : subtype(ty_list(T1),ty_list(T2)) => subtype(T1,T2).

#check "lub_idem" 10 : lub(T,T) = T.

#check "lub_of_subtype" 6: subtype(T1, T2) => lub(T2, T1) = T2.

% ---------- SUBSUMPTION ---------------

#check "subsumption-env2" 8 : store_has_type(R,G), (T,G') = env_lookup(G,V) => store_has_type(R,G').
