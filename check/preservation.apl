#check "preservation-step-branch-not-taken" 15 : check_program(P,Pi), env_ok(G), store_has_type(R,G), check_block(Pi,G,seq(instr_branch_if_nil(V,L),I)), var_lookup(R,V) = value_cons(_,_), (T,G') = env_lookup(G,V) => exists_env_for_store_and_block(4,Pi,R,I).

#check "preservation-step-branch-taken" 15 : check_program(P,Pi), store_has_type(R,G), check_block(Pi,G,seq(instr_branch_if_nil(V,L),I)), var_lookup(R,V) = value_nil, I' = program_lookup(P,L), (T,G') = env_lookup(G,V) => exists_env_for_store_and_block(4,Pi,R,I').

#check "run-prog-finite-well-typed" 15 : check_program(P,Pi), (R,I) = run_prog_finite(P) => exists_env_for_store_and_block(5,Pi,R,I).
