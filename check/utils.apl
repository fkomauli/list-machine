pred check_block_and_env (program_typing,env,instr).
check_block_and_env(Pi,G,I) :- check_block(Pi,G,I), env_ok(G).

/*
 * The existential codification with the following predicate doesn't work
 * because the G variable is not ground and predicates store_has_type, env_ok
 * and check_block expect it as input only.
 *
 *     pred exists_env_for_store_and_block (program_typing,store,instr).
 *     exists_env_for_store_and_block(Pi,R,I) :- store_has_type(R,G), env_ok(G), check_block(Pi,G,I).
 *
 * Therefore the predicate is defined with a depth parameter that limits the
 * size of the existential to be found. Before the three predicates mentioned
 * above, the build_env(N,G) predicate generates a ground env.
 */

pred build_ty (int,ty).
build_ty(_,ty_nil).
build_ty(N,ty_list(T)) :- N > 0, build_ty(N - 1,T).
build_ty(N,ty_listcons(T)) :- N > 0, build_ty(N - 1,T).

pred build_var (var).
build_var(v0).
build_var(v1).
build_var(v2).

pred build_vars (var,var).
build_vars(v0,v1).
build_vars(v1,v2).
build_vars(v2,v0).

pred build_env (int,env).
build_env(N,[]) :- N <= 0.
build_env(N,[(V0,T0)]) :- N > 0, build_var(V0), build_ty(N,T0).
build_env(N,[(V0,T0),(V1,T1)]) :- N > 0, build_vars(V0,V1), build_ty(N,T0), build_ty(N,T1).
build_env(N,[(v0,T0),(v1,T1),(v2,T2)]) :- N > 0, build_ty(N,T0), build_ty(N,T1), build_ty(N,T2).

pred exists_env_for_store_and_block (int,program_typing,store,instr).
exists_env_for_store_and_block(N,Pi,R,I) :- N > 0, exists_env_for_store_and_block(N - 1,Pi,R,I).
exists_env_for_store_and_block(N,Pi,R,I) :- N >= 0, build_env(N,G), store_has_type(R,G), env_ok(G), check_block(Pi,G,I).
