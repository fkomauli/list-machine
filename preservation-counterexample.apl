%% Counterexample to preservation found using F# FsCheck and saved in
%% PreservationCounterexample.fsx
%%
%% The preservation check postcondition uses an existential for the typing
%% environment that is obtained from the execution of an instruction and that
%% must typecheck with the next instruction.
%%
%% The checks of this file try to find such environment by generating it
%% with the iterative deepening approach.


pred sample_prog (program).
sample_prog([
    (l0, seq(
        instr_cons(v0,v0,v0),
        instr_jump(l1)
    )),
    (l1,seq(
        instr_fetch_field(v0,zf,v0),
        instr_jump(l2)
    )),
    (l2,
        instr_halt
    )
]).


pred sample_typing (program_typing).
sample_typing([
    (l0,[
        (v0,ty_nil)
    ]),
    (l1,[
        (v0,ty_listcons(ty_nil))
    ]),
    (l2,[
        (v0,ty_nil)
    ])
]).


%% The execution of the sample program would lead at l1 to the store
%%
%%     [(v0, value_cons(value_nil,value_nil))]
%%
%% but even the store below has the type matching with Pi(l1) and satisfies all
%% preconditions of the preservation check.
pred sample_store (store).
sample_store([
    (v0,value_cons(value_cons(value_nil,value_nil),value_nil))
]).


%% Defines an alternative definition of value_has_ty.
pred value_has_ty' (value,ty).
value_has_ty'(value_nil,ty_nil).
value_has_ty'(value_nil,ty_list(_)).
value_has_ty'(value_cons(A',A''),ty_listcons(T)) :- value_has_ty'(A',T), value_has_ty'(A'',ty_list(T)).
value_has_ty'(value_cons(A',A''),ty_list(T)) :- value_has_ty'(A',T), value_has_ty'(A'',ty_list(T)).

pred store_has_type' (store,env).
store_has_type'(R,[]).
store_has_type'(R,[(V,T)|G]) :- A = var_lookup(R,V), value_has_ty'(A,T), store_has_type'(R,G).


%% Preservation check:
%% #check "preservation" 10 : check_program(P,Pi), env_ok(G), store_has_type(R,G), check_block(Pi,G,I), step(P,R,I,R',I') => exists_env_for_store_and_block(Pi,R',I').


%% All preconditions are satisfied by the counterexample
? sample_prog(P), sample_typing(Pi), sample_store(R), G = block_typing_lookup(Pi,l1), I = program_lookup(P,l1),
  check_program(P,Pi), env_ok(G), store_has_type(R,G), check_block(Pi,G,I), step(P,R,I,R',I').

%% But not with the alternative definition of value_has_ty and store_has_type
? sample_prog(P), sample_typing(Pi), sample_store(R), G = block_typing_lookup(Pi,l1), I = program_lookup(P,l1),
  check_program(P,Pi), env_ok(G), store_has_type'(R,G), check_block(Pi,G,I), step(P,R,I,R',I').


%% Using this store the new definition in the preconditions is satisfied
pred sample_store' (store).
sample_store'([
    (v0,value_cons(value_nil,value_nil))
]).

? sample_prog(P), sample_typing(Pi), sample_store'(R), G = block_typing_lookup(Pi,l1), I = program_lookup(P,l1),
  check_program(P,Pi), env_ok(G), store_has_type'(R,G), check_block(Pi,G,I), step(P,R,I,R',I').



%% Existential predicate
pred exists_env_for_store_and_block (program_typing,store,instr).
exists_env_for_store_and_block(Pi,R,I) :- env_ok(G), store_has_type(R,G), check_block(Pi,G,I).

pred exists_env_for_store_and_block' (program_typing,store,instr).
exists_env_for_store_and_block'(Pi,R,I) :- env_ok(G), store_has_type'(R,G), check_block(Pi,G,I).


%% Existential predicate (yields the found existential)
pred env_for_store_and_block (program_typing,store,instr,env).
env_for_store_and_block(Pi,R,I,G) :- env_ok(G), store_has_type(R,G), check_block(Pi,G,I).

pred env_for_store_and_block' (program_typing,store,instr,env).
env_for_store_and_block'(Pi,R,I,G) :- env_ok(G), store_has_type'(R,G), check_block(Pi,G,I).



%% Loops
%%?   sample_prog(P),
%%    sample_typing(Pi),
%%    sample_store(R),
%%    I = program_lookup(P,l1),
%%    step(P,R,I,R',I'),
%%    env_for_store_and_block(Pi,R',I',G).

?   sample_prog(P),
    sample_typing(Pi),
    sample_store'(R),
    I = program_lookup(P,l1),
    step(P,R,I,R',I'),
    env_for_store_and_block'(Pi,R',I',G).


#check "find existence of env by iterative deepening - old value_has_ty" 30:
    sample_prog(P),
    sample_typing(Pi),
    sample_store(R),
    I = program_lookup(P,l1),
    step(P,R,I,R',I'),
    env_ok(G'),
    store_has_type(R',G'),
    check_block(Pi,G',I')
=>  fail.

#check "find existence of env by iterative deepening - new value_has_ty" 13:
    sample_prog(P),
    sample_typing(Pi),
    sample_store'(R),
    I = program_lookup(P,l1),
    step(P,R,I,R',I'),
    env_ok(G'),
    store_has_type'(R',G'),
    check_block(Pi,G',I')
=>  fail.


%% These checks loop at depth 13
%% #check "preservation - new value_has_ty" 13 : check_program(P,Pi), env_ok(G), store_has_type'(R,G), check_block(Pi,G,I), step(P,R,I,R',I') => exists_env_for_store_and_block'(Pi,R',I').
%% #check "preservation - old value_has_ty" 13 : check_program(P,Pi), env_ok(G), store_has_type(R,G), check_block(Pi,G,I), step(P,R,I,R',I') => exists_env_for_store_and_block(Pi,R',I').
