%% Utility function to make example programs more readable
func blk ([instr]) = instr.
blk([I]) = I.
blk([I|L]) = seq(I,blk(L)).


pred sample_prog (program).
sample_prog([
    (l0, blk([
        instr_cons(v0,v0,v1),
        instr_cons(v0,v1,v1),
        instr_cons(v0,v1,v1),
        instr_jump(l1)
    ])),
    (l1,blk([
        instr_branch_if_nil(v1,l2),
        instr_fetch_field(v1,of,v1),
        instr_branch_if_nil(v0,l1),
        instr_jump(l2)
    ])),
    (l2,blk([
        instr_halt
    ]))
]).


pred sample_typing (program_typing).
sample_typing([
    (l0,[
        (v0,ty_nil)
    ]),
    (l1,[
        (v0,ty_nil),
        (v1,ty_list(ty_nil))
    ]),
    (l2,[])
]).


? sample_prog(P).

? sample_typing(Pi).

? sample_prog(P), sample_typing(Pi), check_program(P,Pi).

? sample_prog(P), run_prog(P).
