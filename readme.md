### List machine

Model of a simple typed assembly language using aProlog.

[Link to list machine language documentation](http://www.cs.princeton.edu/~appel/listmachine/ "List machine")

To execute the checks, run:

```
aprolog -check-nf -q machine.apl types.apl check.apl
```

while to execute unit tests, run

```
aprolog -q machine.apl types.apl unit-test.apl
```

Variuos mutations are introduced in separated branches, named bugX where X is an incremental index.


### Mutations

Below are listed the checks triggered by a mutation and the mutations that a check can find a counterexample for.


| Mutation id                                                                       | Mutation kind                 | Failing unit tests | Counterexamples found by (at depth)                                                                                                                                                                |
|-----------------------------------------------------------------------------------|-------------------------------|--------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| [1](https://bitbucket.org/fkomauli/list-machine/branch/bug1#diff "Mutation 1")    | Variable: var by var          | 1                  | progress(13), soundness(19)                                                                                                                                                                        |
| [2](https://bitbucket.org/fkomauli/list-machine/branch/bug2#diff "Mutation 2")    | Statement: del pred in clause | 0                  | var-set-lookup(9), step_deterministic R(9), preservation(13), soundness(23)                                                                                                                        |
| [3](https://bitbucket.org/fkomauli/list-machine/branch/bug3#diff "Mutation 3")    | Variable: var by var          | 1                  | preservation(13), soundness(23)                                                                                                                                                                    |
| [4](https://bitbucket.org/fkomauli/list-machine/branch/bug4#diff "Mutation 4")    | Variable: var by var          | 0                  | step_deterministic R(6)                                                                                                                                                                            |
| [5](https://bitbucket.org/fkomauli/list-machine/branch/bug5#diff "Mutation 5")    | Variable: var by var          | 0                  | block-typing-lookup-uniq(6), progress-typing-dom-match(7), progress-check-program(18), soundness(25)                                                                                               |
| [6](https://bitbucket.org/fkomauli/list-machine/branch/bug6#diff "Mutation 6")    | Variable: var swap            | 1                  | preservation(13), soundness(23)                                                                                                                                                                    |
| [7](https://bitbucket.org/fkomauli/list-machine/branch/bug7#diff "Mutation 7")    | Statement: del clause         | 1                  | progress(13), soundness(23)                                                                                                                                                                        |
| [8](https://bitbucket.org/fkomauli/list-machine/branch/bug8#diff "Mutation 8")    | Variable: var by var          | 0                  | soundness(23)                                                                                                                                                                                      |
| [9](https://bitbucket.org/fkomauli/list-machine/branch/bug9#diff "Mutation 9")    | Statement: del clause         | 0                  | progress(13), safety(13), soundness(13)                                                                                                                                                            |
| [10](https://bitbucket.org/fkomauli/list-machine/branch/bug10#diff "Mutation 10") | Statement: del pred in clause | 0                  | env-lookup removes variable(7)                                                                                                                                                                     |
| [11](https://bitbucket.org/fkomauli/list-machine/branch/bug11#diff "Mutation 11") | Variable: var by var          | 3                  | env-set-lookup(5), progress-env(7), progress(13), subsumption-env(7), preservation-env-lookup(7)                                                                                                   |
| [12](https://bitbucket.org/fkomauli/list-machine/branch/bug12#diff "Mutation 12") | Variable: var by var          | 4                  | env-set-lookup(3), preservation(13)                                                                                                                                                                |
| [13](https://bitbucket.org/fkomauli/list-machine/branch/bug13#diff "Mutation 13") | Statement: del clause         | 1                  | lub-subtype-left(3), lub-subtype-right(3)                                                                                                                                                          |
| [14](https://bitbucket.org/fkomauli/list-machine/branch/bug14#diff "Mutation 14") | Statement: del clause         | 1                  | lub_idem(1), lub_of_subtype(1)                                                                                                                                                                     |
| [15](https://bitbucket.org/fkomauli/list-machine/branch/bug15#diff "Mutation 15") | Variable: const by const      | 0                  | preservation(13)                                                                                                                                                                                   |
| [16](https://bitbucket.org/fkomauli/list-machine/branch/bug16#diff "Mutation 16") | Constant: constructor by var  | 1                  | preservation(13)                                                                                                                                                                                   |
| [17](https://bitbucket.org/fkomauli/list-machine/branch/bug17#diff "Mutation 17") | Variable: var by var          | 1                  |                                                                                                                                                                                                    |
| [18](https://bitbucket.org/fkomauli/list-machine/branch/bug18#diff "Mutation 18") | Statement: del pred in clause | 0                  | preservation-block-typing(4), preservation-program-typing(12), preservation-step-branch-taken(12), preservation(12), run-prog-finite-well-typed(12), run-well-typed(12), safety(12), soundness(12) |
| [19](https://bitbucket.org/fkomauli/list-machine/branch/bug19#diff "Mutation 19") | Constant: const by anonym var | 0                  | soundness(19)                                                                                                                                                                                      |
| [20](https://bitbucket.org/fkomauli/list-machine/branch/bug20#diff "Mutation 20") | Statement: del pred in clause | 0                  | run-prog-finite-well-typed(13), run-well-typed(13), safety(13), soundness(13)                                                                                                                      |


| Check                              | Mutation id                        |
|------------------------------------|------------------------------------|
| var-set-lookup                     | 2                                  |
| var-lookup-uniq                    |                                    |
| env-lookup-uniq T                  |                                    |
| env-lookup-uniq G                  |                                    |
| env-set-good                       |                                    |
| env-set-lookup                     | 11, 12                             |
| env-lookup removes variable        | 10                                 |
| store-has-type-set-diff            |                                    |
| var-set-lookup-diff                |                                    |
| var-set-type                       |                                    |
| block-typing-lookup-ok             |                                    |
| block-typing-lookup-uniq           | 5                                  |
| step_deterministic R               | 2, 4                               |
| step_deterministic I               |                                    |
| progress-typing-dom-match          | 5                                  |
| progress-check-program             | 5                                  |
| progress-env                       | 11                                 |
| progress-var-bind-set              |                                    |
| progress-var-set                   |                                    |
| progress-branch                    |                                    |
| progress-instr                     |                                    |
| progress                           | 1, 7, 9, 11                        |
| lub-comm                           |                                    |
| lub-subtype-left                   | 13                                 |
| lub-subtype-right                  | 13                                 |
| lub-least                          |                                    |
| subtype-list-invert                |                                    |
| lub_idem                           | 14                                 |
| lub_of_subtype                     | 14                                 |
| subsumption                        |                                    |
| subsumption-env                    | 11                                 |
| subsumption-env2                   |                                    |
| preservation-env-lookup            | 11                                 |
| preservation-block-typing          | 18                                 |
| preservation-program-typing        | 18                                 |
| preservation                       | 2, 3, 6, 12, 15, 16, 18            |
| preservation-step-branch-not-taken |                                    |
| preservation-step-branch-taken     | 18                                 |
| run-prog-finite-well-typed         | 18, 20                             |
| run-well-typed                     | 18, 20                             |
| check-instr-seq-invert             |                                    |
| check-instr2-invert                |                                    |
| check-instr-branch-list-invert     |                                    |
| check-instr-branch-listcons-invert |                                    |
| check-instr-branch-nil-invert      |                                    |
| check-instr-fetch-field-0-invert   |                                    |
| safety                             | 9, 18, 20                          |
| soundness                          | 1, 2, 3, 5, 6, 7, 8, 9, 18, 19, 20 |


No counterexample is found for mutation 17.
Mutation 17 is killed by unit tests, but not by checks.


### Soundness

Below are listed the counterexamples found by the soundness check for the various mutations.

| Mutation id | Depth | Program                                                                                 | Program typing                          | Instruction                                                                      | Next instruction                                                                 | Store                  | Updated store                                               |
|-------------|-------|-----------------------------------------------------------------------------------------|-----------------------------------------|----------------------------------------------------------------------------------|----------------------------------------------------------------------------------|------------------------|-------------------------------------------------------------|
| 1           | 19    | ```[(l0,seq(instr_cons(v0,v2,v0),instr_halt))]```                                       | ```[(l0,[(v0,ty_nil)])]```              | ```seq(instr_cons(v0,v2,v0),instr_halt)```                                       | ```seq(instr_cons(v0,v2,v0),instr_halt)```                                       | ```[(v0,value_nil)]``` | ```[(v0,value_nil)]```                                      |
| 2           | 23    | ```[(l0,seq(seq(instr_cons(v0,v0,v0),instr_fetch_field(v0,zf,v0)),instr_halt))]```      | ```[(l0,[(v0,ty_nil)])]```              | ```seq(seq(instr_cons(v0,v0,v0),instr_fetch_field(v0,zf,v0)),instr_halt)```      | ```seq(instr_fetch_field(v0,zf,v0),instr_halt)```                                | ```[(v0,value_nil)]``` | ```[(v0,value_nil),(v0,value_cons(value_nil,value_nil))]``` |
| 3           | 23    | ```[(l0,seq(seq(instr_cons(v0,v0,v0),instr_fetch_field(v0,zf,v0)),instr_halt))]```      | ```[(l0,[(v0,ty_nil)])]```              | ```seq(seq(instr_cons(v0,v0,v0),instr_fetch_field(v0,zf,v0)),instr_halt)```      | ```seq(instr_fetch_field(v0,zf,v0),instr_halt)```                                | ```[(v0,value_nil)]``` | ```[(v0,value_nil)]```                                      |
| 5           | 25    | ```[(l0,instr_jump(l1)),(l0,instr_halt)]```                                             | ```[(l0,[(v0,ty_nil)]),(l0,[])]```      | ```instr_jump(l1)```                                                             | ```instr_jump(l1)```                                                             | ```[(v0,value_nil)]``` | ```[(v0,value_nil)]```                                      |
| 6           | 23    | ```[(l0,seq(seq(instr_cons(v0,v0,v0),instr_fetch_field(v0,zf,v0)),instr_halt))]```      | ```[(l0,[(v0,ty_nil)])]```              | ```seq(seq(instr_cons(v0,v0,v0),instr_fetch_field(v0,zf,v0)),instr_halt)```      | ```seq(instr_halt,instr_fetch_field(v0,zf,v0))```                                | ```[(v0,value_nil)]``` | ```[(v0,value_cons(value_nil,value_nil))]```                |
| 7           | 23    | ```[(l0,seq(seq(instr_cons(v0,v0,v0),instr_fetch_field(v0,of,v0)),instr_halt))]```      | ```[(l0,[(v0,ty_nil)])]```              | ```seq(seq(instr_cons(v0,v0,v0),instr_fetch_field(v0,of,v0)),instr_halt)```      | ```seq(instr_fetch_field(v0,of,v0),instr_halt)```                                | ```[(v0,value_nil)]``` | ```[(v0,value_cons(value_nil,value_nil))]```                |
| 8           | 23    | ```[(l0,seq(seq(instr_cons(v0,v0,v0),instr_fetch_field(v0,zf,v0)),instr_halt))]```      | ```[(l0,[(v0,ty_nil)])]```              | ```seq(seq(instr_cons(v0,v0,v0),instr_fetch_field(v0,zf,v0)),instr_halt)```      | ```seq(instr_fetch_field(v0,zf,v0),instr_halt)```                                | ```[(v0,value_nil)]``` | ```[(v0,value_nil)]```                                      |
| 9           | 13    | ```[(l0,instr_halt)]```                                                                 | ```[(l0,[(v0,ty_nil)])]```              | ```instr_halt```                                                                 | ```instr_halt```                                                                 | ```[(v0,value_nil)]``` | ```[(v0,value_nil)]```                                      |
| 18          | 12    | ```[(l0,seq(instr_halt,seq(seq(seq(instr_halt,instr_halt),instr_halt),instr_halt)))]``` | ```[(l0,[(v0,ty_nil)])]```              | ```seq(instr_halt,seq(seq(seq(instr_halt,instr_halt),instr_halt),instr_halt))``` | ```seq(instr_halt,seq(seq(seq(instr_halt,instr_halt),instr_halt),instr_halt))``` | ```[(v0,value_nil)]``` | ```[(v0,value_nil)]```                                      |
| 19          | 19    | ```[(l0,seq(instr_cons(v2,v2,v2),instr_halt))]```                                       | ```[(l0,[(v2,ty_nil)])]```              | ```seq(instr_cons(v2,v2,v2),instr_halt)```                                       | ```seq(instr_cons(v2,v2,v2),instr_halt)```                                       | ```[(v0,value_nil)]``` | ```[(v0,value_nil)]```                                      |
| 20          | 13    | ```[(l0,seq(instr_fetch_field(v2,zf,v2),instr_halt))]```                                | ```[(l0,[(v2,ty_listcons(T14037))])]``` | ```seq(instr_fetch_field(v2,zf,v2),instr_halt)```                                | ```seq(instr_fetch_field(v2,zf,v2),instr_halt)```                                | ```[(v0,value_nil)]``` | ```[(v0,value_nil)]```                                      |


### Preservation

Below are listed the counterexamples found by the preservation check for the various mutations.

| Mutation id | Depth | Program                                                                                                 | Program typing             | Instruction                                                                        | Next instruction                                                                                 | Environment                                               | Store                                                                             | Updated store                                               |
|-------------|-------|---------------------------------------------------------------------------------------------------------|----------------------------|------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------|-----------------------------------------------------------|-----------------------------------------------------------------------------------|-------------------------------------------------------------|
| 2           | 13    | ```[(l0,instr_halt)]```                                                                                 | ```[(l0,[(v0,ty_nil)])]``` | ```seq(instr_cons(v2,v2,v2),seq(instr_fetch_field(v2,zf,v2),instr_halt))```        | ```seq(instr_fetch_field(v2,zf,v2),instr_halt)```                                                | ```[(v2,ty_nil)]```                                       | ```[(v2,value_nil)]```                                                            | ```[(v2,value_nil),(v2,value_cons(value_nil,value_nil))]``` |
| 3           | 13    | ```[(l0,instr_halt)]```                                                                                 | ```[(l0,[(v0,ty_nil)])]``` | ```seq(instr_cons(v2,v2,v2),seq(instr_fetch_field(v2,zf,v2),instr_halt))```        | ```seq(instr_fetch_field(v2,zf,v2),instr_halt)```                                                | ```[(v2,ty_nil)]```                                       | ```[(v2,value_nil)]```                                                            | ```[(v2,value_nil)]```                                      |
| 6           | 13    | ```[(l0,instr_halt)]```                                                                                 | ```[(l0,[(v0,ty_nil)])]``` | ```seq(seq(instr_cons(v2,v2,v2),instr_fetch_field(v2,zf,v2)),instr_halt)```        | ```seq(instr_cons(v2,v2,v2),seq(instr_halt,instr_fetch_field(v2,zf,v2)))```                      | ```[(v2,ty_nil)]```                                       | ```[(v2,value_nil)]```                                                            | ```[(v2,value_nil)]```                                      |
| 12          | 13    | ```[(l0,instr_halt)]```                                                                                 | ```[(l0,[(v0,ty_nil)])]``` | ```seq(instr_fetch_field(v0,zf,v1),seq(instr_fetch_field(v1,zf,v0),instr_halt))``` | ```seq(instr_fetch_field(v1,zf,v0),instr_halt)```                                                | ```[(v0,ty_listcons(ty_nil)),(v1,ty_listcons(ty_nil))]``` | ```[(v0,value_cons(value_nil,value_nil)),(v1,value_cons(value_nil,value_nil))]``` | ```[(v0,value_cons(value_nil,value_nil)),(v1,value_nil)]``` |
| 15          | 13    | ```[(l0,instr_halt)]```                                                                                 | ```[(l0,[(v0,ty_nil)])]``` | ```seq(instr_cons(v2,v2,v2),seq(instr_fetch_field(v2,zf,v2),instr_halt))```        | ```seq(instr_fetch_field(v2,zf,v2),instr_halt)```                                                | ```[(v2,ty_nil)]```                                       | ```[(v2,value_nil)]```                                                            | ```[(v2,value_cons(value_nil,value_nil))]```                |
| 16          | 13    | ```[(l0,instr_halt)]```                                                                                 | ```[(l0,[(v0,ty_nil)])]``` | ```seq(instr_fetch_field(v0,of,v0),instr_jump(l0))```                              | ```instr_jump(l0)```                                                                             | ```[(v0,ty_listcons(ty_nil))]```                          | ```[(v0,value_cons(value_nil,value_cons(value_nil,value_nil)))]```                | ```[(v0,value_cons(value_nil,value_nil))]```                |
| 18          | 12    | ```[(l0,seq(seq(seq(seq(seq(instr_halt,instr_halt),instr_halt),instr_halt),instr_halt),instr_halt))]``` | ```[(l0,[(v0,ty_nil)])]``` | ```seq(instr_branch_if_nil(v0,l0),instr_halt)```                                   | ```seq(seq(seq(seq(seq(instr_halt,instr_halt),instr_halt),instr_halt),instr_halt),instr_halt)``` | ```[(v0,ty_nil)]```                                       | ```[(v0,value_nil)]```                                                            | ```[(v0,value_nil)]```                                      |
