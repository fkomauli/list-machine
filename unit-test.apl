%% Test taken from
%% https://github.com/racket/redex/blob/master/redex-examples/redex/examples/list-machine/test.rkt

%% Commented out tests should fail.


? var_lookup([(X,value_nil)],X) = value_nil.
? not(not_same_var(X,Y), var_lookup([(X,value_nil)],Y) = A).
? not(not_same_var(X,Y), not(var_lookup([(X,value_nil),(Y,value_cons(value_nil,value_nil))],Y) = value_cons(value_nil,value_nil))).

? var_set([],X,value_nil) = [(X,value_nil)].
? var_set([(X,value_nil)],X,value_cons(value_nil,value_nil)) = [(X,value_cons(value_nil,value_nil))].
? not(not_same_var(X,Y), not(var_set([(X,value_nil)],Y,value_cons(value_nil,value_nil)) = [(X,value_nil),(Y,value_cons(value_nil,value_nil))])).

? program_lookup([(L1,instr_halt)],L1) = instr_halt.
? not(not_same_label(L1,L2), not(program_lookup([(L1,instr_halt),(L2,seq(instr_halt,instr_halt))],L2) = seq(instr_halt,instr_halt))).

? step([],
       [],
       seq(seq(instr_jump(l0),instr_jump(l1)),instr_jump(l2)),
       [],
       seq(instr_jump(l0),seq(instr_jump(l1),instr_jump(l2)))).
? not(not_same_var(X,Y), not(step([],
       [(X,value_cons(value_cons(value_nil,value_nil),value_nil)),(Y,value_nil)],
       seq(instr_fetch_field(X,zf,Y),instr_halt),
       [(X,value_cons(value_cons(value_nil,value_nil),value_nil)),(Y,value_cons(value_nil,value_nil))],
       instr_halt))).
? not(not_same_var(X,Y), not(step([],
       [(X,value_cons(value_nil,value_cons(value_nil,value_nil))),(Y,value_nil)],
       seq(instr_fetch_field(X,of,Y),instr_halt),
       [(X,value_cons(value_nil,value_cons(value_nil,value_nil))),(Y,value_cons(value_nil,value_nil))],
       instr_halt))).
? not(not_same_var(X,Y), not_same_var(X,Z), not_same_var(Y,Z), not(step([],
       [(X,value_nil),(Y,value_nil)],
       seq(instr_cons(X,Y,Z),instr_halt),
       [(X,value_nil),(Y,value_nil),(Z,value_cons(value_nil,value_nil))],
       instr_halt))).
? step([],
       [(X,value_cons(value_nil,value_nil))],
       seq(instr_branch_if_nil(X,L),instr_halt),
       [(X,value_cons(value_nil,value_nil))],
       instr_halt).
? step([(L,seq(instr_halt,instr_halt))],
       [(X,value_nil)],
       seq(instr_branch_if_nil(X,L),instr_halt),
       [(X,value_nil)],
       seq(instr_halt,instr_halt)).
? step([(L,seq(instr_halt,instr_halt))],
       [(X,value_nil)],
       instr_jump(L),
       [(X,value_nil)],
       seq(instr_halt,instr_halt)).

? env_lookup([(X,ty_nil)],X) = (ty_nil,_).
? not(not_same_var(X,Y), env_lookup([(X,ty_nil)],Y) = (T,_)).
? not(not_same_var(X,Y), not(env_lookup([(X,ty_nil),(Y,ty_list(ty_nil))],Y) = (ty_list(ty_nil),_))).

? env_set([],X,ty_nil) = [(X,ty_nil)].
? env_set([(X,ty_nil)],X,ty_list(ty_nil)) = [(X,ty_list(ty_nil))].
? not(not_same_var(X,Y), not(env_set([(X,ty_nil)],Y,ty_list(ty_nil)) = [(X,ty_nil),(Y,ty_list(ty_nil))])).

? subtype(ty_nil,ty_nil).
? subtype(ty_nil,ty_list(ty_nil)).
? subtype(ty_list(ty_nil),ty_list(ty_list(ty_nil))).
? subtype(ty_listcons(ty_nil),ty_list(ty_list(ty_nil))).
? subtype(ty_listcons(ty_nil),ty_listcons(ty_list(ty_nil))).

%% Here should be a random test that is already implemented in check.apl
%% #check "lub-least" 6 :  T3 = lub(T1,T2), subtype(T1,T4), subtype(T2,T4) => subtype(T3,T4).

? env_sub([(X,ty_nil)],[(X,ty_nil)]).
? env_sub([(X,ty_nil)],[(X,ty_list(ty_nil))]).
? not(not_same_var(X,Y), not(env_sub([(X,ty_nil),(Y,ty_list(ty_nil))],[(X,ty_list(ty_nil))]))).
? not(not_same_var(X,Y), not(env_sub([(Y,ty_list(ty_nil)),(X,ty_nil)],[(X,ty_list(ty_nil))]))).

? check_instr([(L0,[(X,ty_list(ty_nil))])],
              [(X,ty_list(ty_nil))],
              instr_branch_if_nil(X,L0),
              [(X,ty_listcons(ty_nil))]).
? check_instr([(L0,[(X,ty_nil)])],
              [(X,ty_listcons(ty_nil))],
              instr_branch_if_nil(X,L0),
              [(X,ty_listcons(ty_nil))]).
? check_instr([(L0,[(X,ty_nil)])],
              [(X,ty_nil)],
              instr_branch_if_nil(X,L0),
              [(X,ty_nil)]).
? not(not_same_var(X,Y), not(check_instr([],
              [(X,ty_listcons(ty_nil))],
              instr_fetch_field(X,zf,Y),
              [(X,ty_listcons(ty_nil)),(Y,ty_nil)]))).
? not(not_same_var(X,Y), not(check_instr([],
              [(X,ty_listcons(ty_nil))],
              instr_fetch_field(X,of,Y),
              [(X,ty_listcons(ty_nil)),(Y,ty_list(ty_nil))]))).
? not(not_same_var(X,Y), not_same_var(X,Z), not_same_var(Y,Z), not(check_instr([],
              [(X,ty_listcons(ty_nil)),(Y,ty_list(ty_list(ty_nil))),(Z,ty_nil)],
              instr_cons(X,Y,Z),
              [(X,ty_listcons(ty_nil)),(Y,ty_list(ty_list(ty_nil))),(Z,ty_listcons(ty_list(ty_nil)))]))).
? not(not_same_var(V0,X), not_same_var(V0,F), not_same_var(X,F), check_instr([(L0,[(V0,ty_nil)])],
              [(V0,ty_nil)],
              instr_cons(V0,X,F),
              G)).

? check_block([],[],instr_halt).
? not(not_same_var(X,Y), not(check_block([],[(X,ty_listcons(ty_nil))],seq(instr_fetch_field(X,zf,Y),instr_halt)))).
? check_block([(L0,[])],[(X,ty_nil)],instr_jump(L0)).

? check_blocks([],[]).
? check_blocks([(L0,[])],[(L0,instr_halt)]).

? check_program([(l0,instr_halt)],[(l0,[(v0,ty_nil)])]).
