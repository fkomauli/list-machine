%% grammar accepts words with same # of a and b
%% implementation is wrong: b 2 W = [a,a,b] accepted found by sound



:- discontiguous pred/1.
:- dynamic assumable/1.
:- consult("../sabr").
%DPGraph non needed since only sound fails
% assumable(ss(_X)).
% assumable(bb(_X)).
% assumable(aa(_X)).
add_assumption(A, Ass, [A|Ass]).

% S := . | bA | aB
pred(ss(_)).
ss([]) :-rule(s1).
ss([b|W]) :- rule(s2), aa(W).   
ss([a|W]) :- rule(s3),bb(W).

% A := aS | bAA
pred(aa(_)).
aa([a|W]) :-rule(a1), ss(W).
aa([b|VW]) :- rule(a2), aa(V),aa(W), append(V,W,VW). 

% B := bS | aBB
pred(bb(_)).
bb([b|W]) :-rule(b1), ss(W).
bb([a|VW]) :- rule(b2), bb(V),bb(V), append(V,W,VW). % BUG V must be W 

count(_,[],z).
count(X,[X|XS],s(N)) :- !, count(X,XS,N).
count(X,[_|XS],N) :- count(X,XS,N).
     

/*
?- explain(ss([a,a,b]),A).

Proof tree:
ss([a,a,b]) true for rule s3, since:
  bb([a,b]) true for rule b2, since:
    bb([b]),bb([b]),append([b],[],[b]) proved by AND-rule, since:
      [
      bb([b]) true for rule b1, since:
        ss([])  proved for rule s1, since:

      E
      bb([b]) true for rule b1, since:
        ss([])  proved for rule s1, since:

      E
      append([b],[],[b])  proved by system call
      ]


it's s3, b2, b1
*/

% rules: rule(s1)), rule(b1)) rule(b2)), rule(s3))
