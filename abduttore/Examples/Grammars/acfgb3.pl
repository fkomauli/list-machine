%% grammar accepts words with same # of a and b
%% implementation is wrong
%% b 3


:- discontiguous pred/1.
:- dynamic assumable/1.
:- dynamic trusted/1.
:- consult("../sabr").
%DPGraph s <--> b,   a <--> s  b <--> b
assumable(ss(_X)).
assumable(bb(_X)).
assumable(aa(_X)).
add_assumption(A, Ass, [A|Ass]).

% S := . | bA | aB
pred(ss(_)).
ss([]) :-rule(s1).
ss([b|W]) :- rule(s2), aa(W).  
ss([a|W]) :- rule(s3),bb(W).

% A := aS | bAA

pred(aa(_)).
aa([a|W]) :-rule(a1), ss(W).
%aa([b|VW]) :- rule(a2), aa(V),aa(W), append(V,W,VW). % BUG, omitted

% B := bS | aBB

pred(bb(_)).
bb([b|W]) :-rule(b1), ss(W).
bb([a|VW]) :- rule(b2), bb(V),bb(W), append(V,W,VW).

count(_,[],z).
count(X,[X|XS],s(N)) :- !, count(X,XS,N).
count(X,[_|XS],N) :- count(X,XS,N).
     
%% This should not fail: ss([b,b,a,a]). 

 /*
 explain(ss([b,b,a,a]),A).

Proof tree:
ss([b,b,a,a]) true for rule s2, since:
  aa([b,a,a])  proved, since assumed

There is no clause matching that head
*/
