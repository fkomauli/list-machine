%% grammar accepts words with same # of a and b
%% implementation is wrong: b 1


:- discontiguous pred/1.
:- dynamic assumable/1.
:- dynamic trusted/1.
:- consult("../sabr").
%DPGraph s -- b, s --s,  a -> b HERE
% assumable(ss(_X)).
assumable(bb(_X)).
assumable(aa(_X)).
add_assumption(A, Ass, [A|Ass]).
trusted(ss(_X)).
% S := . | bA | aB
pred(ss(_)).
ss([]) :-rule(s1).
ss([b|W]) :- rule(s2), ss(W).   
ss([a|W]) :- rule(s3),bb(W).

% A := aS | bAA
pred(aa(_)).
aa([a|W]) :-rule(a1), ss(W).
aa([b|VW]) :- rule(a2), aa(V),aa(W), append(V,W,VW). 

% B := bS | aBB
pred(bb(_)).
bb([b|W]) :-rule(b1), ss(W).
bb([a|VW]) :- rule(b2), bb(V),bb(W), append(V,W,VW).

count(_,[],z).
count(X,[X|XS],s(N)) :- !, count(X,XS,N).
count(X,[_|XS],N) :- count(X,XS,N).
     
%% soundness fails proves ss[b], it should not.
%% It's equality so investigate ss
% explain(ss([b]),[]).
/*
Proof tree:
ss([b]) vero for rule s2, since:
  ss([])  proved for rule s1.


quindi è s2
*/


%% does not accept ss([b,a]), it should - prove(ss([b,a]), Ass,_).
%% Ass = [bb([])]
