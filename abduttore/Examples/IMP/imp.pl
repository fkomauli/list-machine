%% Aithor: AM
%% First we introduce IMP (a la Nipkow)

:- discontiguous pred/1.
:- dynamic assumable/1.
:- dynamic trusted/1.
:- consult("../sabr").

/*
% aexp
id: name_type.
aexp : type.

c : nat -> aexp.
var : id -> aexp.
plus : (aexp,aexp) -> aexp.

% the state
type state = [(id,nat)].
*/

pred(aval(_,_,_)).
aval(c(N),_, N) :-rule(aconst).
aval(var(X),S,N) :- rule(avar), member((X,N),S).
aval(plus(A1,A2),St,N) :-
    rule(aplus),
    aval(A1,St,N1),
    aval(A2,St,N2), 
    N is N1 + N2.


% bexp 
/*
bexp : type.

b : mbool -> bexp .
less : (aexp, aexp) -> bexp.
cnot : bexp -> bexp.
cand : (bexp,bexp) -> bexp.


% commands
cmd : type.
skip : cmd.
assn : (id , aexp) -> cmd.
seq : (cmd , cmd) -> cmd. 
ift : (bexp,cmd,cmd) -> cmd.
whi : (bexp,cmd) -> cmd.
*/

% 0 for true anything else for false
pred(bval(_,_,_)).
bval(b(B),_, B) :-
    rule(bconst).
bval(cnot(B),S,BN) :-
    rule(bnot),
    bval(B,S,VB),
    VB =:= 0 -> BN = 1; BN = 0.
bval(cand(B1,B2),S, BA) :-
    rule(band),
    bval(B1,S,V1), 
    bval(B2,S,V2),
    B is V1 + V2,
    B =:= 0 -> BA = 0; BA = 1.
bval(less(A1,A2),S, BL) :-
    rule(bless),
   aval(A1,S,V1), 
   aval(A2,S,V2),
   V1 < V2 -> BL = 0; BL = 1.


pred(upd(_,_,_,_)).

upd([],X,N, [(X,N)]) :-
    rule(uemp).
upd([(X,_)|St],X,N, UpSt) :-
    rule(u2), UpSt = [(X,N)|St]. 
upd([(X,Nx)|St],Y,N,[(X,Nx)|Stn]):-
    rule(u2), upd(St,Y,N,Stn).


pred(eval(_S,_E,_NS)).
eval(S,skip,S) :-
    rule(evskip).

eval(S,assn(X,A), US) :-
    rule(evassn), aval(A,S,Va), upd(S,X,Va,US).

eval(S1,seq(C1,C2),S3) :-
    rule(evseq), eval(S1,C1,S2),eval(S2,C2,S3).

eval(S,ift(B,C1,C2),Sn)  :- 
  rule(evif),
  bval(B,S, Res),
  (Res =:= 0 -> eval(S,C1,Sn) ; eval(S,C2,Sn)).

eval(S1,whi(B,C),S2) :-
    rule(evwhile),
    bval(B,S1, Res), 
   (Res > 0 -> S1 = S2 ; 
   eval(S1,C,Sn),eval(Sn,whi(B,C),S2)).


%%%% security types

%% assigning sec levels to a(b)exp

% type sectx = [(id,level)].
pred(asec(_, _,_)).
asec(c(_N), _,0) :-
    rule(asecc).
asec(var(X),T, L):-
     rule(asecv),member((X,L),T).
asec(plus(A1,A2), T,L) :- 
    rule(asecplus),
    asec(A1,T, L1), 
    asec(A2,T,L2),  
    max(L1,L2,L).

pred(bsec(_, _,_)).
bsec(b(_N), _T, 0) :-
    rule(bsecc).
bsec(cnot(B),T, L) :-
    rule(bsecn),bsec(B,T,L).
bsec(less(A1,A2),T, L) :- 
    rule(bsecl),
    asec(A1,T,L1),
    asec(A2,T,L2), 
    max(L1,L2,L).
bsec(cand(B1,B2),T, L) :-
    rule(bsecand),
    bsec(B1,T, L1),
    bsec(B2,T, L2),  
    max(L1,L2,L).


% I could make it into a predicate sectx
pred(secty(_,_,_))-

secty(_,_,skip) :-
    rule(secskip).

secty(L,T,assn(X,A)) :-
        rule(secassn),
	member((X,LX),T), 
	asec(A, T, LA), 
	LA =< LX,L =< LX.

secty(L,T,seq(C1,C2)) :-
    rule(secseq),
    secty(L,T,C1),secty(L,T,C2).

secty(L,T,ift(B,C1,C2)) :- 
    rule(secif),
    bsec(B,T, Lb), 
    max(Lb,L,Max), 
    secty(Max,T, C1),
    secty(Max,T, C2).

secty(L,T,whi(B,C)) :-
        rule(secif),
	bsec(B,T, Lb),
	max(Lb,L,Max),
	secty(Max,T, C). 


% pred(same_dom(state,state,sectx)).
% considered system
same_dom([],[],[]).
same_dom([(X,_)|S],[(X,_)|Sn],[(X,_)|T]) :- 
  same_dom(S,Sn,T). 


pred(simll(_,_,_,_)).
simll([],_,_,[]).
simll([(X,V)|S1],L,T,[(X,V)|S2]) :-
    member((X,Lx),T),
    Lx < L, 
    simll(S1,L,T,S2).
simll([(X,_)|S1],L,T,[(X,_)|S2]) :-
    member((X,Lx),T),
    L =< Lx,
    simll(S1,L,T,S2).

pred(simle(_,_,_,_)).
simle([],_,_,[]).
simle([(X,V1)|S1],L,T,[(X,V2)|S2]) :-
  rule(simle2),
  member((X,Lx),T),
  Lx =<L -> (V1 = V2,   simle(S1,L,T,S2))
            ; simle(S1,L,T,S2).
