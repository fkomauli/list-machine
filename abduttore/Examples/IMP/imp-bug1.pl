%% Author: AM: volpano sec system
%% BUG 1

:- discontiguous pred/1.
:- dynamic assumable/1.
:- dynamic trusted/1.
:- consult("../sabr").

%rule(_).
assumable(simll(_,_,_,_)).
assumable(simle(_,_,_,_)).
trusted(upd(_,_,_,_)).
add_assumption(A, Ass, [A|Ass]).
/*
% aexp
id: name_type.
aexp : type.

c : nat -> aexp.
var : id -> aexp.
plus : (aexp,aexp) -> aexp.

% the state
type state = [(id,nat)].
*/

pred(aval(_,_,_)).
aval(c(N),_, N) :-rule(aconst).
aval(var(X),S,N) :- rule(avar), member((X,N),S).
aval(plus(A1,A2),St,N) :-
    rule(aplus),
   aval(A1,St,N1),
   aval(A2,St,N2), 
   N is N1 + N2.


% bexp 
/*
bexp : type.

b : mbool -> bexp .
less : (aexp, aexp) -> bexp.
cnot : bexp -> bexp.
cand : (bexp,bexp) -> bexp.


% commands
cmd : type.
skip : cmd.
assn : (id , aexp) -> cmd.
seq : (cmd , cmd) -> cmd. 
ift : (bexp,cmd,cmd) -> cmd.
whi : (bexp,cmd) -> cmd.
*/

% 0 for true anything else for false
pred(bval(_,_,_)).
bval(b(B),_, B) :-
    rule(bconst).
bval(cnot(B),S,BN) :-
    rule(bnot),
    bval(B,S,VB),
    VB =:= 0 -> BN = 1; BN = 0.
bval(cand(B1,B2),S, BA) :-
    rule(band),
    bval(B1,S,V1), 
    bval(B2,S,V2),
    B is V1 + V2,
    B =:= 0 -> BA = 0; BA = 1.
bval(less(A1,A2),S, BL) :-
    rule(bless),
   aval(A1,S,V1), 
   aval(A2,S,V2),
   V1 < V2 -> BL = 0; BL = 1.


pred(upd(_,_,_,_)).

upd([],X,N, [(X,N)]) :-
    rule(uemp).
upd([(X,_)|St],X,N, UpSt) :-
    rule(u2), UpSt = [(X,N)|St]. 
upd([(X,Nx)|St],Y,N,[(X,Nx)|Stn]):-
    rule(u2), upd(St,Y,N,Stn).


pred(eval(_S,_E,_NS)).
eval(S,skip,S) :-
    rule(evskip).

eval(S,assn(X,A), US) :-
    rule(evassn), aval(A,S,Va), upd(S,X,Va,US).

eval(S1,seq(C1,C2),S3) :-
    rule(evseq), eval(S1,C1,S2),eval(S2,C2,S3).

eval(S,ift(B,C1,C2),Sn)  :- 
  rule(evif),
  bval(B,S, Res),
  (Res =:= 0 -> eval(S,C1,Sn) ; eval(S,C2,Sn)).

eval(S1,whi(B,C),S2) :-
    rule(evwhile),
    bval(B,S1, Res), 
   (Res > 0 -> S1 = S2 ; 
   eval(S1,C,Sn),eval(Sn,whi(B,C),S2)).


%%%% security types

%% assigning sec levels to a(b)exp

% type sectx = [(id,level)].
pred(asec(_, _,_)).
asec(c(_N), _,0) :-
    rule(asecc).
asec(var(X),T, L):-
     rule(asecv),member((X,L),T).
asec(plus(A1,A2), T,L) :- 
    rule(asecplus),
    asec(A1,T, L1), 
    asec(A2,T,L2),  
    max(L1,L2,L).

pred(bsec(_, _,_)).
bsec(b(_N), _T, 0) :-
    rule(bsecc).
bsec(cnot(B),T, L) :-
    rule(bsecn),bsec(B,T,L).
bsec(less(A1,A2),T, L) :- 
    rule(bsecl),
    asec(A1,T,L1),
    asec(A2,T,L2), 
    max(L1,L2,L).
bsec(cand(B1,B2),T, L) :-
    rule(bsecand),
    bsec(B1,T, L1),
    bsec(B2,T, L2),  
    max(L1,L2,L).


% I could make it into a predicate sectx
pred(secty(_,_,_)).

secty(_,_,skip) :-
    rule(secskip).

secty(L,T,assn(X,A)) :-
        rule(secassn),
	member((X,LX),T), 
	asec(A, T, LA), 
	LA =< LX,L =< LX.

secty(L,T,seq(C1,_C2)) :-
    rule(secseq),
    secty(L,T,C1).

secty(L,T,ift(B,C1,C2)) :- 
    rule(secif),
    bsec(B,T, Lb), 
    max(Lb,L,Max), 
    secty(Max,T, C1),
    secty(Max,T, C2).

secty(L,T,whi(B,C)) :-
        rule(secif),
	bsec(B,T, Lb),
	max(Lb,L,Max),
	secty(Max,T, C). 


% pred(same_dom(state,state,sectx)).
% considered system
same_dom([],[],[]).
same_dom([(X,_)|S],[(X,_)|Sn],[(X,_)|T]) :- 
  same_dom(S,Sn,T). 


pred(simll(_,_,_,_)).
simll([],_,_,[]) :-
    rule(simlnil).
simll([(X,V)|S1],L,T,[(X,V)|S2]) :-
    rule(siml1),
    member((X,Lx),T),
    Lx < L,
    !,
    simll(S1,L,T,S2).
simll([(X,_)|S1],L,T,[(X,_)|S2]) :-
    rule(siml2),
    member((X,Lx),T),
    L =< Lx,
    simll(S1,L,T,S2).

pred(simle(_,_,_,_)).
simle([],_,_,[])  :-  rule(simlnil).
simle([(X,V1)|S1],L,T,[(X,V2)|S2]) :-
  rule(simle2),
  member((X,Lx),T),
  Lx =< L -> (V1 = V2,   simle(S1,L,T,S2))
            ; simle(S1,L,T,S2).


/*

confinement: eval(S,C,S1), (secty(L,T,C), same_dom(S,S1,T)) => simll(S,L,T,S1)
confinement
Checking depth 1 2 3 4 5
Total: 0.036693 s:
C = seq(skip,assn(X29170,c(sc(z)))) 
L = sc(sc(sc(sc(z)))) 
S = [(X29170,sc(sc(z)))] 
S1 = [(X29170,sc(z))] 
T = [(X29170,sc(sc(z)))] 

C = seq(skip,assn(x,c(1))), 
L = 4,
S = [(x,2)] ,
S1 = [(x, 1)], 
T = [(x,2)], 
eval(S,C,S1).

 C = seq(skip,assn(x,c(1))), 
L = 4,
S = [(x,2)] ,
S1 = [(x, 1)], 
T = [(x,2)], 
explain((eval(S,C,S1), secty(L,T,C), same_dom(S,S1,T)),[]).

eval([(x,2)],seq(skip,assn(x,c(1))),[(x,1)]),secty(4,[(x,2)],seq(skip,assn(x,c(1)))),same_dom([(x,2)],[(x,1)],[(x,2)]) for AND-rule, since:
  eval([(x,2)],seq(skip,assn(x,c(1))),[(x,1)]) for rule evseq, since:
    eval([(x,2)],skip,[(x,2)]),eval([(x,2)],assn(x,c(1)),[(x,1)]) for AND-rule, since:
      eval([(x,2)],skip,[(x,2)]) for rule evskip
      &
      eval([(x,2)],assn(x,c(1)),[(x,1)]) for rule evassn, since:
        aval(c(1),[(x,2)],1),upd([(x,2)],x,1,[(x,1)]) for AND-rule, since:
          aval(c(1),[(x,2)],1) for rule aconst
          &
          upd([(x,2)],x,1,[(x,1)]) trusted pred
  &
  secty(4,[(x,2)],seq(skip,assn(x,c(1)))) for rule secseq, since:
    secty(4,[(x,2)],skip) for rule secskip    ^^^^^^^^^^^
  &
  same_dom([(x,2)],[(x,1)],[(x,2)]) by system call


--------
Checking for counterexamples to 
ni: secty(z,T,C), (eval(S,C,S1), (eval(Tau,C,Tau1), (same_dom(S,Tau,T), (same_dom(S,S1,T), simle(S,L,T,Tau))))) => simle(S1,L,T,Tau1)
ni
Checking depth 1 2 3 4 5 6 7 8
Total: 7.372505 s:
C = seq(skip,assn(X2506616,var(X2506294))) 
L = z 
S = [(X2506294,sc(z)),(X2506616,_2498287)] 
S1 = [(X2506294,sc(z)),(X2506616,sc(z))] 
T = [(X2506294,sc(sc(sc(z)))),(X2506616,z)] 
Tau = [(X2506294,z),(X2506616,_2498287)] 
Tau1 = [(X2506294,z),(X2506616,z)] 


C = seq(skip,assn(x,var(y))), 
L = 0 ,
S = [(y,1),(x,_)] ,
S1 = [(y,1),(x,1)] ,
T = [(y,4),(x,0)] ,
Tau = [(y,0),(x,_)], 
Tau1 = [(y,0),(x,0)],
explain((secty(z,T,C), eval(S,C,S1), eval(Tau,C,Tau1), same_dom(S,Tau,T), same_dom(S,S1,T)),[]).

secty(z,[...|...],seq(...,...)),eval(...,...,...),...,... for AND-rule, since:
  secty(z,[(...,...)|...],seq(skip,assn(...,...))) for rule secseq, since:
    secty(z,[(...,...)|...],skip) for rule secskip
  &
  eval([(...,...)|...],seq(skip,assn(...,...)),[(...,...)|...]) for rule evseq, since:
    eval([...|...],skip,[...|...]),eval([...|...],assn(...,...),[...|...]) for AND-rule, since:
      eval([(...,...)|...],skip,[(...,...)|...]) for rule evskip
      &
      eval([(...,...)|...],assn(x,var(...)),[(...,...)|...]) for rule evassn, since:
        aval(var(...),[...|...],1),upd([...|...],x,1,[...|...]) for AND-rule, since:
          aval(var(y),[(...,...)|...],1) for rule avar, since:
            member((y,1),[(...,...)|...]) by system call
          &
          upd([(...,...)|...],x,1,[(...,...)|...]) trusted pred
  &
  eval([(...,...)|...],seq(skip,assn(...,...)),[(...,...)|...]) for rule evseq, since:
    eval([...|...],skip,[...|...]),eval([...|...],assn(...,...),[...|...]) for AND-rule, since:
      eval([(...,...)|...],skip,[(...,...)|...]) for rule evskip
      &
      eval([(...,...)|...],assn(x,var(...)),[(...,...)|...]) for rule evassn, since:
        aval(var(...),[...|...],0),upd([...|...],x,0,[...|...]) for AND-rule, since:
          aval(var(y),[(...,...)|...],0) for rule avar, since:
            member((y,0),[(...,...)|...]) by system call
          &
          upd([(...,...)|...],x,0,[(...,...)|...]) trusted pred
  &
  same_dom([(...,...)|...],[(...,...)|...],[(...,...)|...]) by system call
  &
  same_dom([(...,...)|...],[(...,...)|...],[(...,...)|...]) by system call


*/
