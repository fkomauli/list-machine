:- use_module("../../abr").
abr:assumable(step(_,_,_,_,_)).
%abr:assumable(check_program(_,_)).
%abr:assumable(not_same_var(_,_)).
%%abr: assumable(var_lookup(_R,_V2,_A2)).

% abr:    var_set(_R,_V3,_,_R1).
%abr:assumable(program_lookup(_,_)).
abr:add_assumption(A, Ass, [A|Ass]).

% can use NF and member for lookups (if we trust them

%pred not_same_var (var, var).
not_same_var(v0, v1).
not_same_var(v0, v2).
not_same_var(v1, v0).
not_same_var(v1, v2).
not_same_var(v2, v0).
not_same_var(v2, v1).


%% pred not_same_label (label, label).
not_same_label(l0, l1).
not_same_label(l0, l2).
not_same_label(l1, l0).
not_same_label(l1, l2).
not_same_label(l2, l0).
not_same_label(l2, l1).


%func var_lookup (store,var) = value.
var_lookup([(V,A)|_R],V, A) :-rule(vl1).
var_lookup([(V,_A)|R],V1, A1) :- rule(vl2), not_same_var(V,V1), var_lookup(R,V1,A1).


%func var_set (store,var,value) = store.
var_set([],V,A,  [(V,A)]).
var_set([(V,_A)|R],V,A1,  [(V,A1)|R]).
var_set([(V,A)|R],V1,A1,  [(V,A)|R1]) :-
    not_same_var(V,V1), var_set(R,V1,A1,R1).


%func program_lookup (program,label) = instr.
program_lookup([(L,I)|_P],L,  I).
program_lookup([(L,_I)|P],L1,  I1) :-
    not_same_label(L,L1), program_lookup(P,L1,I1).

%pred exists_program_lookup (program,label).
exists_program_lookup([(L,_I)|_P],L).
exists_program_lookup([(L,_I)|P],L1):-
    not_same_label(L,L1), exists_program_lookup(P,L1).


%pred step (program,store,instr,store,instr).
step(_P,R,seq(seq(I1,I2),I3),R,seq(I1,seq(I2,I3))):-rule(st1).
step(_P,R,seq(instr_fetch_field(V1,zf,V2),I),R1,I) :-
    rule(st2),
     var_lookup(R,V1,value_cons(A,_)),  var_set(R,V2,A,R1).
step(_P,R,seq(instr_fetch_field(V1,of,V2),I),R1,I) :-
        rule(st3),
     var_lookup(R,V1,value_cons(_,A)), var_set(R,V2,A,R1).
step(_P,R,seq(instr_cons(V1,V2,V3),I),R1,I) :-
        rule(st4),
    var_lookup(R,V1,A1),  var_lookup(R,V2,A2),
    var_set(R,V3,value_cons(A1,A2),R1).
step(_P,R,seq(instr_branch_if_nil(V,_L),I),R,I) :-
        rule(st5),
    var_lookup(R,V,value_cons(_,_)).
step(P,R,seq(instr_branch_if_nil(V,L),_I),R,I1) :-
        rule(st6),
    var_lookup(R,V, value_nil), program_lookup(P,L,I1).
step(P,R,instr_jump(L),R,I1) :-
        rule(st7),
    program_lookup(P,L,I1).


% pred steps (program,store,instr,store,instr).
steps(_P,R,I,R,I) :-    rule(sst1).
steps(P,R,I,R2,I2) :-
    rule(sst2),
    step(P,R,I,R1,I1), steps(P,R1,I1,R2,I2).


% pred step_or_halt (program,store,instr).
step_or_halt(_P,_R,instr_halt):-rule(soh1).
step_or_halt(P,R,I) :- rule(soh2), step(P,R,I,_,_).


%pred run (program,store,instr).
run(_P,_R,instr_halt).
run(P,R,I) :-
    step(P,R,I,R1,I1), run(P,R1,I1).


%func initial (program) = (store,instr).
initial(P, [(v0,value_nil)],I) :-
    program_lookup(P,l0,I).


%pred run_prog (program).
run_prog(P) :-
    initial(P,R,I), run(P,R,I).


%% from specification.elf
%func run_prog_finite (program) = (store,instr).
run_prog_finite(P,R,I) :-
    var_set([],v0,value_nil,R),
    program_lookup(P,l0,I).
run_prog_finite(P, R1,I1) :-
    step(P,R,I,R1,I1),
    run_prog_finite(P,R,I).

/* query:


P = [(l0,seq(instr_cons(v0,v2,v0),instr_halt))],
R1 = [(v0,value_nil)], 
I1 =  seq(instr_cons(v0,v2,v0),instr_halt),
prove( step_or_halt(P,R1,I1), As, PT), explain_pt(PT).


*/
