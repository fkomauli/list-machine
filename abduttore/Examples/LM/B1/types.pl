%% MUTATION 1

%% ty: type.
%% ty_nil: ty.
%% ty_list: ty -> ty.
%% ty_listcons: ty -> ty.

%% type env = [(var,ty)].


%% pred new_var (var,env).
new_var(_,[]).
new_var(V,[(V1,_T)|G]) :-
    not_same_var(V,V1), new_var(V,G).


%pred env_ok (env).
env_ok([]).
env_ok([(V,_T)|G]) :-
    new_var(V,G), env_ok(G).


%func env_lookup (env,var) = (ty,env).
env_lookup([(V,T)|G],V,T,G).
env_lookup([(V,T)|G],V1,T1,[(V,T)|G1]) :-
    not_same_var(V,V1), env_lookup(G,V1,T1,G1).


%func env_set(env,var,ty) = env.
env_set([(V,_T)|G],V,T1,[(V,T1)|G]).
env_set([(V,T)|G],V1,T1,[(V,T)|G1]) :-
    not_same_var(V,V1), env_set(G,V1,T1,G1).
env_set([],V,T, [(V,T)]).


% pred subtype (ty,ty).
subtype(T,T).
subtype(ty_nil,ty_list(_)).
subtype(ty_list(T),ty_list(T1)) :-
    subtype(T,T1).
subtype(ty_listcons(T),ty_list(T1)) :-
    subtype(T,T1).
subtype(ty_listcons(T),ty_listcons(T1)) :-
    subtype(T,T1).


%pred env_sub (env,env).
env_sub(_,[]).
env_sub(G,[(V,T)|G1]) :-
    env_lookup(G,V,T1,G1),
    subtype(T1,T),
    env_sub(G1,G1).


%func lub (ty,ty) = ty.
lub(T,T,T).
lub(ty_list(T1),ty_nil,ty_list(T1)).
lub(ty_list(T1),ty_listcons(T2),ty_list(T3)) :- lub(T1,T2,T3).
lub(ty_listcons(T1),ty_list(T2),ty_list(T3)) :- lub(T1,T2,T3).
lub(ty_list(T1),ty_list(T2),ty_list(T3)) :- lub(T1,T2,T3).
lub(ty_nil,ty_list(T1),ty_list(T1)).
lub(ty_nil,ty_listcons(T1),ty_list(T1)).
lub(ty_listcons(T1),ty_nil,ty_list(T1)).
lub(ty_listcons(T1),ty_listcons(T2),ty_listcons(T3)) :- lub(T1,T2,T3).


%pred value_has_ty (value,ty).
value_has_ty(value_nil,ty_nil).
value_has_ty(value_nil,ty_list(_)).
value_has_ty(value_cons(_,_),ty_listcons(_)).
value_has_ty(V,ty_list(T)) :-
    value_has_ty(V,ty_listcons(T)).


%pred store_has_type (store,env).
store_has_type(_R,[]).
store_has_type(R,[(V,T)|G]) :-
    var_lookup(R,V,A),
    value_has_ty(A,T),
    store_has_type(R,G).


%pred exists_variable_with_type (store,var,ty).
exists_variable_with_type(R,V,T) :-
    var_lookup(R,V,A), value_has_ty(A,T).


%type program_typing = [(label,env)].

%func block_typing_lookup (program_typing,label) = env.
block_typing_lookup([(L,G)|_Pi],L,G) :-
    env_ok(G).
block_typing_lookup([(L,_G)|Pi],L1,G1) :-
    not_same_label(L,L1), block_typing_lookup(Pi,L1,G1).


%pred check_instr (program_typing,env,instr,env).
check_instr(Pi,G,seq(I1,I2),G2) :-
        rule(ci1),
    check_instr(Pi,G,I1,G1),
    check_instr(Pi,G1,I2,G2).
check_instr(Pi,G,instr_branch_if_nil(V,L),[(V,ty_listcons(T))|Gn]) :-
            rule(ci2),
    env_lookup(G,V,ty_list(T),Gn),
    block_typing_lookup(Pi,L,G1),
    env_sub([(V,ty_nil)|Gn],G1).
check_instr(Pi,G,instr_branch_if_nil(V,L),G) :-
            rule(ci3),
    env_lookup(G,V,ty_listcons(_),Gn),
    block_typing_lookup(Pi,L,G1),
    env_sub([(V,ty_nil)|Gn],G1).
check_instr(Pi,G,instr_branch_if_nil(V,L),G) :-
    rule(ci4),
    env_lookup(G,V,ty_nil,_Gn),
    block_typing_lookup(Pi,L,G1),
    env_sub(G,G1).
check_instr(_Pi,G,instr_fetch_field(V,zf,Vn),Gn) :-
                rule(ci5),
     env_lookup(G,V,ty_listcons(T)),
     env_set(G,Vn,T,Gn).
check_instr(_Pi,G,instr_fetch_field(V,of,Vn),Gn) :-
                rule(ci6),
    env_lookup(G,V,ty_listcons(T),_),
    env_set(G,Vn,ty_list(T).Gn).
check_instr(_Pi,G,instr_cons(V0,_V1,V),Gn) :-
                rule(ci7),
     env_lookup(G,V0,T0,_),
    env_lookup(G,V0,T1,_),
     lub(ty_list(T0),T1,T),
     env_set(G,V,ty_listcons(T),Gn).


%pred check_block (program_typing,env,instr).
check_block(_Pi,_G,instr_halt) :-     rule(cb1).
check_block(Pi,G,seq(I1,I2)) :-
        rule(cb2),
    check_instr(Pi,G,I1,G1),
    check_block(Pi,G1,I2).
check_block(Pi,G,instr_jump(L)) :-
        rule(cbs3),
    block_typing_lookup(Pi,L,G1), env_sub(G,G1).


%pred check_blocks (program_typing,program).
check_blocks(_Pi,[]) :-     rule(cbs1).
check_blocks(Pi,[(L,I)|P]) :-
    rule(cbs2),
    block_typing_lookup(Pi,L,G), check_block(Pi,G,I), check_blocks(Pi,P).


%pred typing_dom_match(program_typing,program).
typing_dom_match([(L,_G)],[(L,_I)]).
typing_dom_match([(L,_G)|Pi],[(L,_I)|P]) :- typing_dom_match(Pi,P).


%pred check_program (program,program_typing).
check_program(P,Pi) :-
        rule(cp),
    block_typing_lookup(Pi,l0,[(v0,ty_nil)]),
    typing_dom_match(Pi,P),
    check_blocks(Pi,P).

/* query:

Pi = [(l0,[(v0,ty_nil)])], 
P = [(l0,seq(instr_cons(v0,v2,v0),instr_halt))],
prove( check_program(P,Pi), As, PT), explain_pt(PT).


*/
