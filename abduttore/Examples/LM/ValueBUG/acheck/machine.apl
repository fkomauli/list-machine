value : type.
value_nil : value.
value_cons: (value, value) -> value.

%% /!\ using directly id and lab in predicates slows down the checks
%%     keeping a type layer makes things a lot faster
var : type.
v0 : var.
v1 : var.
v2 : var.

pred not_same_var (var, var).
not_same_var(v0, v1).
not_same_var(v0, v2).
not_same_var(v1, v0).
not_same_var(v1, v2).
not_same_var(v2, v0).
not_same_var(v2, v1).

label : type.
l0 : label.
l1 : label.
l2 : label.

pred not_same_label (label, label).
not_same_label(l0, l1).
not_same_label(l0, l2).
not_same_label(l1, l0).
not_same_label(l1, l2).
not_same_label(l2, l0).
not_same_label(l2, l1).


% for fetch, let's not use nats.
flag : type.
zf : flag.
of : flag.


instr : type.
instr_jump :          label -> instr.
instr_branch_if_nil : (var,label) -> instr.
instr_fetch_field :   (var,flag,var) -> instr.
instr_cons :          (var,var,var) -> instr.
instr_halt :          instr.
seq :                 (instr,instr) -> instr.

type block =   (label,instr).
type program = [(block)].

type store =   [(var,value)].
%% /!\ Using list constructors makes execution a lot slower
%%  store_empty :  store.
%%  store_bind :   (var,value,store) -> store.


func var_lookup (store,var) = value.
var_lookup([(V,A)|R],V) = A.
var_lookup([(V,A)|R],V') = A' :- not_same_var(V,V'), A' = var_lookup(R,V').


func var_set (store,var,value) = store.
var_set([],V,A) = [(V,A)].
var_set([(V,A)|R],V,A') = [(V,A')|R].
var_set([(V,A)|R],V',A') = [(V,A)|R'] :- not_same_var(V,V'), R' = var_set(R,V',A').


func program_lookup (program,label) = instr.
program_lookup([(L,I)|P],L) = I.
program_lookup([(L,I)|P],L') = I' :- not_same_label(L,L'), I' = program_lookup(P,L').

pred exists_program_lookup (program,label).
exists_program_lookup([(L,I)|P],L).
exists_program_lookup([(L,I)|P],L') :- not_same_label(L,L'), exists_program_lookup(P,L').


pred step (program,store,instr,store,instr).
step(P,R,seq(seq(I1,I2),I3),R,seq(I1,seq(I2,I3))).
step(P,R,seq(instr_fetch_field(V1,zf,V2),I),R',I) :- value_cons(A,_) = var_lookup(R,V1), R' = var_set(R,V2,A).
step(P,R,seq(instr_fetch_field(V1,of,V2),I),R',I) :- value_cons(_,A) = var_lookup(R,V1), R' = var_set(R,V2,A).
step(P,R,seq(instr_cons(V1,V2,V3),I),R',I) :- A1 = var_lookup(R,V1), A2 = var_lookup(R,V2), R' = var_set(R,V3,value_cons(A1,A2)).
step(P,R,seq(instr_branch_if_nil(V,L),I),R,I) :- var_lookup(R,V) = value_cons(_,_).
step(P,R,seq(instr_branch_if_nil(V,L),I),R,I') :- var_lookup(R,V) = value_nil, I' = program_lookup(P,L).
step(P,R,instr_jump(L),R,I') :- I' = program_lookup(P,L).


pred steps (program,store,instr,store,instr).
steps(P,R,I,R,I).
steps(P,R,I,R'',I'') :- step(P,R,I,R',I'), steps(P,R',I',R'',I'').


pred step_or_halt (program,store,instr).
step_or_halt(P,R,instr_halt).
step_or_halt(P,R,I) :- step(P,R,I,_,_).


pred run (program,store,instr).
run(P,R,instr_halt).
run(P,R,I) :- step(P,R,I,R',I'), run(P,R',I').


func initial (program) = (store,instr).
initial(P) = ([(v0,value_nil)], program_lookup(P,l0)).


pred run_prog (program).
run_prog(P) :- (R,I) = initial(P), run(P,R,I).


%% from specification.elf
func run_prog_finite (program) = (store,instr).
run_prog_finite(P) = (R,I) :- R = var_set([],v0,value_nil), I = program_lookup(P,l0).
run_prog_finite(P) = (R',I') :- step(P,R,I,R',I'), (R,I) = run_prog_finite(P).
