% the JAR BUG in value_has_type


:- discontiguous pred/1.
:- dynamic assumable/1.
:- consult("../../sabr").
assumable(store_has_type(_S,_V)).
assumable(value_has_type(_V,_T)).
assumable(check_instr(_Program_typing,_Env1,_Instr,_Env2)).
assumable(check_block(_Program_typing,_Env,_Instr)).
add_assumption(A, Ass, [A|Ass]).

% can use NF and member for lookups (if we trust them

%pred not_same_var (var, var).
not_same_var(v0, v1).
not_same_var(v0, v2).
not_same_var(v1, v0).
not_same_var(v1, v2).
not_same_var(v2, v0).
not_same_var(v2, v1).


%% pred not_same_label (label, label).
not_same_label(l0, l1).
not_same_label(l0, l2).
not_same_label(l1, l0).
not_same_label(l1, l2).
not_same_label(l2, l0).
not_same_label(l2, l1).

pred(var_lookup(_,_,_)).
%func var_lookup (store,var) = value.
var_lookup([(V,A)|_R],V, A) :-rule(vl1).
var_lookup([(V,_A)|R],V1, A1) :- rule(vl2), not_same_var(V,V1), var_lookup(R,V1,A1).


%func var_set (store,var,value) = store.
var_set([],V,A,  [(V,A)]).
var_set([(V,_A)|R],V,A1,  [(V,A1)|R]).
var_set([(V,A)|R],V1,A1,  [(V,A)|R1]) :-
    not_same_var(V,V1), var_set(R,V1,A1,R1).


%func program_lookup (program,label) = instr.
program_lookup([(L,I)|_P],L,  I).
program_lookup([(L,_I)|P],L1,  I1) :-
    not_same_label(L,L1), program_lookup(P,L1,I1).

%pred exists_program_lookup (program,label).
exists_program_lookup([(L,_I)|_P],L).
exists_program_lookup([(L,_I)|P],L1):-
    not_same_label(L,L1), exists_program_lookup(P,L1).

pred(step(_program,_store,_instr,_store,_instr)).
%pred step (program,store,instr,store,instr).
step(_P,R,seq(seq(I1,I2),I3),R,seq(I1,seq(I2,I3))):-rule(st1).
step(_P,R,seq(instr_fetch_field(V1,zf,V2),I),R1,I) :-
    rule(st2),
     var_lookup(R,V1,value_cons(A,_)),  var_set(R,V2,A,R1).
step(_P,R,seq(instr_fetch_field(V1,of,V2),I),R1,I) :-
        rule(st3),
     var_lookup(R,V1,value_cons(_,A)), var_set(R,V2,A,R1).
step(_P,R,seq(instr_cons(V1,V2,V3),I),R1,I) :-
        rule(st4),
    var_lookup(R,V1,A1),  var_lookup(R,V2,A2),
    var_set(R,V3,value_cons(A1,A2),R1).
step(_P,R,seq(instr_branch_if_nil(V,_L),I),R,I) :-
        rule(st5),
    var_lookup(R,V,value_cons(_,_)).
step(P,R,seq(instr_branch_if_nil(V,L),_I),R,I1) :-
        rule(st6),
    var_lookup(R,V, value_nil), program_lookup(P,L,I1).
step(P,R,instr_jump(L),R,I1) :-
        rule(st7),
    program_lookup(P,L,I1).

pred(steps(_program,_store,_instr,_store,_instr)).

% pred steps (program,store,instr,store,instr).
steps(_P,R,I,R,I) :-    rule(sst1).
steps(P,R,I,R2,I2) :-
    rule(sst2),
    step(P,R,I,R1,I1), steps(P,R1,I1,R2,I2).


pred(step_or_halt(_program,_store,_instr)).
% pred step_or_halt (program,store,instr).
step_or_halt(_P,_R,instr_halt):-rule(soh1).
step_or_halt(P,R,I) :- rule(soh2), step(P,R,I,_,_).


%pred run (program,store,instr).
run(_P,_R,instr_halt).
run(P,R,I) :-
    step(P,R,I,R1,I1), run(P,R1,I1).


%func initial (program) = (store,instr).
initial(P, [(v0,value_nil)],I) :-
    program_lookup(P,l0,I).


%pred run_prog (program).
run_prog(P) :-
    initial(P,R,I), run(P,R,I).


%% from specification.elf
%func run_prog_finite (program) = (store,instr).
run_prog_finite(P,R,I) :-
    var_set([],v0,value_nil,R),
    program_lookup(P,l0,I).
run_prog_finite(P, R1,I1) :-
    step(P,R,I,R1,I1),
    run_prog_finite(P,R,I).


%% types



%% pred new_var (var,env).
new_var(_,[]).
new_var(V,[(V1,_T)|G]) :-
    not_same_var(V,V1), new_var(V,G).


%pred env_ok (env).
env_ok([]).
env_ok([(V,_T)|G]) :-
    new_var(V,G), env_ok(G).


%func env_lookup (env,var) = (ty,env).
env_lookup([(V,T)|G],V,T,G).
env_lookup([(V,T)|G],V1,T1,[(V,T)|G1]) :-
    not_same_var(V,V1), env_lookup(G,V1,T1,G1).


%func env_set(env,var,ty) = env.
env_set([(V,_T)|G],V,T1,[(V,T1)|G]).
env_set([(V,T)|G],V1,T1,[(V,T)|G1]) :-
    not_same_var(V,V1), env_set(G,V1,T1,G1).
env_set([],V,T, [(V,T)]).


% pred subtype (ty,ty).
subtype(T,T).
subtype(ty_nil,ty_list(_)).
subtype(ty_list(T),ty_list(T1)) :-
    subtype(T,T1).
subtype(ty_listcons(T),ty_list(T1)) :-
    subtype(T,T1).
subtype(ty_listcons(T),ty_listcons(T1)) :-
    subtype(T,T1).


%pred env_sub (env,env).
env_sub(_,[]).
env_sub(G,[(V,T)|G1]) :-
    env_lookup(G,V,T1,G1),
    subtype(T1,T),
    env_sub(G1,G1).


%func lub (ty,ty) = ty.
lub(T,T,T).
lub(ty_list(T1),ty_nil,ty_list(T1)).
lub(ty_list(T1),ty_listcons(T2),ty_list(T3)) :- lub(T1,T2,T3).
lub(ty_listcons(T1),ty_list(T2),ty_list(T3)) :- lub(T1,T2,T3).
lub(ty_list(T1),ty_list(T2),ty_list(T3)) :- lub(T1,T2,T3).
lub(ty_nil,ty_list(T1),ty_list(T1)).
lub(ty_nil,ty_listcons(T1),ty_list(T1)).
lub(ty_listcons(T1),ty_nil,ty_list(T1)).
lub(ty_listcons(T1),ty_listcons(T2),ty_listcons(T3)) :- lub(T1,T2,T3).


pred(value_has_ty(_value,_ty)).
value_has_ty(value_nil,ty_nil) :-rule(vhtnln).
value_has_ty(value_nil,ty_list(_)) :-rule(vhtnl).
value_has_ty(value_cons(_,_),ty_listcons(_)) :-rule(vhtcons).
value_has_ty(V,ty_list(T)) :-
    rule(vht-rec),
    value_has_ty(V,ty_listcons(T)).


pred(store_has_type(_store,_env)).
store_has_type(_R,[]) :- rule(sht1).
store_has_type(R,[(V,T)|G]) :-
     rule(sht2),
    var_lookup(R,V,A),
    value_has_ty(A,T),
    store_has_type(R,G).


%pred exists_variable_with_type (store,var,ty).
exists_variable_with_type(R,V,T) :-
    var_lookup(R,V,A), value_has_ty(A,T).


%type program_typing = [(label,env)].

%func block_typing_lookup (program_typing,label) = env.
block_typing_lookup([(L,G)|_Pi],L,G) :-
    env_ok(G).
block_typing_lookup([(L,_G)|Pi],L1,G1) :-
    not_same_label(L,L1), block_typing_lookup(Pi,L1,G1).


pred(check_instr(_program_typing,_env,_instr,_env)).
%pred check_instr (program_typing,env,instr,env).
check_instr(Pi,G,seq(I1,I2),G2) :-
        rule(ci1),
    check_instr(Pi,G,I1,G1),
    check_instr(Pi,G1,I2,G2).
check_instr(Pi,G,instr_branch_if_nil(V,L),[(V,ty_listcons(T))|Gn]) :-
            rule(ci2),
    env_lookup(G,V,ty_list(T),Gn),
    block_typing_lookup(Pi,L,G1),
    env_sub([(V,ty_nil)|Gn],G1).
check_instr(Pi,G,instr_branch_if_nil(V,L),G) :-
            rule(ci3),
    env_lookup(G,V,ty_listcons(_),Gn),
    block_typing_lookup(Pi,L,G1),
    env_sub([(V,ty_nil)|Gn],G1).
check_instr(Pi,G,instr_branch_if_nil(V,L),G) :-
    rule(ci4),
    env_lookup(G,V,ty_nil,_Gn),
    block_typing_lookup(Pi,L,G1),
    env_sub(G,G1).
check_instr(_Pi,G,instr_fetch_field(V,zf,Vn),Gn) :-
                rule(ci5),
     env_lookup(G,V,ty_listcons(T),_), %%%3
     env_set(G,Vn,T,Gn).
check_instr(_Pi,G,instr_fetch_field(V,of,Vn),Gn) :-
                rule(ci6),
    env_lookup(G,V,ty_listcons(T),_),
    env_set(G,Vn,ty_list(T).Gn).
check_instr(_Pi,G,instr_cons(V0,V1,V),Gn) :-
                rule(ci7),
     env_lookup(G,V0,T0,_),
    env_lookup(G,V1,T1,_),
     lub(ty_list(T0),T1,T),
     env_set(G,V,ty_listcons(T),Gn).

pred(check_block(_program_typing,_env,_instr)).
%pred check_block (program_typing,env,instr).
check_block(_Pi,_G,instr_halt) :-     rule(cb1).
check_block(Pi,G,seq(I1,I2)) :-
        rule(cb2),
    check_instr(Pi,G,I1,G1),
    check_block(Pi,G1,I2).
check_block(Pi,G,instr_jump(L)) :-
        rule(cbs3),
    block_typing_lookup(Pi,L,G1), env_sub(G,G1).

pred(check_blocks(_program_typing,_program)).
%pred check_blocks (program_typing,program).
check_blocks(_Pi,[]) :-     rule(cbs1).
check_blocks(Pi,[(L,I)|P]) :-
    rule(cbs2),
    block_typing_lookup(Pi,L,G), check_block(Pi,G,I), check_blocks(Pi,P).


%pred typing_dom_match(program_typing,program).
typing_dom_match([(L,_G)],[(L,_I)]).
typing_dom_match([(L,_G)|Pi],[(L,_I)|P]) :- typing_dom_match(Pi,P).


pred(check_program(_program,_program_typing)).
%pred check_program (program,program_typing).
check_program(P,Pi) :-
        rule(cp),
    block_typing_lookup(Pi,l0,[(v0,ty_nil)]),
    typing_dom_match(Pi,P),
    check_blocks(Pi,P).

/* query:



*/
