%%  TAPL's typed arithmetic language chap 8
%% AM: DEBUGGED VERSION


:- discontiguous pred/1.
:- dynamic assumable/1.
:- dynamic trusted/1.
:- consult("../sabr").


%assumable(exists_step(_)).
%assumable(value(_)).
%assumable(nvalue(_)).
%assumable(bvalue(_)).
add_assumption(A, Ass, [A|Ass]).

pred(bvalue(_)).
bvalue(ttrue):-rule(bt).
bvalue(tfalse):-rule(bv).

pred(nvalue(_)).
nvalue(tzero):-rule(nz).
nvalue(tsucc(T)) :- rule(ns),nvalue(T).

pred(value(_)).
value(V) :- bvalue(V) ; nvalue(V).


pred(step(_,_)).
step(tif(ttrue,T1, _T2),T1):-rule(sift).
step(tif(tfalse, _T1, T2),T2):-rule(siff).
step(tif(T1, T2, T3),tif(T11, T2, T3)) :-rule(sifc), step(T1,T11).
step(tsucc(T),tsucc(T1)) :-rule(ssuc), step(T,T1).
step(tpred(tzero),tzero):-rule(spredz).
step(tpred(tsucc(V)),V) :- rule(spreds),nvalue(V).
step(tpred(T),tpred(T1)):- rule(spredc), step(T,T1).
step(tiszero(tzero),ttrue):-rule(siszz).
step(tiszero(tsucc(V)),tfalse):- rule(siszs), nvalue(V).
step(tiszero(T),tiszero(T1)):- rule(siszc), step(T,T1).

pred(has_type(_,_)).
has_type(ttrue,tbool):-rule(ht).
has_type(tfalse,tbool):-rule(hf).
has_type(tif(T1, T2, T3),A):- rule(hif),
    has_type(T1, tbool), has_type(T2,A),has_type(T3,A).
has_type(tzero,tnat):-rule(hz).

has_type(tsucc(T), tnat):- rule(hs), has_type(T,tnat).
has_type(tpred(T), tnat):- rule(hp), has_type(T,tnat).
has_type(tiszero(T), tbool):- rule(hisz), has_type(T,tnat).


pred(exists_step(_)).
exists_step(tif(ttrue(_T1, _T2))) :-rule(ext).
exists_step(tif(tfalse(_T1, _T2))):-rule(exd).
exists_step(tif(T1, _T2, _T3)):- rule(exif),exists_step(T1).
exists_step(tsucc(T)):- rule(exs), exists_step(T).
exists_step(tpred(tzero)):-rule(exp).
exists_step(tpred(tsucc(V))) :- rule(exps), nvalue(V).
exists_step(tpred(T)):- rule(expp), exists_step(T).
exists_step(tiszero(tzero)):-rule(exisz).
exists_step(tiszero(tsucc(V))):- rule(exiszs),nvalue(V).
exists_step(tiszero(T)):- rule(exiszc), exists_step(T).

pred(nstuck(_)).
nstuck(V) :- rule(sv), value(V).
nstuck(M) :- rule(ss), exists_step(M).

