%%  TAPL's typed arithmetic language var 8 (mine): removed cong rule for tif 
%%  in exists_step
/*
progress: has_type(E,T) => nstuck(E)
progress
Checking depth 1 2 3 4 5
Total: 0.003521 s:
E = tif(tiszero(tzero))(ttrue)(ttrue) 
T = tbool 
 prove(nstuck(tif(tiszero(tzero),ttrue,ttrue)), Abd, P).
*/

:- discontiguous pred/1.
:- dynamic assumable/1.
:- dynamic trusted/1.
:- consult("../sabr").
% we abduct on leaves of DP tree
assumable(exists_step(_)).
assumable(nvalue(_)).
assumable(bvalue(_)).
add_assumption(A, Ass, [A|Ass]).

pred(bvalue(_)).
bvalue(ttrue):-rule(bt).
bvalue(tfalse):-rule(bv).

pred(nvalue(_)).
nvalue(tzero):-rule(nz).
nvalue(tsucc(T)) :- rule(ns),nvalue(T).

pred(value(_)).
value(V) :- rule(valb), bvalue(V).
value(V) :- rule(valn), nvalue(V).


pred(step(_,_)).
step(tif(ttrue,T1, _T2),T1):-rule(sift).
step(tif(tfalse, _T1, T2),T2):-rule(siff).
step(tsucc(T),tsucc(T1)) :-rule(ssuc), step(T,T1).
step(tpred(tzero),tzero):-rule(spredz).
step(tpred(tsucc(V)),V) :- rule(spreds),nvalue(V).
step(tpred(T),tpred(T1)):- rule(spredc), step(T,T1).
step(tiszero(tzero),ttrue):-rule(siszz).
step(tiszero(tsucc(V)),tfalse):- rule(siszs), nvalue(V).
step(tiszero(T),tiszero(T1)):- rule(siszc), step(T,T1).

pred(has_type(_,_)).
has_type(ttrue,tbool):-rule(ht).
has_type(tfalse,tbool):-rule(hf).
has_type(tif(T1, T2, T3),A):- rule(hif),
    has_type(T1, tbool), has_type(T2,A),has_type(T3,A).
has_type(tzero,tnat):-rule(hz).
has_type(tsucc(T), tnat):- rule(hs), has_type(T,tnat).
has_type(tpred(T), tnat):- rule(hp), has_type(T,tnat).
has_type(tiszero(T), tbool):- rule(hisz), has_type(T,tnat).


pred(exists_step(_)).
exists_step(tif(ttrue,_T1, _T2)) :-rule(ext).
exists_step(tif(tfalse,_T1, _T2)):-rule(exd).
exists_step(tsucc(T)):- rule(exs), exists_step(T).
exists_step(tpred(tzero)):-rule(exp).
exists_step(tpred(tsucc(V))) :- rule(exps), nvalue(V).
exists_step(tpred(T)):- rule(expp), exists_step(T).
exists_step(tiszero(tzero)):-rule(exisz).
exists_step(tiszero(tsucc(V))):- rule(exiszs),nvalue(V).
exists_step(tiszero(T)):- rule(exiszc), exists_step(T).

pred(nstuck(_)).
nstuck(V) :- rule(sv), value(V).
nstuck(M) :- rule(ss), exists_step(M).



/*


?-  explain(nstuck(tif(tiszero(tzero),ttrue,ttrue)),A).

Proof tree:
nstuck(tif(tiszero(tzero),ttrue,ttrue)) for rule sv, since:
  value(tif(tiszero(tzero),ttrue,ttrue)) for rule valb, since:
    bvalue(tif(tiszero(tzero),ttrue,ttrue)), for assumed

A = [bvalue(tif(tiszero(tzero), ttrue, ttrue))] ;

Proof tree:
nstuck(tif(tiszero(tzero),ttrue,ttrue)) for rule sv, since:
  value(tif(tiszero(tzero),ttrue,ttrue)) for rule valn, since:
    nvalue(tif(tiszero(tzero),ttrue,ttrue)), for assumed

A = [nvalue(tif(tiszero(tzero), ttrue, ttrue))] ;

Proof tree:
nstuck(tif(tiszero(tzero),ttrue,ttrue)) for rule ss, since:
  exists_step(tif(tiszero(tzero),ttrue,ttrue)), for assumed

A = [exists_step(tif(tiszero(tzero), ttrue, ttrue))] ;
false.


The latter abducible makes sense and points to the lack of congruence rule for tif

exists_step(tif(T1, _T2, _T3)):- rule(exif),exists_step(T1).
 */
