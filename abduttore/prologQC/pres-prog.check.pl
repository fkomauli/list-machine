:- use_module(library(lists)).

random_mem(X, Xs) :-
        length(Xs, N),
        random_between(1, N, I),
        nth1(I, Xs, X).

arbitrary(ty, X) :-
    random_mem(X, [tnat,tbool]).

arbitrary(exp, X) :-
    arbitrary_sized(exp, X,10). % could be a flag


arbitrary_sized(exp, X, N) :-
    % [tzero,ttrue,tfalse,tsucc(N),tpred(N),tiszero(N),tif(_,_,_)]),
    random_member(Y,  [tzero,ttrue,tfalse,tsucc(N),tpred(N),tiszero(N),tif(_,_,_)]),
    functor(Y,F,Ar),
    (Ar =:= 0 -> X = Y
    ;
    Ar =:= 1 -> N1 is N - 1 , arbitrary_sized(exp, Z, N1), C =..[F,Z],
		% has_type(C,_),
		X  = C
    ;
    (Ar =:= 3 -> NS is N div 2, arbitrary_sized(exp,Z1,NS),arbitrary_sized(exp,Z2,NS), arbitrary_sized(exp,Z3,NS),
		 C =..[F,Z1,Z2,Z3],
		 % has_type(C,_),
		 X = C))
.


prop_progress(M: exp,T:ty) :-
    has_type(M,T),
    nstuck(M).

prop_pres(M) :-
    has_type(M,T),
    step(M, M1),
    has_type(M1,T).


call_prog :-
    quickcheck(prop_progress/2).

call_pres :-
    quickcheck(prop_pres/1).

/*
Note, arbitrary is just the randomized grammar of exp's. Too bad, not
typed. If we had randomized backtracking we could use directly has_type (perhaps
 for a given type.) A metainterpter with rule and random_permutation(+List, -Permutation)?

If I type check, random_member is deterministic, so if it fails it all does*/
