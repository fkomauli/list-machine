
% find_def(?Pred,+Defs).
 %make a list of Pred clauses (and print it). On backtracking, gives the whole program
find_def(Pred,Defs) :-
    pred(Pred),
    bagof((Pred :- B),clause(Pred,B),Defs),
%    maplist(portray_clause,Defs),
    true.

% % find_def(-Prog): retrives the whole program
find_prog(Prog) :-
    bagof(Defs,Pred^find_def(Pred,Defs),Prog).


rsolve(true).
rsolve(rule(_)) :-!.
rsolve((G1,G2)) :-
    !,
    rsolve(G1),rsolve(G2). % could be randomized

% 
rsolve(A) :-
    find_def(A,DefA),
    rsolve_def(A,DefA).

rsolve_def(A,DefA) :-
    random_member((AR :- Body), DefA),
    ( A = AR -> rsolve(Body)
    ;
    select((AR :- Body), DefA, DefLessAR),
    rsolve_def(A,DefLessAR)).

