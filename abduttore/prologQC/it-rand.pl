% bagof((value(V) :- B),clause(value(V),B),Defs),maplist(portray_clause,Defs).

% find_def(?Pred,+Defs).
%make a list of Pred clauses (and print it). On backtracking, gives the whole program
find_def(Pred,Defs) :-
    pred(Pred),
    bagof((Pred :- B),clause(Pred,B),Defs),
%    maplist(portray_clause,Defs),
    true.

% % find_def(-Prog): retrives the while program
find_prog(Prog) :-
    bagof(Defs,Pred^find_def(Pred,Defs),Prog).

rsolve(G) :-
    rsolve(G,100).
rsolve(true,_).
rsolve(rule(_),_) :-!.
rsolve((G1,G2),N) :-
    !,
    rsolve(G1,N),rsolve(G2.N). % could be randomized, perhaps turning conjunctions in lists

% 
rsolve(A,N) :-
    find_def(A,DefA),
    rsolve_def(A,DefA,N).

rsolve_def(A,DefA,N) :-
    N1 is N -1,
    random_member((AR :- Body), DefA),
    ( A = AR -> rsolve(Body,N1)
    ;
    select((AR :- Body), DefA, DefLessAR),
    rsolve(A,DefLessAR), N1). % could be N1



%% from MI
provable(true, _).
provable((A,B), Defs) :-
        provable(A, Defs),
        provable(B, Defs).
provable(g(Goal), Defs) :-
        (   predicate_property(Goal, built_in) ->
            call(Goal)
        ;   member(Def, Defs),
            copy_term(Def, Goal-Body),
            provable(Body, Defs)
        ).

%%
mi_clause(G, Ls) :-
        clause(G, Body),
        phrase(body_list(Body), Ls).


body_list(true)  --> [].
body_list((A,B)) -->
        body_list(A),
        body_list(B).
body_list(G) -->
        { G \= true },
        { G \= (_,_) },
        [G].

%% randomized conjunction
mi_list1([]).
mi_list1([G|Gs]) :-
        mi_clause(G, Body),
        mi_list1(Body),
        mi_list1(Gs).

mi_list2([]).
mi_list2([G0|Gs0]) :-
        mi_clause(G0, Body),
        append(Body, Gs0, Gs),
	random_permutation(Gs,Perm),
        mi_list2(Perm).


/*


?- has_type(M,tbool).
M = ttrue ;
M = tfalse ;
M = tif(ttrue, ttrue, ttrue) ;
M = tif(ttrue, ttrue, tfalse) ;
M = tif(ttrue, ttrue, tif(ttrue, ttrue, ttrue)) ;
M = tif(ttrue, ttrue, tif(ttrue, ttrue, tfalse)) ;
M = tif(ttrue, ttrue, tif(ttrue, ttrue, tif(ttrue, ttrue, ttrue))) ;
M = tif(ttrue, ttrue, tif(ttrue, ttrue, tif(ttrue, ttrue, tfalse))) a

?- has_type(M,tnat).
  C-c C-cAction (h for help) ? a
abort
% Execution Aborted
?- mi_list2([has_type(M,tbool)]).
M = ttrue ;
M = tfalse ;
M = tif(ttrue, ttrue, ttrue) ;
M = tif(tfalse, ttrue, ttrue) ;
M = tif(tif(ttrue, ttrue, ttrue), ttrue, ttrue) ;
M = tif(tif(tfalse, ttrue, ttrue), ttrue, ttrue) ;
M = tif(tif(tif(ttrue, ttrue, ttrue), ttrue, ttrue), ttrue, ttrue) ;
M = tif(tif(tif(ttrue, ttrue, tfalse), ttrue, ttrue), ttrue, ttrue) ;
M = tif(tif(tif(ttrue, ttrue, tif(ttrue, ttrue, ttrue)), ttrue, ttrue), ttrue, ttrue) ;
M = tif(tif(tif(ttrue, ttrue, tif(tfalse, ttrue, ttrue)), ttrue, ttrue), ttrue, ttrue) ;
M = tif(tif(tif(ttrue, ttrue, tif(tif(ttrue, ttrue, ttrue), ttrue, ttrue)), ttrue, ttrue), ttrue, ttrue) 

*/
