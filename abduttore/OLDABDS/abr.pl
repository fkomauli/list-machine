:- module(abr, [prove/3, prove/4, explain_pt/1, explain_pt/2, rule/1]).
:- multifile([
       type/1, pred/1, open_pred/1, open_type/1]).

/*-----------------------------------------------------------
abr :  abductive reasoner.   RAGIONAMENTO BASATO SU ASSUNZIONI
da usare con :-use_module(<path di abr>)

Le clausole meta-interpretate hanno la forma

<head> :- rule(<name>), <body>.
<head> :- rule(<name>).

Le assunzioni vanno dichiarate con

assumable(<atomica>).
     assumable(A):  quando il metainterprete incontra
     A la assume  usando add_assumption/3

askable(<atomica>).
     askable(A):   quando il metainterprete incontra
     A  esegue ask(A, Answer);
     assume A giustificata da Answer o fallisce, in
     base alla risposta dell'utente

add_assumption/3 � usato per aggiungere una assunzione
alle assunzioni correnti con chiamata:

 add_assumption(Ass, Correnti, Nuove)

Ass pu� essere aggiunta con o senza duplicati oppure
add_assumption pu� fallire in caso di inconsistenza;
tutto � deciso dal modulo utente che DEVE DEFINIRE
add_assumption/3 se ha degli assumibili


NOTA IMPORTANTE 1. Se si hanno askable, USARE UNA BASE DATI DELLE
RISPOSTE GIA' OTTENUTE, PER EVITARE DI PORRE ALL'UTENTE LA STESSA
DOMANDA PIU' VOLTE

Codice di esempio di ask, parziale

:- dynamic(asked/4).

ask(voto(Materia,Voto), Subst, Ok, Spiega) :-
	asked(voto(Materia,Voto), Subst, Ok, Spiega),
	% GIA' RISPOSTO,  CUT
	!.

ask(voto(Materia, Voto), Subst, Ok, Spiega) :-
	maplist(write, ['Inserire il voto di ', Materia,
			' o INVIO se non sostenuto\n--> ']),
	readln(Risp),
	( Risp=[N|_],
	  integer(N),
	  N > 17,
	  N < 32,!,
	  Ok = true,
	  Subst = [Voto = N]
	  ;
	  Ok = false,
	  Subst=[]
	),
	write('Vuoi inserire un commento? (scrivi il commento o INVIO)\n--> '),
	readln(Commento),
	atomic_list_concat(Commento, ' ', Spiega),
	assert(asked(voto(Materia, Voto), Subst, Ok, Spiega)).

NOTA IMPORTANTE 2. Oltre a =, si possono usare nel body delle regole
SOLO I METAPREDICATI PROLOG:
        not,
	call,
	catch,
	throw
Questo per obbligare a definire predicati corrispondenti a concetti
significativi nel problema; � comunque sempre possibile ricorrere
alla call.

Esempio.
    piu_giovane(X,Y) :- rule(eta),
	eta(X,E1),
	eta(Y,E2),
	X =< Y.

in fase di metainterpretazione da' luogo ad errore su X =< Y, che
non � consentito

SOLUZIONE 1:

   piu_giovane(X,Y) :- rule(eta),
        eta(X,E1),
	eta(Y,E2),
	eta_minore(X,Y).
   eta_minore(X,Y) :-
	% non � una regola, non viene metainterpretato
	% e non origina errori
	X =< Y.

SOLUZIONE 2:

   piu_giovane(X,Y) :- rule(eta),
            eta(X,E1),
            eta(Y,E2),
            call(X =< Y).

Usare la soluzione 1 se eta_minore(X,Y) � un concetto importante
nel dominio di problema.
Usare lasoluzione 2 se si tratta semplicemente di far ricorso
a Prolog.
*/

% I TIPI DI DATI -------------------------------------------
type(_:atomic).
%   formula atomica
type(_:body).
%   body di una clausola prolog, sintassi omessa
type(_:assumable).
%   una atomica assumibile, sintassi omessa
type([true,
      pt(atomic, true, call),
      pt(not(body), true, naf),
      pt(atomic, true, assumed),
      pt(atomic, true, answer(any)),
      pt(atomic, proof_tree, rule),
      pt((body, body), (proof_tree, proof_tree), and),
      pt((body ; body), proof_tree, or(integer)),
      pt((body -> body), proof_tree, if)
     ]:proof_tree).
%  pt(G, true, call):  G � un goal prolog riconosciuto
%     dal metainterprete, ed � stato risolto con
%     chiamata a prolog
%  pt(not(B), true, naf): la chiamata a prolog di B
%     � fallita (naf acronimo di negation as failure).
%     NOTARE:  not NON � meta-interpretato
%  pt(A,true,assumed):  A vera perch� assunta
%  pt(A,true, answer(S)):  A vera in base a risposta
%     dell'utente e S spiegazione fornita dall'utente
%  pt(A, PT, R): prova l'atomica A per la regola R
%     con sottoprova PT
%  pt((B1,B2),(PT1,PT2),and): prova (B1,B2) per and
%     con PT1 prova di B1 e PT2 prova B2
%  pt((B1;B2), PT, or(K)): prova (B1;B2) per or(K)
%     con PT prova B1, se K=1, o prova di B2, se K=2
%  pt((C -> B), PT, if): prova C->B
%     nel caso C vera, con PT prova di B
%     NOTARE:  C � una "guardia" che non viene
%     meta-interpretata, ma valutata con call a prolog


% ========================================================
% Predicati definiti da abr

pred(prove(body, list(assumable), proof_tree)).
%   prove(G, Ass, PT) significa:
%   il goal G � provabile con assunzioni Ass
%   e PT � il suo albero di prova
%   MODO  (+, -, -) nondet
pred(prove(body, list(assumable), list(assumable), proof_tree)).
%   prove(G, Ass1, Ass2, PT) significa:
%   il goal G � provabile con assunzioni Ass2 contenenti
%   Ass1 e altre assunzioni fatte durante la prova
%   e PT � il suo albero di prova
%   MODO  (+,+,-,-) nondet
pred(explain_pt(proof_tree)).
%  explain_pt(PT): stampa leggibile del proof-tree PT
%  MODO  (+) det
pred(explain_pt(proof_tree, integer)).
%  explain_pt(PT,K): stampa leggibile del proof-tree PT
%  con limite di profondit� K.
%  A profondit� K chiede se si vuole visualizzare
%  la sottoprova con:
%
%  "per la prova inserisci una prof.>0:"
%
%  - inserendo un intero, ad es.2, viene mostrata la
%    sottoprova fino a profondit� 2
%  - con qualunque altro isnerimento la sottoprova
%    viene saltata
%    MODO (+,+) INTERATTIVO



% ==============================================
% Predicati aperti di abr

open_pred(assumable(atomic)).
%  assumable(A):   A � assumibile
%  MODO    (?)
%  APERTO, OPZIONALE:  da definire se si hanno degli assumibili

open_pred(add_assumption(assumable, list(assumable), list(assumable))).
%  add_assumption(A, Ass, Ass1):   A � aggiunta ad Ass dando luogo ad
%  Ass1, se pu� essere aggiunta consistentemente; se non consistente
%  fallisce
%  MODO    (+,+,-)
%  APERTO, da definire se si � definito assumable
%

open_pred(askable(atomic)).
%   askable(G) :  si chiede all'utente una risposta al
%   goal G usando il predicato ask(G, Answer).
%   MODO  (?)
%   APERTO,  OPZIONALE:  da definire solo se si vuole
%   chiedere all'utente durante la meta-interpretazione
%
open_pred(ask(atomic, substitution, boolean, atom)).
%   ask(G, Subst, Ok, Spiega) chiede all'utente una risposta
%   Subst al goal ?- G(X1,..,Xn) e una eventuale spiegazione
%   -  Ok = false e Subst = [] oppure
%   -  Ok = true e Subst = [X1=Term1,...,Xn=Termn];
%   Spiega � una spiegazione (opzionale)
%   MODO   (+,-,-,-)
%   APERTO, da definire se si � definito askable




% ==========================================================
% Accesso alle regole del programma in esecuzione.

pred(rule(any)).
% always succeeds
rule(_).

pred(get_rule(rule,head, body)).
% get_rule(rule(R), Head, Body) vero sse nel
% programma in esecuzione c'� un modulo M
% che contiene una clausola da metainterpretare,
% con regola rule(R)
/*get_rule(rule(R), H, true) :-
	current_predicate(_,M:H),
	clause(M:H, rule(R)).
get_rule(rule(R), H, B) :-
	current_predicate(_,M:H),
	clause(M:H, (rule(R),B)).*/

get_rule(rule(R), H, B) :-
	current_predicate(_,M:H),
	clause(M:H, BR),
	(    BR = rule(R), !, B=true
	;   BR = (rule(R),B)).

%  DEFAULTS:  true non deve essere chiesto o assunto
askable(true) :-
	fail.
assumable(true) :-
	fail.

%=============================================================
% Il metainterprete.
%
% Per l'implementazione di prove vedi il codice.
% prove usa prolog_goal e user_goal , vedi section(goals).


prove(H, Ass, PT) :-
	prove_body(H, [], Ass, PT).
prove(H, Ass1, Ass2, PT) :-
	prove_body(H, Ass1, Ass2, PT).



prove_head(true, Ass, Ass, true) :- !.
prove_head(!,_,_,_) :-
	throw('IL CUT NON E'' METAINTERPRETATO').
prove_head(not(H), Ass, Ass, pt(not(H), true, naf)) :- !,
	not(H).
prove_head(H, Ass, Ass,  pt(H, true, answer(Answer))) :-
	askable(H), !,
	ask(H, Subst, true, Answer),
	apply_subst(Subst).
prove_head(H, Ass, Ass, pt(H, true, prolog_goal)) :-
      prolog_goal(H),!,
      H.
prove_head(H, Ass, Ass, pt(H, true, user_goal)) :-
    user_goal(H, M),
    !,
    writeln(called(M:H)),
    call(M:H).
prove_head(H, Ass, NewAss,  pt(H, true, assumed)) :-
    assumable(H),
        not(get_rule(_, H, _Body)),
	!,
	writeln(add(H)),
	add_assumption(H, Ass, NewAss).



%prove_head(H, Ass, Ass,  pt(H, true, R)) :-
%	get_rule(R, H, true).

prove_head(H, Ass, NewAss,  PT) :-
	get_rule(R, H, Body),
	(   Body=true, !,
	    PT = pt(H, true, R)
	;   prove_body(Body, Ass, NewAss, SubProofs),
	    PT = pt(H, SubProofs, R)).

%prove_body(H, Ass, NewAss, PT) :-
%	system_goal(H), !,
%	do(H, Ass,NewAss,PT).

prove_body(H, Ass, NewAss, PT) :-
	is_atomic(H),!,
	prove_head(H, Ass, NewAss, PT).
prove_body((A,B), Ass, NewAss, pt((A,B), (P1,P2), and)) :-
	prove_body(A, Ass, Ass1, P1),
	prove_body(B, Ass1, NewAss, P2).
prove_body((A;B), Ass, NewAss, pt((A;B), PA, or(1))) :-
	prove_body(A, Ass, NewAss, PA).
prove_body((A;B), Ass, NewAss, pt((A;B), PB, or(2))) :-
	prove_body(B, Ass, NewAss, PB).
prove_body((A->B), Ass, NewAss, pt((A->B), PB, if)) :-
	call(A), !,
	% NB:   A non viene meta-interpretato
	% � trattato come un test puro
	prove_body(B, Ass, NewAss, PB).

is_atomic(H) :-
	not(H=(_,_)),
	not(H=(_->_)),
	not(H=(_;_)).
apply_subst([X=T|S]) :-
	X=T, !,
	apply_subst(S).
apply_subst([]).


%==============================================================
%  Spiegazioni dal proof-tree.  --------------------------------------
%  Implementa explain_pt/1 e explain_pt/2
%

explain_pt(PT) :-
	nb_setval(depth, 10000),
	expl_pt(0, PT).


explain_pt(PT, N) :-
	nb_setval(depth, N),
	expl_pt(0, PT).


pred(expl_pt(integer, proof_tree)).
%   AUSILIARIO
%   expl_pt(K, PT): PT ha profondit� K e viene visualizzato a livello di
%   indentazione K
%
expl_pt(K, pt(H, true, R)):- !,
       explanation(R,Expl),
       indent(K, [H, '  vero'|Expl]),
       nl.
expl_pt(K, pt(H, SubPt, R)) :-
       nb_getval(depth,D),
       K+1 > D,!,
       indent(K, [H, ' vero per	', R, ';\n'] ),
       (   get_answer(K,['per la prova inserisci una prof.>0: '], DD) ->
	   D1 is D+DD,
	   nb_setval(depth,D1),
	   expl_pt(K, pt(H, SubPt, R)),
	   nb_setval(depth,D)
       ;   indent(K, ['... salto\n'] )
       ).
expl_pt(K, pt(H, (P1,P2), R)):- !,
       indent(K, [H, ' vero per	AND, perche :\n'] ),
       indent(K,['  [\n']),
       expl_and_pt(K+1,pt(H, (P1,P2), R)),
       indent(K,['  ]\n']).
expl_pt(K, pt((A;B), P, or(I))):- !,
       extract_or_pt(pt((A;B), P, or(I)), PT, 1, J),
       indent(K, [(A;B), ' vero per OR(',J,'), perche :\n']),
       expl_pt(K+1, PT).
expl_pt(K, pt((A->B), P, if)):- !,
       indent(K, [(A -> B), ' vero perche ', A, ' vero e:\n']),
       expl_pt(K+1,P).
expl_pt(K, pt(H, P, R)):-
       explanation(R,Expl),
       indent(K, [H, ' vero'|Expl]),!,
       expl_pt(K+1,P).

pred(extract_or_pt(proof_tree, proof_tree, integer, integer)).
%   AUSILIARIO
%   extract_or_pt(pt((A1;A2;..;An), PTS, or(_)), PTk, I, J):
%      PTS contiene la prova PTk del disgiunto Ak e J = I+k
%   Modo:   (+,-,+,-) det.

extract_or_pt(pt((A;_B), pt(A, PA, R), or(1)), pt(A, PA, R), I, I) :- !.
extract_or_pt(pt((_A;B), pt(B, PP, R), or(2)), PT, I,J) :-
	I1 is I+1,
	extract_or_pt(pt(B, PP, R), PT, I1, J).

pred(expl_and_pt(integer, proof_tree)).
%   AUSILIARIO
%   expl_and_pt(K, pt((A1,A2,..,An), PTS, and)):
%   PTS contiene le sottoprove P1,P2,..,Pn di
%   A1, A2, ..., An
%   Le relative spiegazioni vengono visualizzate
%   tutte allo stesso livello K
%   Modo:   (+,+) det.

expl_and_pt(K, pt((_,_),(P1,P2),and)) :-!,
	expl_pt(K, P1),
	indent(K,['E\n']),
	expl_and_pt(K, P2).
expl_and_pt(K, pt(A, P, R)) :-
	expl_pt(K,pt(A, P, R)).

pred(explanation(rule, list(atom))).
%  explanation(R, L):  L � la lista di atomi che
%  vengono concatenati in uscita con maplist(write,L)
%  e forniscono la spiegazione all'utente di R
%
explanation(rule(R),[' for rule ', R, ', since:\n']):-!.
explanation(assumed,[', since assumed']):- !.
explanation(called,['.']):- !.
explanation(answer(Answer),[' for answer ', Answer]) :- !.
explanation(naf, [' for NF']) :- !.
explanation(R,[' for ', R]).

indent(H) :-
	H=:=0
	;
	H > 0,
	V is H,
	forall(between(1,V,_), write('  ')).
indent(H,L) :-
	indent(H),
	maplist(write, L).

get_answer(H, Q, N) :-
	indent(H,Q),
	readln(Risp),
	(   Risp = [h|_], !,
	    show_commands,
	    get_answer(H,Q,N)
	;   Risp = [a|_], !,
	    abort
	;   Risp = [N|_],
	    integer(N),
	    N > 0, !
	).
show_commands :-
	maplist(writeln, ['\nComandi: h-elp, a-bort, N:integer>0',
			  'INVIO o qualsiasi altro input = risposta NO\n']).


%=================  SEZIONE GOALS ===================================
% Riconoscimento dei goals atomici.
%
% Vengono riconosciuti i goals utilizzabili nel body delle regole
% ma da non metainterpretare

pred(prolog_goal(any)).
%  prolog_goal(G) :  G � =, call, throw, catch
prolog_goal(G) :-
	G=..[X|_],
	prolog_op(X).

prolog_op(X) :- member(X,[
  =,      % unificazione
  call,   % chiamate base a prolog
  throw,
  catch
]).

pred(user_goal(any, atom)).
%  user_goal(G, M) :  G � un predicato utente
%  del modulo M da non meta-interpretare
%  (non ci sono rule che definiscono G)
%  Portabilit�:   DIPENDE da SWI,
%  in altri dialetti va ridefinito
user_goal(H, M) :-
	not(assumable(H)),
	functor(H, F, N),
	functor(G, F, N),
	current_predicate(_,M:H),
	not(clause(M:G, (rule(_),_))),!.

