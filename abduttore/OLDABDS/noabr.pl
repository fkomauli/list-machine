
rule(_).
%
% get_rule(rule(R), Head, Body) vero sse nel
% programma in esecuzione c'� un modulo M
% che contiene una clausola da metainterpretare,
% con regola rule(R)
/*get_rule(rule(R), H, true) :-
	current_predicate(_,H),
	clause(H, rule(R)).
get_rule(rule(R), H, B) :-
	current_predicate(_,H),
	clause(H, (rule(R),B)).*/

get_rule(rule(R), H, B) :-
	current_predicate(_,H),
	clause(H, BR),
	(    BR = rule(R), !, B=true
	;   BR = (rule(R),B)).

%  DEFAULTS:  true non deve essere chiesto o assunto
%% spostare nel file oggetto

askable(true) :-
	fail.
/*

assumable(true) :-
	fail.
*/
%=============================================================
% Il metainterprete.
%
% Per l'implementazione di prove vedi il codice.
% prove usa prolog_goal e user_goal , vedi section(goals).

%prove(+H, -Ass, -PT) :-
prove(H, Ass, PT) :-
	prove_body(H, [], Ass, PT).
prove(H, Ass1, Ass2, PT) :-
	prove_body(H, Ass1, Ass2, PT).



prove_head(true, Ass, Ass, true) :- !.
prove_head(!,_,_,_) :-
	throw('IL CUT NON E'' METAINTERPRETATO').
prove_head(not(H), Ass, Ass, pt(not(H), true, naf)) :- !,
	not(H).
prove_head(H, Ass, Ass,  pt(H, true, answer(Answer))) :-
	askable(H), !,
	ask(H, Subst, true, Answer),
	apply_subst(Subst).
prove_head(H, Ass, Ass, pt(H, true, prolog_goal)) :-
      prolog_goal(H),!,
      H.
prove_head(H, Ass, Ass, pt(H, true, user_goal)) :-
    user_goal(H),
    !,
    writeln(calledusergoal(H)),
    call(H).
prove_head(H, Ass, NewAss,  pt(H, true, assumed)) :-
    assumable(H),
        not(get_rule(_, H, _Body)),
	!,
	writeln(add(H)),
	add_assumption(H, Ass, NewAss).



%prove_head(H, Ass, Ass,  pt(H, true, R)) :-
%	get_rule(R, H, true).

prove_head(H, Ass, NewAss,  PT) :-
	get_rule(R, H, Body),
	(   Body=true, !,
	    PT = pt(H, true, R)
	;   prove_body(Body, Ass, NewAss, SubProofs),
	    PT = pt(H, SubProofs, R)).

%prove_body(H, Ass, NewAss, PT) :-
%	system_goal(H), !,
%	do(H, Ass,NewAss,PT).

prove_body(H, Ass, NewAss, PT) :-
	is_atomic(H),!,
	prove_head(H, Ass, NewAss, PT).
prove_body((A,B), Ass, NewAss, pt((A,B), (P1,P2), and)) :-
	prove_body(A, Ass, Ass1, P1),
	prove_body(B, Ass1, NewAss, P2).
prove_body((A;B), Ass, NewAss, pt((A;B), PA, or(1))) :-
	prove_body(A, Ass, NewAss, PA).
prove_body((A;B), Ass, NewAss, pt((A;B), PB, or(2))) :-
	prove_body(B, Ass, NewAss, PB).
prove_body((A->B), Ass, NewAss, pt((A->B), PB, if)) :-
	call(A), !,
	% NB:   A non viene meta-interpretato
	% � trattato come un test puro
	prove_body(B, Ass, NewAss, PB).

is_atomic(H) :-
	not(H=(_,_)),
	not(H=(_->_)),
	not(H=(_;_)).
apply_subst([X=T|S]) :-
	X=T, !,
	apply_subst(S).
apply_subst([]).


%==============================================================
%  Spiegazioni dal proof-tree.  --------------------------------------
%  Implementa explain_pt/1 e explain_pt/2
%

explain_pt(PT) :-
	nb_setval(depth, 10000),
	expl_pt(0, PT).


explain_pt(PT, N) :-
	nb_setval(depth, N),
	expl_pt(0, PT).



%   AUSILIARIO
%   expl_pt(K, PT): PT ha profondit� K e viene visualizzato a livello di
%   indentazione K
%
expl_pt(K, pt(H, true, R)):- !,
       explanation(R,Expl),
       indent(K, [H, '  vero'|Expl]),
       nl.
expl_pt(K, pt(H, SubPt, R)) :-
       nb_getval(depth,D),
       K+1 > D,!,
       indent(K, [H, ' vero per	', R, ';\n'] ),
       (   get_answer(K,['per la prova inserisci una prof.>0: '], DD) ->
	   D1 is D+DD,
	   nb_setval(depth,D1),
	   expl_pt(K, pt(H, SubPt, R)),
	   nb_setval(depth,D)
       ;   indent(K, ['... salto\n'] )
       ).
expl_pt(K, pt(H, (P1,P2), R)):- !,
       indent(K, [H, ' vero per	AND, perche :\n'] ),
       indent(K,['  [\n']),
       expl_and_pt(K+1,pt(H, (P1,P2), R)),
       indent(K,['  ]\n']).
expl_pt(K, pt((A;B), P, or(I))):- !,
       extract_or_pt(pt((A;B), P, or(I)), PT, 1, J),
       indent(K, [(A;B), ' vero per OR(',J,'), perche :\n']),
       expl_pt(K+1, PT).
expl_pt(K, pt((A->B), P, if)):- !,
       indent(K, [(A -> B), ' vero perche ', A, ' vero e:\n']),
       expl_pt(K+1,P).
expl_pt(K, pt(H, P, R)):-
       explanation(R,Expl),
       indent(K, [H, ' vero'|Expl]),!,
       expl_pt(K+1,P).


%   AUSILIARIO
%   extract_or_pt(pt((A1;A2;..;An), PTS, or(_)), PTk, I, J):
%      PTS contiene la prova PTk del disgiunto Ak e J = I+k
%   Modo:   (+,-,+,-) det.

extract_or_pt(pt((A;_B), pt(A, PA, R), or(1)), pt(A, PA, R), I, I) :- !.
extract_or_pt(pt((_A;B), pt(B, PP, R), or(2)), PT, I,J) :-
	I1 is I+1,
	extract_or_pt(pt(B, PP, R), PT, I1, J).


%   AUSILIARIO
%   expl_and_pt(K, pt((A1,A2,..,An), PTS, and)):
%   PTS contiene le sottoprove P1,P2,..,Pn di
%   A1, A2, ..., An
%   Le relative spiegazioni vengono visualizzate
%   tutte allo stesso livello K
%   Modo:   (+,+) det.

expl_and_pt(K, pt((_,_),(P1,P2),and)) :-!,
	expl_pt(K, P1),
	indent(K,['E\n']),
	expl_and_pt(K, P2).
expl_and_pt(K, pt(A, P, R)) :-
	expl_pt(K,pt(A, P, R)).


%  explanation(R, L):  L � la lista di atomi che
%  vengono concatenati in uscita con maplist(write,L)
%  e forniscono la spiegazione all'utente di R
%
explanation(rule(R),[' for rule ', R, ', since:\n']):-!.
explanation(assumed,[', since assumed']):- !.
explanation(called,['.']):- !.
explanation(answer(Answer),[' for answer ', Answer]) :- !.
explanation(naf, [' for NF']) :- !.
explanation(R,[' for ', R]).

indent(H) :-
	H=:=0
	;
	H > 0,
	V is H,
	forall(between(1,V,_), write('  ')).
indent(H,L) :-
	indent(H),
	maplist(write, L).

get_answer(H, Q, N) :-
	indent(H,Q),
	readln(Risp),
	(   Risp = [h|_], !,
	    show_commands,
	    get_answer(H,Q,N)
	;   Risp = [a|_], !,
	    abort
	;   Risp = [N|_],
	    integer(N),
	    N > 0, !
	).
show_commands :-
	maplist(writeln, ['\nComandi: h-elp, a-bort, N:integer>0',
			  'INVIO o qualsiasi altro input = risposta NO\n']).


%=================  SEZIONE GOALS ===================================
% Riconoscimento dei goals atomici.
%
% Vengono riconosciuti i goals utilizzabili nel body delle regole
% ma da non metainterpretare


%  prolog_goal(G) :  G � =, call, throw, catch
prolog_goal(G) :-
	G=..[X|_],
	prolog_op(X).

prolog_op(X) :- member(X,[
  =,      % unificazione
  call,   % chiamate base a prolog
  throw,
  catch
]).


%  user_goal(G) :  G � un predicato utente
%   da non meta-interpretare
%  (non ci sono rule che definiscono G)
%  Portabilit�:   DIPENDE da SWI,
%  in altri dialetti va ridefinito
user_goal(H) :-
	not(assumable(H)),
	functor(H, F, N),
	functor(G, F, N),
	current_predicate(_,H),
	not(clause(G, (rule(_),_))),!.

