/*   REQUIRES
 pred/1  es.   pred(p(_,_))
 assumable/1  es.  assumable(p(_,_))
 add_assumption/3
 */

:- dynamic(active/1).
rule(_).
tr(Topic,Msg) :-
    active(Topic) ->
	writeln('*****':Msg)
    ;
    true.

% get_rule(--R, :Head, --Body) vero sse nel
% programma in esecuzione contiene una clausola da metainterpretare,
% con regola R = rule(Nome)

get_rule(rule(R), H, B) :-
    %%is_pred(H),
    clause(H, BR),
    (   BR = rule(R), B=true
    ;   BR = (rule(R),B)).

%=============================================================
% Il metainterprete.

prove(H, Ass, PT) :-
	prove_goal(H, [], Ass, PT).
explain(H, Ass) :-
        prove_goal(H, [], Ass, PT),
	writeln('\nProof tree:'),
	explain_pt(PT),
	nl.

/*prove_pred(H, Ass, Ass,  pt(H, true, answer(Answer))) :-
	askable(H), !,
	ask(H, Subst, true, Answer),
	apply_subst(Subst).*/

prove_pred(H, Ass, NewAss,  PT) :-
    (get_rule(R, H, Body) *-> 
	 prove_goal(Body, Ass, NewAss, SubProofs), PT = pt(H, SubProofs, R)
    ;
    tr(assumable,assumed(H)),
    assumable(H) ,
    tr(assumable,afterasm(H)),
    add_assumption(H, Ass, NewAss),
    tr(assumable,added(H,Ass, NewAss)),
    PT =  pt(H, true, assumed)
    ).

%% prove_pred(H, Ass, NewAss,  pt(H, true, assumed)) :-
%%     not(get_rule(_,H,_)),
%%     assumable(H),
%%     add_assumption(H, Ass, NewAss).

%prove_goal(H, Ass, NewAss, PT) :-
%	system_goal(H), !,
%	do(H, Ass,NewAss,PT).
prove_goal(true,Ass,Ass,true).
prove_goal((A,B), Ass, NewAss, pt((A,B), (P1,P2), and)) :-
	prove_goal(A, Ass, Ass1, P1),
	prove_goal(B, Ass1, NewAss, P2).
prove_goal((A;B), Ass, NewAss, pt((A;B), PA, or(1))) :-
	prove_goal(A, Ass, NewAss, PA).
prove_goal((A;B), Ass, NewAss, pt((A;B), PB, or(2))) :-
    prove_goal(B, Ass, NewAss, PB).
%NEW
prove_goal(H, Ass, NewAss, PT) :-
%	is_pred(H),
	(trusted(H) -> call(H), PT = pt(H,true,trusted)
	;
	     prove_pred(H, Ass, NewAss, PT)
	).

%% prove_goal(H, Ass, NewAss, PT) :-
%%	pred(H),
%%	prove_pred(H, Ass, NewAss, PT).
prove_goal(H, Ass, Ass, pt(H,true,system)) :-
	system(H),!,
	call(H).

%% is_pred(H) :-
%%     pred(P),
%%     functor(P,F,N),
%%     functor(H,F,N).

system(H) :-
	not(H==true),
	not(H = pred(_)),
	not(H=(_,_)),
        not(H=(_;_)).

%==============================================================
%  Spiegazioni dal proof-tree.
%

explain_pt(PT) :-
	expl_pt(0, PT).

%   AUSILIARIO
%   expl_pt(K, PT): PT ha profondit� K e viene visualizzato a livello di
%   indentazione K
%

expl_pt(K, pt(H,true, R)):- !,
       explanation(R,Expl),
       indent(K, [H|Expl]),
       nl.
expl_pt(K, pt((A;B), P, or(I))):- !,
       extract_or_pt(pt((A;B), P, or(I)), PT, 1, J),
       indent(K, [(A;B), ' for OR(',J,')-rule, since:\n']),
       expl_pt(K+1, PT).
expl_pt(K, pt(H,(P1,P2), R)):- !,
    indent(K, [H, ' for AND-rule, since:\n'] ),
%    indent(K),
%    write(['  [\n']),
    expl_and_pt(K+1,pt(H, (P1,P2), R)).
%    indent(K).
%    write(['  ]\n']).
expl_pt(K, pt(H, P, R)):-
       explanation(R,Expl),
       indent(K, [H|Expl]),
       writeln(', since:'),!,
       expl_pt(K+1,P).

%   AUSILIARIO
%   extract_or_pt(pt((A1;A2;..;An), PTS, or(_)), PTk, I, J):
%      PTS contiene la prova PTk del disgiunto Ak e J = I+k
%   Modo:   (+,-,+,-) det.

extract_or_pt(pt((A;_B), pt(A, PA, R), or(1)), pt(A, PA, R), I, I) :- !.
extract_or_pt(pt((_A;B), pt(B, PP, R), or(2)), PT, I,J) :-
    !,
    I1 is I+1,
    extract_or_pt(pt(B, PP, R), PT, I1, J).
extract_or_pt(PT,PT,I,I).

%   AUSILIARIO
%   expl_and_pt(K, pt((A1,A2,..,An), PTS, and)):
%   PTS contiene le sottoprove P1,P2,..,Pn di
%   A1, A2, ..., An
%   Le relative spiegazioni vengono visualizzate
%   tutte allo stesso livello K
%   Modo:   (+,+) det.

expl_and_pt(K, pt((_,_),(P1,P2),and)) :-!,
	expl_pt(K, P1),
	indent(K),
	write('&\n'),
	expl_and_pt(K, P2).
expl_and_pt(K, pt(A, P, R)) :-
	expl_pt(K,pt(A, P, R)).

%  explanation(R, L):  L � la lista di atomi che
%  vengono concatenati in uscita con maplist(write,L)
%  e forniscono la spiegazione all'utente di R
%
explanation(rule(R),[' for rule ', R]):-!.
explanation(assumed,[', for assumed']) :- !.
explanation(system,[' by system call']) :- !.
explanation(trusted,[' trusted pred']) :- !.
%  NON PREVISTO:
explanation(Bo, [' prove by Boo? ', Bo]).

indent(H) :-
	H=:=0
	;
	H > 0,
	V is H,
	forall(between(1,V,_), write('  ')).
indent(H,[F|L]) :-
	indent(H),
	catch(nb_getval(verbosity, V),_,V=full),
	write_wff(V, F),
	maplist(write, L).

write_wff(full, F) :-
	write(F).
write_wff(light, F) :-
    	catch(nb_getval(depth, D),_,D=3),
	write_term(F, [max_depth(D)]).

write_wff(skel, (_,_)) :- !.
write_wff(skel, (_;_)) :- !.
write_wff(skel, F) :-
	functor(F,P,A),
	write(P/A).










