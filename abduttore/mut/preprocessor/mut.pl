% Mutation that removes a pred from a program

%% to do: 
%%% ==> generalize to other mutations

%% 

:- op(1100,xfy,::).
:- op(1000, fx, pred).
:- op(800,xfx,\ ).
:- op(950,xfx,#).
:- op(600,xfy,->).
:- op(600,xfy,=>).
:- op(900,fx,type).


%% general read of a **preproceed** program from a file into lists of headers and  preds
%% do a delete mutation, write on file

% list of possible mutations, now 1
mutations([del_some]).

% collect_pr(+FileIn, -OutFile) :-
collect_pr(FileIn, HOutList,MOutList,FileOut) :-
    open(FileIn, read, InStream),
    open(FileOut, write, OutStream),
    read_hd_pr(InStream,[],HOutList,[], MOutList),
    maplist(portray_clause,MOutList),
    mutations(PossibleMuts),
    random_member(Mut,PossibleMuts),
    apply_mut(Mut,MOutList, MainMutant),
%    del_some(MOutList, MainMutant),
    append(HOutList,MainMutant,Mutant),
    maplist(portray_clause(OutStream),Mutant),
    close(InStream),
    close(OutStream).

apply_mut(Trans,Prog,Mut) :-
    call(Trans,Prog,Mut).

% read clauses into headers and mains
%read_hd_pr(+InStream, +HAcc, -HOut,+MAcc, -MOut)
read_hd_pr(InStream, HAcc, HOut,MAcc, MOut) :-
	read_term(InStream, Term,[module(op_module)]),
	(   Term == end_of_file,  !, HAcc = HOut, MAcc = MOut
	;  % if reserved, put in header
	header_pred(Term), !, append(HAcc,[Term],HTmp), read_hd_pr(InStream,HTmp,HOut, MAcc,MOut);
	append(MAcc,[Term],MTmp), read_hd_pr(InStream, HAcc,HOut,MTmp, MOut)). %else in main


% name of non reserved preds
header_pred(Term) :-
    Term = (:- _A)
    ;
    functor(Term,F,_),
    reserved(F).

reserved(K) :-
   memberchk(K,[type,pred,func,(::),(,)]).

% my_random_select(-X,+List,-Rest,-Pos) :- as before, but returns the index where selection occorred
my_random_select(X,List,Rest,Pos) :-
	must_be(list, List),
	length(List, B),
	B>0,
	Pos is random(B),
	nth0(Pos,  List,X, Rest).


% del_some(+Prog, -NewProg) :-
/*
pick a random clause C from Main list
  if a fact, delete the fact
  else delete a random predicate from C and reassemble program
*/


del_some(Prog, NewProg) :-
    my_random_select((H :- Body), Prog, Rest,Pos),
    (Body = rule(_), !, NewProg = Rest,
     write("deleted fact: "), write(H), nl % if a fact, delete the fact
    ;
    Body = (rule(R),RealBody),
    r2l(RealBody,BodyList), %else it's a rule
    random_select(Deleted,BodyList,RestList),
    write("deleting: "), write(Deleted), nl, write("from: "), write((H :- Body)),nl,
    l2r([rule(R)|RestList],NewBody),
    nth0(Pos,NewProg,(H :- NewBody),Rest)).
% put new clause in original spot


%% conversion beween goals and list of goals
r2l((A,B),[A| Bs]) :-
    !,
    r2l(B,Bs).
r2l((A),[A]).

l2r([A],(A)) :- !.
l2r([A|As],(A,Rs)) :- l2r(As,Rs).

/*
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% OLD STUFF

% deletes a predicate/arity from a clause
rsel(S,[C|Cs],Rs) :-
    reserved(C),
    !,
    rsel(S,Cs,Rs).
    
rsel(S,Cs,R) :-
    random_select(S,Cs,R).

% name of non reserved preds
ok_pred(Term) :-
    functor(Term,F,_),
    not((reserved(F), Term = (:- _A))). % skip directives

% infer preds names and arity from program

preds_names(Mains, Sign) :-
    defined_pred(Mains,Defs),
    sort(Defs,Sign).

defined_pred([],[]).
defined_pred([(:- _A) | Ps], Ds) :- !, defined_pred(Ps,Ds).
defined_pred([(H :- _) |Ps],[D/N|Ds]) :-
    functor(H,D,N),
    defined_pred(Ps,Ds).


% this could be computed so that is consists of the builtin occuring
% in the bodies.

builtins([(#)/2,(<)/2]).


% rm_pred(+Pred/+N, +(H :- Body), -(H :- NewBody))
rm_pred(Pred/N, (H :- Body), (H :- NewBody)) :-
    functor(Goal,Pred,N),
    r2l(Body,BodyList),
    delete(BodyList,Goal,NewBodyList),
    l2r(NewBodyList,NewBody).

% assume all clauses have rule(_) in body, so deletion does not create facts
% pick a clause, pick a pred pred: if unchanged, pick another, else reassemble program with mutated clause

delete_some(Defs,NewDefs  ) :-
    preds_names(Defs,Sign),
    builtins(Builtins), 
    append(Builtins, Sign,Candidates),
    random_member(Pred/N,Candidates),
    write('selected: '),write(Pred/N),nl,
    del_some(Pred/N,Defs,NewDefs).

% old driver

try(File) :-
    collect_pr(File,H,P),
    delete_some(P,NP),
    nl,
    maplist(portray_clause,P),
    nl,
    write('deleting:'),
    nl,
    append(H,NP,NHP),
    maplist(portray_clause,NHP).

% old
% collect(+InStream,-OutList)
collect(In,HOut,MOut) :-
    read_hd_pr(In,[],HOut,[], MOut).
read_pr(InStream, Acc, Out) :-
	read_term(InStream, Term,[module(op_module)]),
	(   Term == end_of_file,  !, Acc = Out
	;
	ok_pred(Term) , !,  append(Acc,[Term],Tmp), read_pr(InStream, Tmp, Out) ;
	read_pr(InStream,  Acc,Out)).  % remove reserved

*/
