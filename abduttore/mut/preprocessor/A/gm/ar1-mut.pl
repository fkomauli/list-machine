/*

progress: has_type(E,T) => nstuck(E)
progress
Checking depth 1 2 3 4
Total: 0.000779 s:
E = tif(tfalse,ttrue,ttrue) ,
T = tbool ,
explain(nstuck(E),PP).
PP = [nstuck(tif(tfalse, ttrue, ttrue))]
*/

:- dynamic assumable/1.
:- dynamic trusted/1.
:- consult("../../../../abd.pl").
assumable(nstuck(_)).
assumable(nvalue(_)).
assumable(bvalue(_)).
assumable(exists_step(_)).
%assumable(value(_)).
%assumable(step(_M,_T)).
add_assumption(A, Ass, [A|Ass]).


:- style_check(- (discontiguous)).
:- use_module(op_module1).
tm::(type).
ttrue::tm.
tfalse::tm.
tif::(tm, tm, tm)=>tm.
tzero::tm.
tsucc::tm=>tm.
tpred::tm=>tm.
tiszero::tm=>tm.
pred bvalue(tm).
pred nvalue(tm).
pred value(tm).
pred step(tm, tm).
ty::(type).
tnat::ty.
tbool::ty.
pred has_type(tm, ty).
pred exists_step(tm).
pred nstuck(tm).
bvalue(ttrue) :-
	rule('bvalue/1-1').
bvalue(tfalse) :-
	rule('bvalue/1-2').
nvalue(tzero) :-
	rule('nvalue/1-1').
nvalue(tsucc(A)) :-
	rule('nvalue/1-2'),
	nvalue(A).
value(A) :-
	rule('value/1-1'),
	(   bvalue(A)
	;   nvalue(A)
	).
step(tif(ttrue, A, _), A) :-
	rule('step/2-1').
step(tif(tfalse, _, A), A) :-
	rule('step/2-2').
step(tif(C, A, B), tif(D, A, B)) :-
	rule('step/2-3'),
	step(C, D).
step(tsucc(A), tsucc(B)) :-
	rule('step/2-4'),
	step(A, B).
step(tpred(tzero), tzero) :-
	rule('step/2-5').
step(tpred(tsucc(A)), A) :-
	rule('step/2-6'),
	nvalue(A).
step(tpred(A), tpred(B)) :-
	rule('step/2-7'),
	step(A, B).
step(tiszero(tzero), ttrue) :-
	rule('step/2-8').
step(tiszero(tsucc(A)), tfalse) :-
	rule('step/2-9'),
	nvalue(A).
step(tiszero(A), tiszero(B)) :-
	rule('step/2-10'),
	step(A, B).
has_type(ttrue, tbool) :-
	rule('has_type/2-1').
has_type(tfalse, tbool) :-
	rule('has_type/2-2').
has_type(tif(A, B, D), C) :-
	rule('has_type/2-3'),
	has_type(A, tbool),
	has_type(B, C),
	has_type(D, C).
has_type(tzero, tnat) :-
	rule('has_type/2-4').
has_type(tsucc(A), tnat) :-
	rule('has_type/2-5'),
	has_type(A, tnat).
has_type(tpred(A), tnat) :-
	rule('has_type/2-6'),
	has_type(A, tnat).
has_type(tiszero(A), tbool) :-
	rule('has_type/2-7'),
	has_type(A, tnat).
exists_step(tif(ttrue, _, _)) :-
	rule('exists_step/1-1').
exists_step(tif(A, _, _)) :-
	rule('exists_step/1-3'),
	exists_step(A).
exists_step(tsucc(A)) :-
	rule('exists_step/1-4'),
	exists_step(A).
exists_step(tpred(tzero)) :-
	rule('exists_step/1-5').
exists_step(tpred(tsucc(A))) :-
	rule('exists_step/1-6'),
	nvalue(A).
exists_step(tpred(A)) :-
	rule('exists_step/1-7'),
	exists_step(A).
exists_step(tiszero(tzero)) :-
	rule('exists_step/1-8').
exists_step(tiszero(tsucc(A))) :-
	rule('exists_step/1-9'),
	nvalue(A).
exists_step(tiszero(A)) :-
	rule('exists_step/1-10'),
	exists_step(A).
nstuck(A) :-
	rule('nstuck/1-1'),
	value(A).
nstuck(A) :-
	rule('nstuck/1-2'),
	exists_step(A).
