/*
preservation: has_type(M,T), step(M,M') => has_type(M',T)
preservation
Checking depth 1 2 3
Total: 0.000702 s:
M = tiszero(tsucc(tzero)), 
M1 = tfalse, 
T = tbool,
explain(has_type(M1,T),PP). 


Proof tree:
has_type(tfalse,tbool), for assumed

M = tiszero(tsucc(tzero)),
M1 = tfalse,
T = tbool,
PP = [has_type(tfalse, tbool)] 

in fact, its' missing
*/

:- style_check(- (discontiguous)).
:- use_module(op_module1).

:- dynamic assumable/1.
:- dynamic trusted/1.
:- consult("../../Examples/sabr").
assumable(has_type(_M,_T)).
%assumable(step(_M,_T)).
add_assumption(A, Ass, [A|Ass]).
%trusted(ss(_X)).


tm::(type).
ttrue::tm.
tfalse::tm.
tif::(tm, tm, tm)=>tm.
tzero::tm.
tsucc::tm=>tm.
tpred::tm=>tm.
tiszero::tm=>tm.
pred bvalue(tm).
pred nvalue(tm).
pred value(tm).
pred step(tm, tm).
ty::(type).
tnat::ty.
tbool::ty.
pred has_type(tm, ty).
pred exists_step(tm).
pred nstuck(tm).
bvalue(ttrue) :-
	rule('bvalue/1-11').
bvalue(tfalse) :-
	rule('bvalue/1-12').
nvalue(tzero) :-
	rule('nvalue/1-11').
nvalue(tsucc(A)) :-
	rule('nvalue/1-12'),
	nvalue(A).
value(A) :-
	rule('value/1-6'),
	(   bvalue(A)
	;   nvalue(A)
	).
step(tif(ttrue, A, _), A) :-
	rule('step/2-41').
step(tif(tfalse, _, A), A) :-
	rule('step/2-42').
step(tif(C, A, B), tif(D, A, B)) :-
	rule('step/2-43'),
	step(C, D).
step(tsucc(A), tsucc(B)) :-
	rule('step/2-44'),
	step(A, B).
step(tpred(tzero), tzero) :-
	rule('step/2-45').
step(tpred(tsucc(A)), A) :-
	rule('step/2-46'),
	nvalue(A).
step(tpred(A), tpred(B)) :-
	rule('step/2-47'),
	step(A, B).
step(tiszero(tzero), ttrue) :-
	rule('step/2-48').
step(tiszero(tsucc(A)), tfalse) :-
	rule('step/2-49'),
	nvalue(A).
step(tiszero(A), tiszero(B)) :-
	rule('step/2-50'),
	step(A, B).
has_type(ttrue, tbool) :-
	rule('has_type/2-29').
has_type(tif(A, B, D), C) :-
	rule('has_type/2-31'),
	has_type(A, tbool),
	has_type(B, C),
	has_type(D, C).
has_type(tzero, tnat) :-
	rule('has_type/2-32').
has_type(tsucc(A), tnat) :-
	rule('has_type/2-33'),
	has_type(A, tnat).
has_type(tpred(A), tnat) :-
	rule('has_type/2-34'),
	has_type(A, tnat).
has_type(tiszero(A), tbool) :-
	rule('has_type/2-35'),
	has_type(A, tnat).
exists_step(tif(ttrue, _, _)) :-
	rule('exists_step/1-26').
exists_step(tif(tfalse, _, _)) :-
	rule('exists_step/1-27').
exists_step(tif(A, _, _)) :-
	rule('exists_step/1-28'),
	exists_step(A).
exists_step(tsucc(A)) :-
	rule('exists_step/1-29'),
	exists_step(A).
exists_step(tpred(tzero)) :-
	rule('exists_step/1-30').
exists_step(tpred(tsucc(A))) :-
	rule('exists_step/1-31'),
	nvalue(A).
exists_step(tpred(A)) :-
	rule('exists_step/1-32'),
	exists_step(A).
exists_step(tiszero(tzero)) :-
	rule('exists_step/1-33').
exists_step(tiszero(tsucc(A))) :-
	rule('exists_step/1-34'),
	nvalue(A).
exists_step(tiszero(A)) :-
	rule('exists_step/1-35'),
	exists_step(A).
nstuck(A) :-
	rule('nstuck/1-3'),
	value(A).
nstuck(A) :-
	rule('nstuck/1-4'),
	exists_step(A).
