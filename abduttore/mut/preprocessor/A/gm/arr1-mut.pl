:- style_check(- (discontiguous)).
:- use_module(op_module1).
tm::(type).
ttrue::tm.
tfalse::tm.
tif::(tm, tm, tm)=>tm.
tzero::tm.
tsucc::tm=>tm.
tpred::tm=>tm.
tiszero::tm=>tm.
pred bvalue(tm).
pred nvalue(tm).
pred value(tm).
pred step(tm, tm).
ty::(type).
tnat::ty.
tbool::ty.
pred has_type(tm, ty).
pred exists_step(tm).
pred nstuck(tm).
bvalue(ttrue) :-
	rule('bvalue/1-9').
bvalue(tfalse) :-
	rule('bvalue/1-10').
nvalue(tzero) :-
	rule('nvalue/1-9').
nvalue(tsucc(A)) :-
	rule('nvalue/1-10'),
	nvalue(A).
value(A) :-
	rule('value/1-5'),
	(   bvalue(A)
	;   nvalue(A)
	).
step(tif(ttrue, A, _), A) :-
	rule('step/2-31').
step(tif(tfalse, _, A), A) :-
	rule('step/2-32').
step(tif(C, A, B), tif(D, A, B)) :-
	rule('step/2-33'),
	step(C, D).
step(tsucc(A), tsucc(B)) :-
	rule('step/2-34'),
	step(A, B).
step(tpred(tzero), tzero) :-
	rule('step/2-35').
step(tpred(tsucc(A)), A) :-
	rule('step/2-36'),
	nvalue(A).
step(tpred(A), tpred(B)) :-
	rule('step/2-37'),
	step(A, B).
step(tiszero(tzero), ttrue) :-
	rule('step/2-38').
step(tiszero(tsucc(A)), tfalse) :-
	rule('step/2-39'),
	nvalue(A).
step(tiszero(A), tiszero(B)) :-
	rule('step/2-40'),
	step(A, B).
has_type(tfalse, tbool) :-
	rule('has_type/2-23').
has_type(tif(A, B, D), C) :-
	rule('has_type/2-24'),
	has_type(A, tbool),
	has_type(B, C),
	has_type(D, C).
has_type(tzero, tnat) :-
	rule('has_type/2-25').
has_type(tsucc(A), tnat) :-
	rule('has_type/2-26'),
	has_type(A, tnat).
has_type(tpred(A), tnat) :-
	rule('has_type/2-27'),
	has_type(A, tnat).
has_type(tiszero(A), tbool) :-
	rule('has_type/2-28'),
	has_type(A, tnat).
exists_step(tif(ttrue, _, _)) :-
	rule('exists_step/1-16').
exists_step(tif(tfalse, _, _)) :-
	rule('exists_step/1-17').
exists_step(tif(A, _, _)) :-
	rule('exists_step/1-18'),
	exists_step(A).
exists_step(tsucc(A)) :-
	rule('exists_step/1-19'),
	exists_step(A).
exists_step(tpred(tzero)) :-
	rule('exists_step/1-20').
exists_step(tpred(tsucc(A))) :-
	rule('exists_step/1-21'),
	nvalue(A).
exists_step(tpred(A)) :-
	rule('exists_step/1-22'),
	exists_step(A).
exists_step(tiszero(tzero)) :-
	rule('exists_step/1-23').
exists_step(tiszero(tsucc(A))) :-
	rule('exists_step/1-24'),
	nvalue(A).
exists_step(tiszero(A)) :-
	rule('exists_step/1-25'),
	exists_step(A).
nstuck(A) :-
	rule('nstuck/1-1'),
	value(A).
nstuck(A) :-
	rule('nstuck/1-2'),
	exists_step(A).
