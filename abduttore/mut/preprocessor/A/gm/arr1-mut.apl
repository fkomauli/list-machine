tm: type.
ttrue:tm.
tfalse:tm.
tif:(tm,tm,tm)->tm.
tzero:tm.
tsucc:tm->tm.
tpred:tm->tm.
tiszero:tm->tm.
pred bvalue(tm).
pred nvalue(tm).
pred value(tm).
pred step(tm, tm).
ty: type.
tnat:ty.
tbool:ty.
pred has_type(tm, ty).
pred exists_step(tm).
pred nstuck(tm).
bvalue(ttrue).
bvalue(tfalse).
nvalue(tzero).
nvalue(tsucc(A)) :-
	nvalue(A).
value(A) :-
	(   bvalue(A)
	;   nvalue(A)
	).
step(tif(ttrue, A, _), A).
step(tif(tfalse, _, A), A).
step(tif(C, A, B), tif(D, A, B)) :-
	step(C, D).
step(tsucc(A), tsucc(B)) :-
	step(A, B).
step(tpred(tzero), tzero).
step(tpred(tsucc(A)), A) :-
	nvalue(A).
step(tpred(A), tpred(B)) :-
	step(A, B).
step(tiszero(tzero), ttrue).
step(tiszero(tsucc(A)), tfalse) :-
	nvalue(A).
step(tiszero(A), tiszero(B)) :-
	step(A, B).
has_type(tfalse, tbool).
has_type(tif(A, B, D), C) :-
	has_type(A, tbool),
	has_type(B, C),
	has_type(D, C).
has_type(tzero, tnat).
has_type(tsucc(A), tnat) :-
	has_type(A, tnat).
has_type(tpred(A), tnat) :-
	has_type(A, tnat).
has_type(tiszero(A), tbool) :-
	has_type(A, tnat).
exists_step(tif(ttrue, _, _)).
exists_step(tif(tfalse, _, _)).
exists_step(tif(A, _, _)) :-
	exists_step(A).
exists_step(tsucc(A)) :-
	exists_step(A).
exists_step(tpred(tzero)).
exists_step(tpred(tsucc(A))) :-
	nvalue(A).
exists_step(tpred(A)) :-
	exists_step(A).
exists_step(tiszero(tzero)).
exists_step(tiszero(tsucc(A))) :-
	nvalue(A).
exists_step(tiszero(A)) :-
	exists_step(A).
nstuck(A) :-
	value(A).
nstuck(A) :-
	exists_step(A).
