/*
step_det: step(E,E1), step(E,E2) => E1 = E2
step_det
Checking depth 1 2 3
Total: 0.010912 s:
E = tiszero(tsucc(tif(ttrue,tzero,_15295))), 
E1 = tfalse ,
E2 = tiszero(tsucc(tzero)) ,
explain(( step(E,E1), step(E,E2)),PP).

step(tiszero(tsucc(tif(ttrue,tzero,_502))),tfalse),step(tiszero(tsucc(tif(ttrue,tzero,_502))),tiszero(tsucc(tzero))) for AND-rule, since:
  step(tiszero(tsucc(tif(ttrue,tzero,_502))),tfalse) for rule step/2-19
  &
  step(tiszero(tsucc(tif(ttrue,tzero,_502))),tiszero(tsucc(tzero))) for rule step/2-20, since:
    step(tsucc(tif(ttrue,tzero,_502)),tsucc(tzero)) for rule step/2-14, since:
      step(tif(ttrue,tzero,_502),tzero) for rule step/2-11

BACO in 19 nonvalue missing

*/

:- dynamic assumable/1.
:- dynamic trusted/1.
:- consult("../../../../Examples/sabr").
%assumable(value(_)).
%assumable(step(_M,_T)).
add_assumption(A, Ass, [A|Ass]).


:- style_check(- (discontiguous)).
:- use_module(op_module1).
tm::(type).
ttrue::tm.
tfalse::tm.
tif::(tm, tm, tm)=>tm.
tzero::tm.
tsucc::tm=>tm.
tpred::tm=>tm.
tiszero::tm=>tm.
pred bvalue(tm).
pred nvalue(tm).
pred value(tm).
pred step(tm, tm).
ty::(type).
tnat::ty.
tbool::ty.
pred has_type(tm, ty).
pred exists_step(tm).
pred nstuck(tm).
bvalue(ttrue) :-
	rule('bvalue/1-3').
bvalue(tfalse) :-
	rule('bvalue/1-4').
nvalue(tzero) :-
	rule('nvalue/1-3').
nvalue(tsucc(A)) :-
	rule('nvalue/1-4'),
	nvalue(A).
value(A) :-
	rule('value/1-2'),
	(   bvalue(A)
	;   nvalue(A)
	).
step(tif(ttrue, A, _), A) :-
	rule('step/2-11').
step(tif(tfalse, _, A), A) :-
	rule('step/2-12').
step(tif(C, A, B), tif(D, A, B)) :-
	rule('step/2-13'),
	step(C, D).
step(tsucc(A), tsucc(B)) :-
	rule('step/2-14'),
	step(A, B).
step(tpred(tzero), tzero) :-
	rule('step/2-15').
step(tpred(tsucc(A)), A) :-
	rule('step/2-16'),
	nvalue(A).
step(tpred(A), tpred(B)) :-
	rule('step/2-17'),
	step(A, B).
step(tiszero(tzero), ttrue) :-
	rule('step/2-18').
step(tiszero(tsucc(_)), tfalse) :-
	rule('step/2-19'). % bug missing nvalue(V)
step(tiszero(A), tiszero(B)) :-
	rule('step/2-20'),
	step(A, B).
has_type(ttrue, tbool) :-
	rule('has_type/2-8').
has_type(tfalse, tbool) :-
	rule('has_type/2-9').
has_type(tif(A, B, D), C) :-
	rule('has_type/2-10'),
	has_type(A, tbool),
	has_type(B, C),
	has_type(D, C).
has_type(tzero, tnat) :-
	rule('has_type/2-11').
has_type(tsucc(A), tnat) :-
	rule('has_type/2-12'),
	has_type(A, tnat).
has_type(tpred(A), tnat) :-
	rule('has_type/2-13'),
	has_type(A, tnat).
has_type(tiszero(A), tbool) :-
	rule('has_type/2-14'),
	has_type(A, tnat).
exists_step(tif(ttrue, _, _)) :-
	rule('exists_step/1-11').
exists_step(tif(tfalse, _, _)) :-
	rule('exists_step/1-12').
exists_step(tif(A, _, _)) :-
	rule('exists_step/1-13'),
	exists_step(A).
exists_step(tsucc(A)) :-
	rule('exists_step/1-14'),
	exists_step(A).
exists_step(tpred(tzero)) :-
	rule('exists_step/1-15').
exists_step(tpred(tsucc(A))) :-
	rule('exists_step/1-16'),
	nvalue(A).
exists_step(tpred(A)) :-
	rule('exists_step/1-17'),
	exists_step(A).
exists_step(tiszero(tzero)) :-
	rule('exists_step/1-18').
exists_step(tiszero(tsucc(A))) :-
	rule('exists_step/1-19'),
	nvalue(A).
exists_step(tiszero(A)) :-
	rule('exists_step/1-20'),
	exists_step(A).
nstuck(A) :-
	rule('nstuck/1-3'),
	value(A).
nstuck(A) :-
	rule('nstuck/1-4'),
	exists_step(A).
