
:- ensure_loaded('prepro.pl').
:- ensure_loaded('mut.pl').
/* main driver: 
   	read .apl file
        converti in pl
        mutate pl in mpl
        convert mp in apl to be checked

*/
% ex change_suff('a.apl',O,".pl").
change_suff(FIN,FOU,SUFF) :-
    split_string(FIN, ".", "", [Name|_Suff]),
    string_concat(Name,SUFF,FOU).

% invents a name for mutant of FIN
make_mut_name(FIN,FOU) :-
    split_string(FIN, ".", "", [Name|_Suff]),
    gensym(Name,NewName),
    atom_string(NewName,NewS),
    string_concat(NewS,"-mut",TMP),
    string_concat(TMP,".pl",FOU).

main(FileAPL) :-
    change_suff(FileAPL,FilePL,".pl"),
    preprocess(FileAPL,FilePL,a2p),
    make_mut_name(FilePL,Mutant),
    collect_pr(FilePL, _,_,Mutant),
    change_suff(Mutant,MutantPL,".apl"),
    preprocess(Mutant,MutantPL,p2a).

/*
a2p_name(APL,PL) :-
    split_string(APL, ".", "", [Name|Suff]),
    string_concat(Name,".pl",PL).
*/

:- main('A/ar.apl').
