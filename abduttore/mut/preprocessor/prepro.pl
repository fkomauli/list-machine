
% Needed HERE for portray_clause
:- op(1050,xfx,(::)).
:- op(1090, xfx, :).
:- op(1100, fx, pred).
:- op(1100, fx, func).
:- op(800,xfx,\ ).
:- op(800,xfx,#).
:- op(960,xfx,->).
:- op(960,xfx,=>).
:- op(800, xfx, \).
:- op(1100,fx,type).

% preprocess(+FileIn, -FileOut, +Transformation) where Transformation
% is a2p, p2a

%EX.
%   preprocess('stlc.apl','s.pl', a2p).
%   preprocess('s.pl','s.apl', p2a).

preprocess(FileIn, FileOut, Transformation) :-
	open(FileIn, read, InStream),
	open(FileOut, write, OutStream),
	headers(OutStream, Transformation),
	transform(InStream, OutStream, Transformation),
	close(InStream),
	close(OutStream).

%prepreprocess, adding ops. Will be done by asserta
headers(OutStream, Transformation) :-
    Transformation == a2p ->
    portray_clause(OutStream, :- style_check(-discontiguous)),
    portray_clause(OutStream, :- use_module(op_module1))
    ;
    true. %if p2a, no header

%% from aprolog to prolog

% the ->/=> not usefule



transform(InStream, OutStream, Tr) :-
	read_term(InStream, Term,[module(op_module1)]),
	(   Term == end_of_file,  !
	;
	call(Tr, Term, Term1),
	print_clause(Tr,OutStream, Term1),
	!,
	transform(InStream, OutStream, Tr)).

print_clause(a2p,OutStream, Term1) :-
    print_clause_a2p(OutStream, Term1).

print_clause(p2a,OutStream, Term1) :-
    print_clause_p2a(OutStream, Term1).


print_clause_a2p(Stream, Clause) :-
    add_rule(Clause,NewClause),
    !,
    portray_clause(Stream, NewClause).

print_clause_a2p(Stream, Term) :-
        portray_clause(Stream, Term).

print_clause_p2a(_,true) :- !.
print_clause_p2a(Stream, :(A,type)) :- !,
	write_term(Stream, A,[module(op_module)]),
        write(Stream,': type.\n').
print_clause_p2a(Stream, :(A,(B->C))) :- !,
	write_term(Stream, :(A,(B->C)),[module(op_module)]),
	write(Stream, '.\n').

print_clause_p2a(Stream, Term) :-
        portray_clause(Stream, Term).


add_rule(Head :- Body,  Head :- (rule(Name), Body)) :-
    !,
    mk_name(Head,Name).
add_rule(Head,  Head :- rule(Name)) :-
    mk_name(Head,Name).


reserved(K) :-
    memberchk(K,[type,pred,func,(::),(,)]).

mk_name(Head,NameOut) :-
    functor(Head,Name,Arity),
    not(reserved(Name)),
    atomic_list_concat([Name,/,Arity,-], Base),
    gensym(Base, NameOut).

a2p((:(Id,(->(T1,T2)))), ::(Id, =>(T1,T2))) :-!,
	portray_clause(::(Id, =>(T1,T2))), nl.
a2p((:(Id,T)), ::(Id, T)) :-!,
	portray_clause(::(Id,T)), nl.
a2p(T,T) :-
	portray_clause(T), nl.


p2a(:- use_module(_), true) :- !.
p2a(:- use_module(_,_),true) :- !.
p2a(:- style_check(_),true) :- !.
p2a((Id :: (T1=>T2)), :(Id,->(T1,T2))) :-!,
    % portray_clause would interpret T1 -> T2 ad if T1 then T2
    write_term(:(Id,->(T1,T2)),[module(op_module2)]), nl.
p2a(Id :: T, :(Id,T)) :-!,
    portray_clause(:(Id,T)), nl.
p2a((H :- (rule(_),B)), (H :- B)) :-!.
p2a((H :- rule(_)), H) :-!.
p2a(T,T) :-
	portray_clause(T), nl.
