
%%%%%%


:- op(1100,xfy,::).
:- op(1000, fx, pred).
:- op(800,xfx,\ ).
:- op(950,xfx,#).
:- op(600,xfy,->).
:- op(600,xfy,=>).
:- op(900,fx,type).

% collecting the progra by reading the file. May want to keep reserved preds, now excluded
collect_pr(FileIn, Out) :-
	open(FileIn, read, InStream),
	collect(InStream, Out),
	close(InStream).

collect(In,Out) :-
    read_pr(In,[],Out).
read_pr(InStream, Acc, Out) :-
	read_term(InStream, Term,[module(op_module)]),
	(   Term == end_of_file,  !, Acc = Out
	;
	ok_pred(Term) , !,  append(Acc,[Term],Tmp), read_pr(InStream, Tmp, Out) ; 
	read_pr(InStream,  Acc,Out)).

ok_pred(Term) :-
    functor(Term,F,_),
    not_reserved(F).

not_reserved(K) :-
    not(memberchk(K,[type,pred,func,(::),(,)])).

% assumed there are rules
del_some(Pred/N,Prog, NewProg) :-
    random_select(Cls, Prog, Rest),
    rm_pred(Pred/N,Cls, NewCls),
    (Cls = NewCls, !, % pred does not occur in clause, choose again
	 del_some(Pred/N,Prog, NewProg)
    ;
    select(NewCls,  NewProg,Rest)). % insert clause, discontigous %% should be done by defs, not by clause

try(Pred/N) :-
    collect_pr('sample.pl',P),
    del_some(Pred/N,P,NP),
    nl,
    maplist(portray_clause,P),
    nl,
    write('deleting:'),write(Pred),
    nl,
    maplist(portray_clause,NP).
%% creating a program as a list of clauses from pred
%% program(-P).

% finds all defined predicates with their types
% find_pred(-Preds)
find_pred(Preds) :-
    findall(P,pred(P), List), maplist((=..),List,Preds).

% R = [[value, tm], [subst, tm, id, tm, tm], [step, tm, tm],
% [exists_step, tm], [has_type, ctx, tm, ty], [steps, tm, tm],
% [progress, tm], [not_stuck|...], [...|...]].


% creates lists of all defined heads coming from find_pred
defined_pred([],[]).
defined_pred([P|Ps],[D|Ds]) :-
    P = [Pred|Args],
    length(Args,N),
    functor(D,Pred,N),
    defined_pred(Ps,Ds).

%    maplist(clause,Heads,Pr).
%    findall((Heads :- B),clause(Heads, B), Pr).

% build clauses
make_clauses([],[]).
make_clauses([H|Hs], [(H :- B)| Cls]) :-
    clause(H,B),
    make_clauses(Hs,Cls).

program(Pr) :-
    find_pred(Preds),
    defined_pred(Preds, Heads),
    make_clauses(Heads,Pr),
    maplist(portray_clause,Pr).

%% a sample mutation: removing a predicate from a random clause.

%% this loop canbe generalized and improved, of course
rm_some(Pred,Prog, NewProg) :-
    random_select(Cls, Prog, Rest),
    rm_pred(Pred,Cls, NewCls),
    (Cls = NewCls -> % pred does not occur in clause
	 rm_some(Pred,Prog, NewProg)
    ;
    random_select(NewCls,  NewProg,Rest)). % insert clause, discontigous %% should be done by defs, not by clause

% deletes a predicate/arity from a clause
% rm_pred(+Pred/+N, +(H :- Body), -(H :- NewBody))
rm_pred(Pred/N, (H :- Body), (H :- NewBody)) :-
    functor(Goal,Pred,N),
    r2l(Body,BodyList),
    delete(BodyList,Goal,NewBodyList),
    l2r(NewBodyList,NewBody).


r2l((A,B),[A| Bs]) :-
    !,
    r2l(B,Bs).
r2l((A),[A]).


l2r([A],(A)) :- !.
l2r([A|As],(A,Rs)) :- l2r(As,Rs).


/* Example
Clause = (has_type(A, lam(x\C), funTy(B, D)) :-
	rule('has_type/3-7'),
	x#A,
	has_type([(x, B)|A], C, D)),

rm_pred((#)/2,Clause, NC),
portray_clause(NC).

NC =  (has_type(A, lam(x\C), funTy(B, D)):-rule('has_type/3-7'), has_type([(x, B)|A], C, D))
 */


%apply_remove_some(

%%% OLD

% failure driven example
prova(FileIn) :-
    open(FileIn, read, InStream),
    repeat,
    read(InStream,Term),
    (  Term == end_of_file,
    !
    ;  portray_clause(Term),
       fail
    ).


%%%%

/*
[my_clause(user,partition(_522,[],[],[]),true),my_clause(user,partition(_464,[_474|_476],[_474|_482],_470),(_474=<_464,partition(_464,_476,_482,_470))),my_clause(user,partition(_406,[_416|_418],_410,[_416|_424]),(_416>_406,partition(_406,_418,_410,_424)))]*/

% from my_clause to usual rules
m2rc(my_clause(_M,H,B), H :- B).
m2rp(Prog,Rules) :-
    maplist(m2rc,Prog, Rules),
    maplist(portray_clause, Rules).


%% use portray_clause(Stream,Clause) to write it on a file 
%%     tmp_file_stream(text, Tmp, Stream),
%%    maplist(portray_clause(Stream, Rules),Rules).


