/*
% :- op(1000,xfx,:).
:- op(800,xfx,::).
:- op(900,fy,pred).
:- discontiguous (pred)/1.


legge come clausola i on modulo a, anche con op
 listing(a:i).
i.
*/

i : type.

a : i.
b : i.
c : i.
d :: i.

pred ispref([i],[i]).

% bugged
ispref([],_).
ispref([X|XS],[Y|YS]) :- X = Y ; ispref(XS,YS).


pred app([i],[i],[i]).
app([],Ys, Ys).
app([X|Xs],Ys,[X|Zs]) :- app(Xs,Ys,Zs).

pred ex_app([i],[i]).
ex_app(Xs,Ys) :- app(Xs,L,Ys).

%% ispref(Xs,Ys) => esists Zs, append(Xs,Zs,Ys). 
%% ispref(Xs,Ys) => esists Zs, Xs++Zs = Ys. 

