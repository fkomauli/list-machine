exp : type.
id  : name_type.
ty  : type.
cnt : type.


% u   : exp.
c   : cnt -> exp.
var : id -> exp.
app : (exp,exp) -> exp. %% use infix?
lam : (id\exp,ty) -> exp.
error : exp.

% constants
cons : cnt.
hd : cnt.
tl : cnt.
nil : cnt.
toInt : int -> cnt.
%% plus : cnt.

% unitTy : ty.
intTy : ty.
funTy  : (ty,ty) -> ty.
%% change this to make in constant listintTy
listTy : (ty) -> ty.


type ctx = [(id,ty)].

% int added
% remove u ?

%% func subst(exp,id,exp) = exp.

pred subst(exp,id,exp,exp).
subst(error,_,_,error).
subst(c(C),_,_,c(C)).
subst(var(X),X,N, N).
subst(var(Y),X,N,var(Y)) :- X # Y.
subst(app(M1,M2),X,N,app(R1,R2)) :-
    subst(M1,X,N,R1), subst(M2,X,N,R2).
subst(lam(y\M,T),X,N, lam(y\R,T)) :- y # (X,N),subst(M,X,N,R).

%substp(M,X,N,MM) :- subst(M,X,N) = MM.

pred tcc(cnt,ty).
tcc(toInt(_N), intTy).
tcc(nil, listTy(intTy)).
tcc(hd, funTy(listTy(intTy),intTy)).
tcc(tl, funTy(listTy(intTy),listTy(intTy))).
tcc(cons, funTy(intTy,funTy(listTy(intTy),listTy(intTy)))).
% tcc(plus, funTy(intTy,funTy(intTy,intTy))).


pred tc(ctx,exp,ty).
tc(_,error,T).
tc(_,c(C),T) :- tcc(C,T).
tc([(Y,T)|G],var(X),U) :- X = Y,T=U.
tc([_|G],var(X),T) :- tc(G,var(X),T).
tc(G,app(M,N),U) :-  tc(G,M,funTy(T,U)), tc(G,N,T).
tc(G,lam(x\M,T),funTy(T,U)) :- x # G, tc([(x, T) |G],M,U).

pred valid_ctx(ctx).
valid_ctx([]).
valid_ctx([(X,T)|G]) :- X # G, valid_ctx(G).


pred value(exp).
value(c(_)).
value(lam(_,_)).
% value(app(c(cons),V)) :- value(V).
value(app(K,V)) :- K = c(cons), value(V).
value(app(K,V2)) :- K = app(c(cons),V1), value(V1), value(V2).

pred step(exp,exp).
step(app(c(hd),app(app(c(cons),X),_)),X).
step(app(c(tl),app(app(c(cons),_),XS)),XS).
step(app(lam(x\M,T),N),P) :- value(N),  subst(M,x,N,P).
step(app(M1,M2),app(M1n,M2n)) :- step(M1,M1n), M2=M2n.
step(app(M1,M2),app(M1n,M2n)) :- value(M1),  M1=M1n, step(M2,M2n).


pred exists_step(exp).
%exists_step(app(lam(_),_)).
exists_step(app(lam(M,T),V)):- value(V).
exists_step(app(c(hd),app(app(c(cons),_X),_))).
exists_step(app(c(tl),app(app(c(cons),_),_XS))).
exists_step(app(M1,M2)) :- exists_step(M1).
exists_step(app(M1,M2)) :- value(M1), exists_step(M2).

pred is_err(exp).
is_err(error).
is_err(app(c(hd), c(nil))).
is_err(app(c(tl), c(nil))).
is_err(app(E1,E2)) :- is_err(E1).
is_err(app(V1,E2)) :- value(V1),is_err(E2).

pred progress(exp).
progress(V) :- value(V).
progress(E) :- is_err(E).
progress(M) :- exists_step(M).


