%% STLC from TAPL chap 9

tm : type.
id  : name_type.
ty  : type.

type ctx = [(id,ty)]. 

ttrue : tm.
tfalse : tm.
tif : (tm, tm, tm) -> tm.
var : id -> tm.
app : (tm,tm) -> tm.
lam : id\tm -> tm.

boolTy : ty.
funTy  : (ty,ty) -> ty.

pred value(tm).
value(ttrue).
value(tfalse).
value(lam(_)).

pred subst(tm,id,tm,tm).
subst(ttrue,_,_,ttrue).
subst(tfalse,_,_,tfalse).
subst(tif(M1, M2, M3), X,N,tif(R1,R2,R3)) :- 
    subst(M1,X,N,R1),subst(M2,X,N,R2),subst(M3,X,N,R3).
subst(var(X),X,N, N).
subst(var(Y),X,N,var(Y)) :- X # Y.
subst(app(M1,M2),X,N,app(R1,R2)) :-
    subst(M1,X,N,R1), subst(M2,X,N,R2).
subst(lam(y\M),X,N, lam(y\R)) :- y # (X,N),subst(M,X,N,R).

pred step(tm,tm).
step(app(lam(x\M),N),P) :- value(N),  subst(M,x,N,P).
step(app(M1,M2),app(M11,M2)) :- step(M1,M11).
step(app(M1,M2),app(M1,M22)) :- value(M1),step(M2,M22).
step(tif(ttrue, T1, T2),T1).
step(tif(tfalse, T1, T2),T2).
step(tif(T1, T2, T3),tif(T11, T2, T3)):- step(T1,T11).


pred exists_step(tm).
exists_step(tif(ttrue,M2,M3)).
exists_step(tif(tfalse,M2,M3)).
exists_step(tif(M1,M2,M3)) :- exists_step(M1).
exists_step(app(lam(M),N)).
exists_step(app(M1,M2)) :- exists_step(M1).
exists_step(app(M1,M2)) :- exists_step(M2).


pred has_type(ctx,tm,ty).
has_type(G,ttrue,boolTy).
has_type(G,tfalse,boolTy).
has_type(G,tif(T1, T2, T3),A):- 
    has_type(G,T1, boolTy), has_type(G,T2,A),has_type(G,T3,A).
has_type([(X,T)|G],var(X),T).
has_type([(Y,_)|G],var(X),T) :- X # Y, has_type(G,var(X),T).

has_type(G,app(M,N),U) :-  has_type(G,M,funTy(T,U)), has_type(G,N,T).
has_type(G,lam(x\M),funTy(T,U)) :- x # G, has_type([(x, T) |G],M,U).

pred steps(tm,tm).
steps(M,N) :- M = N.
steps(M,P) :- step(M,N), steps(N,P).


pred not_stuck(tm).
not_stuck(V) :- value(V).
not_stuck(M) :- exists_step(M).






