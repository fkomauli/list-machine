\documentclass{llncs}

%\usepackage{fullpage}
%\usepackage{aopmath}
\usepackage{boxedminipage}
\usepackage{xspace,url}
\usepackage[pdf]{pstricks}
\usepackage{epsfig}
\usepackage{amssymb}
\usepackage{enumitem}
\usepackage{proof}
\input{macros}

\begin{document}
\title{The Blame Game for Property-based Testing\\
(work in  progress)}
\author{Alberto Momigliano and Mario Ornaghi}
\institute{DI, Universit\`{a} di Milano}
\maketitle{}
\begin{abstract}
  We report on work in progress aiming to add \emph{blame} features to
  property-based-testing in logic programming, in particular w.r.t.\
  the \acheck tool. Once the latter reports a counterexample to a
  property stated in \aprolog, a combination of \emph{abduction} and
  \emph{proof explanation}  tries to help the user to locate
  the part of the program that is responsible for the unintended
  behavior. To validate our approach, we have also implemented a
  mutation testing tool for a fragment of \aprolog, so as to generate
  a set of unbiased potential mutants against faults we can deploy the
  blame tool without previous knowledge of the bugs'
  nature. Preliminary experiments points to the usefulness of such an
  approach. The mutator is of independent interest, allowing us to gauge the effectiveness of the various strategies of \acheck in killing mutants.  
\end{abstract}

\section{Introduction}
\label{sec:intro}

I don't know about yours, but our students seem to enjoy using
property-based testing~\cite{fink97sen} (PBT) during their functional
programming coursework. After type checking succeeds, the
QuickCheck~\cite{claessen00icfp} message ``\emph{Ok, passed 100
  tests}'', whereby the tool reports the failure to refute some
property, gives the students a warm fuzzy feeling of having written
correct code. This feeling comes to a sudden halt when a
counterexample is reported, signaling a mismatch between code and
specification. In our experience, the student freezes, disconnects the
brain, typically uttering something like ``Prof, there's a problem
here'', and refrains from taking any further action.

Now, no-one likes to be wrong, not only students, and this is why
testers and programmers may be different entities in a software
company. While a counterexample to a property is a more useful
response than, say, the \emph{false} of a failed logic programming
query --- or a core dump for that matter --- we can do better and go
beyond the mere reporting of that counterexample; namely, try to
\textit{circumscribe} the origin of the latter. After all, the program
under test may be arbitrary large and, arguably, even partial
solutions pinpointing the slice of the program involved in an error can
help pursuing the painful process of bug fixing.

``Where do bugs come from?'' is a crucial question that has sparked a
huge amount of research that is quite impossible to sum up. In this
short paper, we report on preliminary work on addressing a much, much
smaller area: first, the domain is \emph{mechanized meta-theory
  model-checking}~\cite{alphacheck}, that is the validation of the
mechanization in a logical framework of the meta-theory of programming
languages and related calculi. To fix ideas, think about the formal
verification of compiler correctness~\cite{Leroy09} and look no
further than~\cite{Klein12} for more impressive case studies.  In this
domain, the specifications under validation correspond to the theorems
that the formal system under test should obey and are therefore
\emph{trusted}. Hence, we put the blame of a counterexample om the system
encoding, not on a possibly erroneous property.

Secondly, and consequently, we restrict to (nominal) logic
programming, that is to encoding written in
$\alpha$Prolog~\cite{nominallogicb} and properties checked by
\acheck~\cite{alphacheck}.  Logic programming, of course, is
particularly suited to this task. In fact, the related notion of
\emph{declarative debugging} ~\cite{Caballero}, starting with
Shapiro's thesis, originated there. Our blame tool is also written in
standard Prolog, a choice taken mostly out of convention, since it
allows us the flexibility to experiment with ideas by
{meta-interpretation}, rather then wiring them up in \aprolog's OCAML
implementation. This will be the next step.

Our approach is likely to be applicable outside those boundaries, but
we haven not explored enough to make any strong claim.

\subsection{A motivating example}
\label{sec:ex}

% To cut to the chase, here is a motivating example,.

Suppose (adapted from~\cite{NitManual}) we wish to study the
following grammar, which, as proven in Aho \& Ulmann's textbook,
characterizes all the strings with the same number of $a$'s
and $b's$:
\begin{small}
\begin{verbatim}
S ::= .  | bA | aB
A ::= aS | bAA
B ::= bS | aBB
\end{verbatim}
\end{small}
Here's the bulk of the \aprolog code (in fact pure Prolog code), where we
have omitted the type declaration and the obvious definition of
list-based predicates, which we consider trusted:
% \begin{verbatim}
% s1: ss([]).
% s2: ss([b|W])  :- ss(W). 
% s3: ss([a|W])  :- bb(W).

% b1: bb([b|W])  :- ss(W).
% b2: bb([a|VW]) :- bb(V), bb(W), append(V,W,VW).

% a1: aa([a|W])  :- ss(W).
% a2: aa([b|VW]) :- aa(V), aa(W), append(V,W,VW)
% \end{verbatim}
\begin{small}
\begin{verbatim}
ss([]).
ss([b|W])  :- ss(W). 
ss([a|W])  :- bb(W).

bb([b|W])  :- ss(W).
bb([a|VW]) :- bb(V), bb(W), append(V,W,VW).

aa([a|W])  :- ss(W).
aa([b|VW]) :- aa(V), aa(W), append(V,W,VW)
\end{verbatim}
\end{small}
However, our
encoding is flawed --- we ask the reader to suspend her disbelief,
since the bug is quite apparent. Still, it's easy to
conjure an analogous scenario where the grammar has dozens of
productions and the bugs harder to spot.
%

We shall use $\alpha$Check to debug it. We split the characterization of the grammar into
soundness and completeness properties:
\begin{small}
\begin{verbatim}
#check "sound" 10 : ss(W), count(a,W,N1), count(b,W,N2) => N1 = N2.
#check "compl" 10 : count(a,W,N), count(b,W,N) => ss(W).
\end{verbatim}
\end{small}
The tool dutifully reports (at least) two counterexamples:
\begin{small}
\begin{verbatim}
Checking for counterexamples to 
sound: ss(W), count(a,W,N1), count(b,W,N2) => N1 = N2
Checking depth 1 2 3
Total: 0.000329 s:
N1 = z, N2 = s(z), W = [b] 

compl: count(a,W,N), count(b,W,N) => ss(W)
Checking depth 1 2 3 4 5 6 7 8 9
Total: 0.016407 s:
N = s(z), W = [b,a] 
\end{verbatim}
\end{small}
Now that we have the counterexamples, where is the bug? More precisely, which clauses shall we blame?


\subsection{Debugging couterexamples}
\label{sec:idea}
The \verb|#check| pragma \acheck corresponds to
specification formulas of the form
%
\begin{equation}\label{eq:typical-spec}
 \new \vec{\Aa}. \forall \vec{X}.~G \impp A
\end{equation}
%
where $G$ is a goal and $A$ an atomic formula (including equality and
freshness constraints).  Since the $\new$-quantifier is self-dual, the
negation of the formula (\ref{eq:typical-spec}) is
$\new \vec{\Aa}. \exists{\vec{X}}. G \andd \neg A$.  A \emph{(finite)
  counterexample} is a closed substitution $\theta$ providing values
for $\vec{X}$ that satisfy this formula: that is, such that
$\theta(G)$ is derivable, but the conclusion $\theta(A)$ is not. In
this paper, we identify negation with negation-as-failure (NAF) and we
will not make essential use of nominal features.

The tool synthesizes (trusted) type-based exhaustive generators for
the free variables of the conclusion, so that NAF executes safely, and
it will search with a complete search strategy, here naive iterative
deepening for a proof of:
\begin{equation}
  \label{pt}
 \exists \vec{X}{:}\vec{\tau}.~G \andd
 \gen\SB{\tau}(X_1) \andd \cdots \andd \gen\SB{\tau}(X_n)\andd not(A)
\end{equation}
To wit, completeness will be the query
\begin{small}
\begin{verbatim}
count(a,W,N), count(b,W,N), gen_ablist(W), not(ss(W)).
\end{verbatim}
\end{small}
where by mode analysis we now that we do not geed henerators for
numerals, but only for lists of \texttt{a}'s and \texttt{b}'s.

For a query such as the above and more in general (\ref{pt}) to succeed, two (possibly overlapping) things
may have gone wrong~\cite{naish97declarative}:
\begin{description}
\item[MA:] the atom $A$ fails, whereas it morally belongs to the intended
  interpretation of its definition (\emph{missing answer});
\item[WA:] a bug in $G$ creates some erroneous bindings, which again
  make $A$ fail (\emph{wrong answer}: some atom in G succeeds but it is not in the intended model).% \footnote{We also trust the
    % generators, beside the properties.}
\end{description}

Our ``old-school'', simple-minded idea constins of coupling
\begin{itemize}
\item 
\emph{abduction}~\cite{ABD92} to try and diagnose \emph{MA's} with
\item \emph{proof verbalization}, that is presenting, at various level
  of abstraction, \emph{proof-trees} for \emph{WA's} as a mean to
  explain where, if not why, the WA occurred.
\end{itemize}

Of course, since we do not know in principle which is which and the
two phenomena can occur simultaneously, we are going to us some simple
heuristics to drive this process. What we would rarher stay away from
is full declarative debugging (see
e.g.,~\cite{DershowitzL93}), as our experience suggests that making the
user the oracle is just too much work.


\newcommand{\cA}{{\cal A}}

Our notion of {abduction} is  a simple-mined proof procedure that finds a set of
assumption $\Delta$ such that $\Gamma,\Delta\vdash G$, but
$\Gamma\not\vdash G$. In our limited setting here of first-order logic
programming, we as expected, internalize $\Gamma$ as a fixed program $P$ and we see
$\Delta$ as a  (additive) conjunction of atoms $\cA$. An abduction
procedure depends on the choice of what the \emph{abducibles}
are. This choice is related to which part of the program we trust and
which we want to investigate. We give those choices to the user, while
trying to make it easy to change one's mind. The system will also
suggest abducibles based on the dependency graph of the query under
investigation.

We use Prolog for fast-prototyping via meta-interpreters. Those are not
allowed by the simple typing discipline of \aprolog, where the
explanation features would be hard-wired in the interpreter.

\smallskip We now apply the above idea to example in
Section~\ref{sec:ex}.\footnote{It'd be better to stage the example
  with 2 bugs, one caught by soundness, then fix it and find the second
  by completeness. We make it shorter and less convincing here.} To
begin with, we need to decide what to trust. Consistently with our
faith in properties, we will trust \texttt{count} and
\texttt{append}. For convenience sake, let's start investigating the
second bug, that is the failure of
\texttt{ss([b,a])}. This is a case of MA. The dependency graph suggests (quite
unhelpfully) to put all of \texttt{aa,bb,ss} as abductables. The tool returns this explanation:
\begin{small}
\begin{verbatim}
ss([b,a]) for rule s2, since:
  ss([a]) for rule s3, since:
    bb([]) for assumed
\end{verbatim}
\end{small}
Besides the primitive verbalization, this means that \emph{if it were
  the case} that \texttt{bb([])} holds, then \texttt{ss([b,a])} would
have succeeded and the counterexample not found. However, there is no
production for \textit{b} accepting the empty string, so we should
blame \texttt{ss}, namely either \texttt{s2} or \texttt{s3}. By
comparing the clauses to the productions they are supposed to encode
we see that \texttt{s2} should be \texttt{ss([b|W]) :- aa(W).}

For the soundness bug, it's a case of WA. We trust unification since
thanks to grounding it's just syntactic equality. Hence we need to
understand what went wrong in the premises.

The tool answers:
\begin{small}
\begin{verbatim}
ss([b])  for rule s2, since:
  ss([])   for fact s1.
\end{verbatim}
\end{small}
Since \texttt{s1} is OK, it's again \texttt{s2}'s fault.

Clearly, a proof-tree of height 2 is not that difficult to
interpret. However, we will offer various levels of explanation, from
the whole tree to   a skeleton containing only the involved rule
names.
%Once we add the clause
% \begin{verbatim}
% a3: aa([b|VW]) :- aa(V), aa(W), append(V,W,VW)
% \end{verbatim}
% the counterexample to completeness disappears.
% unwanted success of
% \texttt{ss([b,b,a,a])}. The dependency graph suggests (quite
% unhelpfully) to put all of \texttt{aa,bb,ss} as abductable and the
% call to the abduction procedure returns (in this case the unique)
% \texttt{Asm = [aa([b, a, a])]}. This means that \emph{if it were the
%   case} that \texttt{aa([b, a, a])} holds, then \texttt{ss([b,b,a,a])}
% would have succeeded and the counterexample not found. It's a case of
% MA. Now, no clause head matches \texttt{aa([b, a, a])} and this points
% to the fact that there is no clause for the second production for
% \texttt{a}. % Once we add the clause
% % \begin{verbatim}
% % a3: aa([b|VW]) :- aa(V), aa(W), append(V,W,VW)
% % \end{verbatim}
% % the counterexample to completeness disappears.

\section{How it works}
\label{sec:calculus}

Our calculus encodes a judgment $\vdash_P G \leadsto\cA,\pi $, whereby
for a fixed program $P$ the execution of a goal $G$ yields a set of
assumptions $\cA$ and a proof tree $\pi$ such that $\pi$ corresponds in a
way that it is easy (?) to make precise to a sequent calculus
derivation in minimal logic of $P;\cA\vdash G$, which is \emph{strict} in $\cA$. This means that al least one of the assumptions \emph{must} be used (even more than once). That's another way to say that  $P;\dot\not\vdash G$.

In the interest of fast-prototyping, we use a very simple
meta-interpreter to compute $\cA,\pi $. Here's the syntax of goals

\begin{verbatim}
G ::= true | G,G | G;G | A
A ::= system  | pred 
pred ::= trusted | suspected
system ::= any other atom
\end{verbatim}

The definitions of a program are split into ``system'', such as
\texttt{count}, which we assume to be non problematic, and those we
want to look into (``pred'', for compatibility with \aprolog). The
latter clauses are enriched with rule names for explanation
purposes. However, once such as ``pred'' overcome our scrutiny, it can
graduate to ``trusted'' and be off the hook. System and trusted are \texttt{not} meta-interpreted, that is we just \texttt{call} them. Abductibles must be non-trusted \emph{pred} definitions. To wit, here's the current syntax of the above example (\texttt{rule} will become an infix operator, no \texttt{trusted} annotations here):
\begin{small}
\begin{verbatim}
:- discontiguous pred/1.
:- dynamic assumable/1.
:- dynamic trusted/1.
:- consult("../sabr"). % the meta-interpreter

assumable(ss(_X)).
assumable(bb(_X)).
assumable(aa(_X)).
add_assumption(A, Ass, [A|Ass]). % to customize the handling of assm

pred(ss(_)).
ss([]) :-rule(s1).
ss([b|W]) :- rule(s2), ss(W).   
ss([a|W]) :- rule(s3),bb(W).

pred(aa(_)).
aa([a|W]) :-rule(a1), ss(W).
aa([b|VW]) :- rule(a2), aa(V),aa(W), append(V,W,VW). 

pred(bb(_)).
bb([b|W]) :-rule(b1), ss(W).
bb([a|VW]) :- rule(b2), bb(V),bb(W), append(V,W,VW).

count(_,[],z).
count(X,[X|XS],s(N)) :- !, count(X,XS,N).
count(X,[_|XS],N) :- count(X,XS,N).
\end{verbatim}
\end{small}

Here is the flow we envision for getting explanations for a property
$\forall \vec{X}.~G \impp A$

\begin{enumerate}
\item Use abduction to investigate failure of $A$ to understand whether
  it is a case of MA or WA:
  \begin{itemize}
  \item put $A$ and its dependency graph as abductable and if the
    graph is a tree, put just the leaves?
  \end{itemize}
\item If failure of $A$ points to a case of MA, great. If it is
  ``unreasonable'', it is a case of WA and we verbalize the proof-tree for $G$.
  \begin{itemize}
  \item Special case: if $A$ is a constraint (equality, freshness), we assume it's WA and look at the premises directly.
  \end{itemize}

\end{enumerate}



TODO:

\begin{itemize}
\item syntax of proof-terms and proof-terms distillation~\cite{BlancoCM17}, as opposed to different levels of proof verbalization
\item calculus' rules and sketch of soundness
\end{itemize}

\section{Experimental evaluation}
\label{sec:exper}

\begin{itemize}
\item Reading in and out \aprolog programs
\item Mutating them with~\cite{MutProlog}
\item Experiment on blame allocation w/o knowing the mutation
\item Extend mutation operators to account for \aprolog features
  
% \item An aside: use Prolog's QC\cite{QProlog} as a (randomized)
%   baseline for \acheck on same language subset.
\end{itemize}


\subsection{Mutation testing for \aprolog}
\label{ssec:mutaprolog}

Mutation analysis~\cite{softwaretesting} is a common technique-to
evaluate software testing approaches, as a form of white box testing,
whereby a program is changed in a localized way by introducing a
\emph{single fault}. The resulting program is called a ``mutant'' and
the aim of a testing suite is to recognize the faulted code, which is
known as ``killing'' the mutant.

Our interest in that is two-fold: for once, it would be very useful to
have automatic generation of mutants so as to gauge the effectiveness
of \acheck in locating them. In previous work~\cite{KomauliM18}, we
resorted to \emph{manual} generation of mutants, but this is obviously
biased and impossible to scale. Secondly, killed mutants are good
candidates for blame assignment, since they simulate (under the
``competent programmer assumption'' \cite{softwaretesting}) reasonable
bugs that occur in a program without being planted by ourselves.

We thus developed a mutator for \aprolog programs, with an emphasis in
trying to devise mutation operators for the kind of programs we are in
interested in, that is models of PL artifacts. Previous work on
mutations for declarative programs~\cite{MutProlog,muchek} are only
partially relevant, the first for not caring about types, the latter
being mostly concerned with pattern matching and HO functions. This
means for us producing mutants that cannot be detected already by
static analysis such as type checking in \aprolog or singleton variable
analysis in standard Prolog. Further, since the checker uses a complete search strategy, we do not care for reordering clauses, nor do we aim to change the control flow dictated by the only control operator we allow from \aprolog, namely if-then-else.




\begin{small}
\begin{description}
  \item[Clause mutations:] predicate deletion and swap, replacement of conjunction by disjunction.%, or disjunction by conjunction.% \footnote{The authors of \cite{mutationtestingprolog} include in this category also cut insertion, removal and permutation. However in the \aProlog\ list-machine implementation we did not make use of cuts.}
  \item[Operator mutations:] arithmetic and relational operator mutation.
  \item[Variable mutations:] replacing variable  by (anonymous) variable and vice versa
  \item[Constant mutations:] replacing constant by constant, by (anonymous) variable and vice versa.
\end{description}
\end{small}

\section{Conclusion}
\label{sec:concl}


\bibliographystyle{alpha} \bibliography{blame,lm}
\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:

%  LocalWords:  PBT Ornaghi QuickCheck Ok Aho Ulmann's b's NAF WA's
%  LocalWords:  ss aa bb Asm pragma pred Abductibles TODO Milano lm
%  LocalWords:  mutator di OCAML
